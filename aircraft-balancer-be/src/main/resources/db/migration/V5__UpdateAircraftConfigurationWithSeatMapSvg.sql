ALTER TABLE AIRCRAFT ADD COLUMN SEAT_MAP_SVG TEXT;

UPDATE AIRCRAFT SET SEAT_MAP_SVG = '<?xml version="1.0" encoding="utf-8"?>
<svg viewBox="0 0 800 400" width="800px" height="400px" xmlns="http://www.w3.org/2000/svg" xmlns:bx="https://boxy-svg.com">
  <defs>
    <bx:grid x="0.291" y="-0.075" width="31.566" height="30.467"/>
  </defs>
  <line style="stroke: rgb(0, 0, 0);" x1="0" y1="100" x2="800" y2="100"/>
  <line style="stroke: rgb(0, 0, 0);" x1="0" y1="300" x2="800" y2="300"/>
  <g>
    <title>Right</title>
    <g>
      <title>R1</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-1F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-1E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-1D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 25, 0)">
      <title>R2</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-2F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-2E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-2D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 50, 0)">
      <title>R3</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-3F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-3E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-3D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 75, 0)">
      <title>R4</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-4F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-4E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-4D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 100, 0)">
      <title>R5</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-5F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-5E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-5D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 125, 0)">
      <title>R6</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-6F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-6E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-6D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 150, 0)">
      <title>R7</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-7F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-7E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-7D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 175, 0)">
      <title>R8</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-8F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-8E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-8D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 200, 0)">
      <title>R9</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-9F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-9E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-9D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 225, 0)">
      <title>R10</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-10F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-10E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-10D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 250, 0)">
      <title>R11</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-11F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-11E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-11D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 275, 0)">
      <title>R12</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-12F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-12E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-12D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 300, 0)">
      <title>R14</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-14F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-14E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-14D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 325, 0)">
      <title>R15</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-15F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-15E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-15D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 350, 0)">
      <title>R16</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-16F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-16E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-16D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 375, 0)">
      <title>R17</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-17F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-17E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-17D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 400, 0)">
      <title>R18</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-18F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-18E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-18D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 425, 0)">
      <title>R19</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-19F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-19E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-19D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 450, 0)">
      <title>R20</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-20F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-20E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-20D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 475, 0)">
      <title>R21</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-21F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-21E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-21D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 500, 0)">
      <title>R22</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-22F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-22E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-22D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 525, 0)">
      <title>R23</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-23F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-23E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-23D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 550, 0)">
      <title>R24</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-24F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-24E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-24D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 575, 0)">
      <title>R25</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-25F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-25E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-25D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 600, 0)">
      <title>R26</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-26F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-26E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-26D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 625, 0)">
      <title>R27</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-27F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-27E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-27D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 650, 0)">
      <title>R28</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-28F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-28E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-28D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 675, 0)">
      <title>R29</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-29F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-29E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-29D"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 700, 0)">
      <title>R30</title>
      <rect x="50" y="110.325" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-30F"/>
      <rect x="50" y="134.563" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-30E"/>
      <rect x="50" y="159" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-30D"/>
    </g>
  </g>
  <g>
    <title>Left</title>
    <g transform="matrix(1, 0, 0, 1, 0, 0)">
      <title>L1</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-1C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-1B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-1A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 25, 0)">
      <title>L2</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-2C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-2B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-2A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 50, 0)">
      <title>L3</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-3C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-3B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-3A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 75, 0)">
      <title>L4</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-4C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-4B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-4A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 100, 0)">
      <title>L5</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-5C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-5B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-5A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 125, 0)">
      <title>L6</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-6C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-6B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-6A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 150, 0)">
      <title>L7</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-7C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-7B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-7A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 175, 0)">
      <title>L8</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-8C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-8B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-8A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 200, 0)">
      <title>L9</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-9C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-9B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-9A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 225, 0)">
      <title>L10</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-10C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-10B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-10A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 250, 0)">
      <title>L11</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-11C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-11B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-11A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 275, 0)">
      <title>L12</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-12C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-12B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-12A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 300, 0)">
      <title>L14</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-14C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-14B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-14A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 325, 0)">
      <title>L15</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-15C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-15B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-15A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 350, 0)">
      <title>L16</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-16C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-16B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-16A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 375, 0)">
      <title>L17</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-17C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-17B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-17A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 400, 0)">
      <title>L18</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-18C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-18B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-18A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 425, 0)">
      <title>L19</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-19C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-19B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-19A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 450, 0)">
      <title>L20</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-20C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-20B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-20A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 475, 0)">
      <title>L21</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-21C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-21B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-21A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 500, 0)">
      <title>L22</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-22C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-22B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-22A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 525, 0)">
      <title>L23</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-23C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-23B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-23A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 550, 0)">
      <title>L24</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-24C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-24B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-24A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 575, 0)">
      <title>L25</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-25C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-25B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-25A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 600, 0)">
      <title>L26</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-26C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-26B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-26A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 625, 0)">
      <title>L27</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-27C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-27B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-27A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 650, 0)">
      <title>L28</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-28C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-28B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-28A"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 675, 0)">
      <title>L29</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-29C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-29B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-29A"/>
      <polyline style="fill: rgb(216, 216, 216); stroke: rgb(0, 0, 0);" points="-564.395 123.621"/>
    </g>
    <g transform="matrix(1, 0, 0, 1, 700, 0)">
      <title>L30</title>
      <rect x="50" y="212.746" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-30C"/>
      <rect x="50" y="237.172" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-30B"/>
      <rect x="50" y="260.911" width="20" height="25" style="stroke: rgb(0, 0, 0); fill: rgb(255, 255, 255);" id="id-30A"/>
    </g>
  </g>
  <g>
    <title>Line</title>
    <text style="fill: rgb(51, 51, 51); font-size: 19px; white-space: pre;" x="30" y="280">A</text>
    <text style="fill: rgb(51, 51, 51); font-size: 19px; white-space: pre;" x="30" y="255">B</text>
    <text style="fill: rgb(51, 51, 51); font-size: 19px; white-space: pre;" x="30" y="230">C</text>
    <text style="fill: rgb(51, 51, 51); font-size: 19px; white-space: pre;" x="30" y="180">D</text>
    <text style="fill: rgb(51, 51, 51); font-size: 19px; white-space: pre;" x="30" y="155">E</text>
    <text style="fill: rgb(51, 51, 51); font-size: 19px; white-space: pre;" x="30" y="130">F</text>
  </g>
  <g>
    <title>Row</title>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="55" y="205">1</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="80" y="205">2</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="105" y="205">3</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="130" y="205">4</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="155" y="205">5</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="180" y="205">6</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" y="205" x="205">7</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="230" y="205">8</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="255" y="205">9</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="273" y="205">10</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="299" y="205">11</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="323" y="205">12</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="348" y="205">14</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="373" y="205">15</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="398" y="205">16</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="423" y="205">17</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="448" y="205">18</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="473" y="205">19</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="498" y="205">20</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="523" y="205">21</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="548" y="205">22</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="573" y="205">23</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="598" y="205">24</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="623" y="205">25</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="648" y="205">26</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="673" y="205">27</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="698" y="205">28</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="723" y="205">29</text>
    <text style="fill: rgb(51, 51, 51); font-size: 17px; white-space: pre;" x="748" y="205">30</text>
  </g>
  <g>
    <title>SeatSection</title>
    <line style="stroke: rgb(0, 0, 0);" x1="50" y1="30" x2="770" y2="30"/>
    <line style="stroke: rgb(0, 0, 0);" x1="270" y1="20" x2="270" y2="40"/>
    <line style="stroke: rgb(0, 0, 0);" x1="50" y1="20" x2="50" y2="40"/>
    <line style="stroke: rgb(0, 0, 0);" x1="495" y1="20" x2="495" y2="40"/>
    <line style="stroke: rgb(0, 0, 0);" x1="770" y1="20" x2="770" y2="40"/>
    <text style="fill: rgb(51, 51, 51); font-size: 19px; white-space: pre; text-anchor: middle;" x="150" y="20">OA</text>
    <text style="fill: rgb(51, 51, 51); font-size: 19px; white-space: pre; text-anchor: middle;" x="370" y="20">OB</text>
    <text style="fill: rgb(51, 51, 51); font-size: 19px; white-space: pre; text-anchor: middle;" x="620" y="20">OC</text>
    <text style="fill: rgb(51, 51, 51); font-size: 19px; white-space: pre; text-anchor: middle;" x="150" y="55" id="OA">0 kg</text>
    <text style="fill: rgb(51, 51, 51); font-size: 19px; white-space: pre; text-anchor: middle;" x="370" y="55" id="OB">0 kg</text>
    <text style="fill: rgb(51, 51, 51); font-size: 19px; white-space: pre; text-anchor: middle;" x="620" y="55" id="OC">0 kg</text>
  </g>
  <g>
    <title>Wing</title>
    <polyline style="fill: rgb(216, 216, 216); stroke: rgb(0, 0, 0);" points="250 99 265 79 450 79 445 99"/>
    <polyline style="fill: rgb(216, 216, 216); stroke: rgb(0, 0, 0);" points="250 301 265 321 450 321 445 301"/>
  </g>
  <g>
    <title>CompSection</title>
    <g>
      <title>COMP 1</title>
      <line style="stroke: rgb(0, 0, 0);" x1="140" y1="360" x2="270" y2="360"/>
      <line style="stroke: rgb(0, 0, 0);" x1="140" y1="350" x2="140" y2="370"/>
      <line style="stroke: rgb(0, 0, 0);" x1="270" y1="350" x2="270" y2="370"/>
      <text style="fill: rgb(51, 51, 51); font-size: 19px; white-space: pre; text-anchor: middle;" x="205" y="350">COMP 1</text>
      <text style="fill: rgb(51, 51, 51); font-size: 19px; white-space: pre; text-anchor: middle;" x="205" y="385" id="COMP1">0 kg</text>
    </g>
    <g>
      <title>COMP 3-5</title>
      <line style="stroke: rgb(0, 0, 0);" x1="425" y1="360" x2="785" y2="360"/>
      <line style="stroke: rgb(0, 0, 0);" x1="425" y1="350" x2="425" y2="370"/>
      <line style="stroke: rgb(0, 0, 0);" x1="545" y1="350" x2="545" y2="370"/>
      <line style="stroke: rgb(0, 0, 0);" x1="665" y1="350" x2="665" y2="370"/>
      <line style="stroke: rgb(0, 0, 0);" x1="785" y1="350" x2="785" y2="370"/>
      <text style="fill: rgb(51, 51, 51); font-size: 19px; white-space: pre; text-anchor: middle;" x="490" y="350">COMP 3</text>
      <text style="fill: rgb(51, 51, 51); font-size: 19px; white-space: pre; text-anchor: middle;" x="610" y="350">COMP 4</text>
      <text style="fill: rgb(51, 51, 51); font-size: 19px; white-space: pre; text-anchor: middle;" x="725" y="350">COMP 5</text>
      <text style="fill: rgb(51, 51, 51); font-size: 19px; white-space: pre; text-anchor: middle;" x="490" y="385" id="COMP3">0 kg</text>
      <text style="fill: rgb(51, 51, 51); font-size: 19px; white-space: pre; text-anchor: middle;" x="610" y="385" id="COMP4">0 kg</text>
      <text style="fill: rgb(51, 51, 51); font-size: 19px; white-space: pre; text-anchor: middle;" x="725" y="385" id="COMP5">0 kg</text>
    </g>
  </g>
  <g>
    <title>Exits</title>
    <path d="M 37.831 108.423 H 43.819 L 43.819 89.584 L 62.826 114.703 L 43.819 139.823 L 43.819 120.983 H 37.831 V 108.423 Z" style="fill: rgb(93, 91, 91); stroke: rgb(93, 91, 91);" transform="matrix(0.004807, -0.999988, 0.999988, 0.004807, -87.473763, 132.547089)" bx:shape="arrow 37.831 89.584 24.995 50.239 12.56 19.007 0 1@f5bd2441"/>
    <path d="M 37.831 108.423 H 43.819 L 43.819 89.584 L 62.826 114.703 L 43.819 139.823 L 43.819 120.983 H 37.831 V 108.423 Z" style="fill: rgb(93, 91, 91); stroke: rgb(93, 91, 91);" transform="matrix(0.004807, -0.999988, 0.999988, 0.004807, 194.865692, 133.484421)" bx:shape="arrow 37.831 89.584 24.995 50.239 12.56 19.007 0 1@f5bd2441"/>
    <path d="M 37.831 108.423 H 43.819 L 43.819 89.584 L 62.826 114.703 L 43.819 139.823 L 43.819 120.983 H 37.831 V 108.423 Z" style="fill: rgb(93, 91, 91); stroke: rgb(93, 91, 91);" transform="matrix(0.004807, -0.999988, 0.999988, 0.004807, 245.959686, 133.484421)" bx:shape="arrow 37.831 89.584 24.995 50.239 12.56 19.007 0 1@f5bd2441"/>
    <path d="M 37.831 108.423 H 43.819 L 43.819 89.584 L 62.826 114.703 L 43.819 139.823 L 43.819 120.983 H 37.831 V 108.423 Z" style="fill: rgb(93, 91, 91); stroke: rgb(93, 91, 91);" transform="matrix(0.004807, -0.999988, 0.999988, 0.004807, 659, 133.484421)" bx:shape="arrow 37.831 89.584 24.995 50.239 12.56 19.007 0 1@f5bd2441"/>
    <path d="M -37.831 -70.744 H -31.843 L -31.843 -89.584 L -12.836 -64.464 L -31.843 -39.345 L -31.843 -58.184 H -37.831 V -70.744 Z" style="fill: rgb(93, 91, 91); stroke: rgb(93, 91, 91);" transform="matrix(0.004807, 0.999988, 0.999988, -0.004807, 92.395231, 342.73122)" bx:shape="arrow -37.831 -89.584 24.995 50.239 12.56 19.007 0 1@8639654b"/>
    <path d="M -37.831 -70.744 H -31.843 L -31.843 -89.584 L -12.836 -64.464 L -31.843 -39.345 L -31.843 -58.184 H -37.831 V -70.744 Z" style="fill: rgb(93, 91, 91); stroke: rgb(93, 91, 91);" transform="matrix(0.004807, 0.999988, 0.999988, -0.004807, 375.114714, 342.73122)" bx:shape="arrow -37.831 -89.584 24.995 50.239 12.56 19.007 0 1@8639654b"/>
    <path d="M 37.831 108.423 H 43.819 L 43.819 89.584 L 62.826 114.703 L 43.819 139.823 L 43.819 120.983 H 37.831 V 108.423 Z" style="fill: rgb(93, 91, 91); stroke: rgb(93, 91, 91);" transform="matrix(0.004807, 0.999988, 0.999988, -0.004807, 245.959688, 267.931377)" bx:shape="arrow 37.831 89.584 24.995 50.239 12.56 19.007 0 1@f5bd2441"/>
    <path d="M -37.831 -70.744 H -31.843 L -31.843 -89.584 L -12.836 -64.464 L -31.843 -39.345 L -31.843 -58.184 H -37.831 V -70.744 Z" style="fill: rgb(93, 91, 91); stroke: rgb(93, 91, 91);" transform="matrix(0.004807, 0.999988, 0.999988, -0.004807, 838.847227, 342.731251)" bx:shape="arrow -37.831 -89.584 24.995 50.239 12.56 19.007 0 1@8639654b"/>
  </g>
</svg>'
WHERE MODEL_NUMBER = 'A320-214';