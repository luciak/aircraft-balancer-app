package org.luciak.acb.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class PaxNotFoundException extends Exception {

    public PaxNotFoundException(String message) {
        super(message);
    }
}
