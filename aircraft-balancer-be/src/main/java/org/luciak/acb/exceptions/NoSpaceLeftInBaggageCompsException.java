package org.luciak.acb.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class NoSpaceLeftInBaggageCompsException extends RuntimeException {

    public NoSpaceLeftInBaggageCompsException(String message) {
        super(message);
    }
}
