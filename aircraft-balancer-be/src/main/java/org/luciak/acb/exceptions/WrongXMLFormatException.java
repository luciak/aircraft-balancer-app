package org.luciak.acb.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class WrongXMLFormatException extends RuntimeException {

    public WrongXMLFormatException(String message) {
        super(message);
    }
}
