package org.luciak.acb.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class SeatIsAlreadyOccupied extends RuntimeException {

    public SeatIsAlreadyOccupied(String message) {
        super(message);
    }
}
