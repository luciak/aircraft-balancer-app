package org.luciak.acb.exceptions;


import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class NoEmptySeatException extends RuntimeException {

    public NoEmptySeatException(String message) {
        super(message);
    }
}
