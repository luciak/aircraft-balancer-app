package org.luciak.acb.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class FlightAlreadyExistsException extends Exception {

    public FlightAlreadyExistsException(String message) {
        super(message);
    }
}
