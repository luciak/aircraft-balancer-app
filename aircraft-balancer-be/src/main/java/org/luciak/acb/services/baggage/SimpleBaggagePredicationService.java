package org.luciak.acb.services.baggage;

import org.springframework.stereotype.Component;

/**
 * Simple impementation of baggage predication service.
 *
 * @author Lucia Kleinova
 */
@Component
public class SimpleBaggagePredicationService implements BaggagePredicationService {

    final static int PREDICATED_BAGGAGE_WEIGHT = 10;

    /**
     * {@link BaggagePredicationService#predicatedBaggageWeight()}
     */
    @Override
    public int predicatedBaggageWeight() {

        return PREDICATED_BAGGAGE_WEIGHT;
    }
}
