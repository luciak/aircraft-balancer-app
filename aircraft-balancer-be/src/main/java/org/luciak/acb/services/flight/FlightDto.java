package org.luciak.acb.services.flight;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import org.apache.commons.lang3.StringUtils;
import org.luciak.acb.domain.flight.FlightStatus;
import org.luciak.acb.domain.flight.Sex;
import org.luciak.acb.services.aircraft.AircraftDto;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

/**
 * Data transfer object between UI and Service layer which transfers information about
 * flights.
 *
 * @author Lucia Kleinova
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonDeserialize(builder = FlightDto.Builder.class)
public final class FlightDto {

    /* **************************************************************************************************************
     * CLASS MEMBERS
     * **************************************************************************************************************/

    /**
     * Unique flight identifier according to AIDX specification.
     */
    private final String ufi;

    /**
     * Aircraft configuration.
     */
    private final AircraftDto aircraft;

    /**
     * Flight number.
     */
    private final String flightNumber;

    /**
     * UTC scheduled date of departure of a flight.
     */
    private final LocalDate originDate;

    /**
     * Departure airport code (IATA or ICAO).
     */
    private final String departureAirport;

    /**
     * Arrival airport code (IATA or ICAO).
     */
    private final String arrivalAirport;

    /**
     * Optional repeat number.
     */
    private final Integer repeatNumber;

    /**
     * Summary information about passengers.
     */
    private final Map<Sex, Integer> paxSummary;

    /**
     * Flight status.
     */
    private final FlightStatus status;


    /* **************************************************************************************************************
     * CONVENTIONAL METHODS (getters, constructors, hashCode, equals etc.)
     * **************************************************************************************************************/

    /**
     * Get Unique Flight Identifier.
     *
     * @return unique flight identifier
     */
    public String getUfi() {
        return ufi;
    }

    /**
     * Get aircraft configuration.
     *
     * @return aircraft configuration {@link AircraftDto}
     */
    public AircraftDto getAircraft() {
        return aircraft;
    }

    /**
     * Get aircraft flight number using format:
     * <p>
     * <ul>
     * <li>Airline code (IATA or ICAO)</li>
     * <li>Flight number (IATA)</li>
     * <li>Operational suffix (optional)</li>
     * </ul>
     *
     * @return aircraft flight number
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Get UTC origin departure date of a flight.
     *
     * @return origin departure date of a flight
     */
    public LocalDate getOriginDate() {
        return originDate;
    }

    /**
     * Get departure airport code (IATA or ICAO).
     *
     * @return departure airport code
     */
    public String getDepartureAirport() {
        return departureAirport;
    }

    /**
     * Get arrival airport code (IATA or ICAO).
     *
     * @return arrival airport code
     */
    public String getArrivalAirport() {
        return arrivalAirport;
    }

    /**
     * Get repeat number (optional).
     *
     * @return repeat number
     */
    public Integer getRepeatNumber() {
        return repeatNumber;
    }

    /**
     * Get pax summary.
     *
     * @return pax summary
     */
    public Map<Sex, Integer> getPaxSummary() {
        return paxSummary;
    }

    /**
     * Get flight status {@link FlightStatus}
     *
     * @return flight status
     */
    public FlightStatus getStatus() {
        return status;
    }

    /**
     * Private constructor. Use FlightDto.Builder instead.
     *
     * @param builder {@link FlightDto.Builder}
     */
    private FlightDto(Builder builder) {

        this.ufi = builder.ufi;
        this.aircraft = builder.aircraft;
        this.flightNumber = builder.flightNumber;
        this.originDate = builder.originDate;
        this.departureAirport = builder.departureAirport;
        this.arrivalAirport = builder.arrivalAirport;
        this.repeatNumber = builder.repeatNumber;
        this.paxSummary = builder.paxSummary;
        this.status = builder.status;
    }


    /* **************************************************************************************************************
     * OBJECT BUILDER
     * **************************************************************************************************************/

    /**
     * Flight DTO builder.
     */
    @JsonPOJOBuilder
    static class Builder {

        private String ufi;

        private final AircraftDto aircraft;

        private final String flightNumber;

        private final LocalDate originDate;

        private final String departureAirport;

        private final String arrivalAirport;

        private Integer repeatNumber;

        private Map<Sex, Integer> paxSummary;

        private FlightStatus status;

        @JsonCreator
        Builder(@JsonProperty("aircraft") AircraftDto aircraft,
                @JsonProperty("flightNumber") String flightNumber,
                @JsonProperty("originDate") LocalDate originDate,
                @JsonProperty("departureAirport") String departureAirport,
                @JsonProperty("arrivalAirport") String arrivalAirport) {

            Assert.notNull(aircraft, "Aircraft configuration cannot be null!");
            Assert.isTrue(StringUtils.isNotBlank(aircraft.getRegistrationMark()),
                    "Aircraft registration mark cannot be null or empty!");
            Assert.isTrue(StringUtils.isNotBlank(flightNumber),
                    "Flight number cannot be null or empty!");
            Assert.notNull(originDate, "Origin date cannot be null!");
            Assert.isTrue(StringUtils.isNotBlank(departureAirport),
                    "Departure airport cannot be null or empty!");
            Assert.isTrue(StringUtils.isNotBlank(arrivalAirport),
                    "Arrival airport cannot be null or empty!");

            this.aircraft = aircraft;
            this.flightNumber = flightNumber;
            this.originDate = originDate;
            this.departureAirport = departureAirport;
            this.arrivalAirport = arrivalAirport;
        }

        Builder withUfi(String ufi) {

            this.ufi = ufi;
            return this;
        }

        Builder withRepeatNumber(Integer repeatNumber) {

            this.repeatNumber = repeatNumber;
            return this;
        }

        Builder withStatus(FlightStatus status) {

            this.status = status;
            return this;
        }

        Builder withPaxSummary(Map<Sex, Integer> paxSummary) {

            if (paxSummary != null) {

                this.paxSummary = new HashMap<>();
                this.paxSummary.putAll(paxSummary);
            }
            return this;
        }

        FlightDto build() {

            return new FlightDto(this);
        }
    }

}
