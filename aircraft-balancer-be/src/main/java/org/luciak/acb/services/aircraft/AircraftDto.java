package org.luciak.acb.services.aircraft;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

/**
 * Data transfer object between UI and Service layer which transfers information about
 * aircraft configuration.
 *
 * @author Lucia Kleinova
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonDeserialize(builder = AircraftDto.Builder.class)
public final class AircraftDto {

    /* **************************************************************************************************************
     * CLASS MEMBERS
     * **************************************************************************************************************/

    /**
     * Unique registration mark of aircraft
     */
    private final String registrationMark;

    /**
     * Model number of aircraft
     */
    private final String modelNumber;

    /**
     * Version of aircraft
     */
    private final String version;

    /**
     * Configuration code of aircraft
     */
    private final String cfgCode;


    /* **************************************************************************************************************
     * CONVENTIONAL METHODS (getters, constructors, hashCode, equals etc.)
     * **************************************************************************************************************/

    /**
     * Get unique registration mark of aircraft.
     *
     * @return unique registration mark of aircraft
     */
    public String getRegistrationMark() {
        return registrationMark;
    }

    /**
     * Get model number of aircraft.
     *
     * @return model number of aircraft
     */
    public String getModelNumber() {
        return modelNumber;
    }

    /**
     * Get version of aircraft.
     *
     * @return version of aircraft
     */
    public String getVersion() {
        return version;
    }

    /**
     * Get configuration code of aircraft.
     *
     * @return configuration code of aircraft
     */
    public String getCfgCode() {
        return cfgCode;
    }

    /**
     * Private Constructor. Use AircraftDto.Builder instead.
     *
     * @param builder {@link AircraftDto.Builder}
     */
    private AircraftDto(final Builder builder) {

        this.registrationMark = builder.registrationMark;
        this.modelNumber = builder.modelNumber;
        this.version = builder.version;
        this.cfgCode = builder.cfgCode;
    }


    /* **************************************************************************************************************
     * OBJECT BUILDER
     * **************************************************************************************************************/

    /**
     * AircraftDto builder.
     */
    @JsonPOJOBuilder
    static class Builder {

        private final String registrationMark;

        private String modelNumber;

        private String version;

        private String cfgCode;

        @JsonCreator
        Builder(@JsonProperty("registrationMark") final String registrationMark) {

            Assert.isTrue(StringUtils.isNotBlank(registrationMark),
                    "Registration mark cannot be null or empty!");

            this.registrationMark = registrationMark;
        }

        Builder withModelNumber(final String modelNumber) {

            this.modelNumber = modelNumber;
            return this;
        }

        Builder withVersion(final String version) {

            this.version = version;
            return this;
        }

        Builder withCfgCode(final String cfgCode) {

            this.cfgCode = cfgCode;
            return this;
        }

        AircraftDto build() {

            return new AircraftDto(this);
        }
    }
}
