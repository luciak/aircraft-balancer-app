package org.luciak.acb.services.flight;

import org.apache.commons.lang3.StringUtils;
import org.luciak.acb.domain.flight.Flight;
import org.luciak.acb.domain.flight.FlightStatus;
import org.luciak.acb.domain.flight.Pax;
import org.luciak.acb.exceptions.AircraftNotFoundException;
import org.luciak.acb.exceptions.FlightAlreadyExistsException;
import org.luciak.acb.exceptions.FlightNotFoundException;
import org.luciak.acb.exceptions.IncorrectStateOfFlightException;
import org.luciak.acb.exceptions.PaxNotFoundException;
import org.luciak.acb.exceptions.SeatNotFoundException;
import org.luciak.acb.repositories.flight.FlightFactory;
import org.luciak.acb.repositories.flight.FlightRepository;
import org.luciak.acb.repositories.flight.PaxFactory;
import org.luciak.acb.services.aircraft.SeatDto;
import org.luciak.acb.services.aircraft.SeatDtoMapper;
import org.luciak.acb.services.baggage.BaggagePredicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Flight service.
 *
 * @author Lucia Kleinova
 */
@Service
public class FlightService {

    /**
     * Flight repository.
     */
    private final FlightRepository flightRepository;

    /**
     * Flight factory.
     */
    private final FlightFactory flightFactory;

    /**
     * Pax factory.
     */
    private final PaxFactory paxFactory;

    /**
     * Baggage weight predication service
     */
    private final BaggagePredicationService baggagePredicationService;

    /**
     * Autowired constructor of FlightService.
     *
     * @param flightRepository          {@link FlightRepository}
     * @param flightFactory             {@link FlightFactory}
     * @param paxFactory                {@link PaxFactory}
     * @param baggagePredicationService {@link BaggagePredicationService}
     */
    @Autowired
    public FlightService(final FlightRepository flightRepository,
                         final FlightFactory flightFactory,
                         final PaxFactory paxFactory,
                         final BaggagePredicationService baggagePredicationService) {

        this.flightRepository = flightRepository;
        this.flightFactory = flightFactory;
        this.paxFactory = paxFactory;
        this.baggagePredicationService = baggagePredicationService;
    }

    /**
     * Find all flights ordered by departure date.
     *
     * @return list of flights. {@link FlightDto}
     */
    public List<FlightDto> findAllFlights() {

        return flightRepository.findAllFlights()
                .stream()
                .map(FlightDtoMapper::mapFlightInfoToFlightDto)
                .collect(Collectors.toList());
    }

    /**
     * Find all flights by flight status ordered by departure date
     *
     * @param status flight status
     * @return list of flights. {@link FlightDto}
     */
    public List<FlightDto> findFlightsByStatus(final FlightStatus status) {

        Assert.notNull(status, "Flight status cannot be null!");

        return flightRepository.findAllFlightsByStatus(status)
                .stream()
                .map(FlightDtoMapper::mapFlightInfoToFlightDto)
                .collect(Collectors.toList());
    }

    /**
     * Get flights detail by UFI.
     *
     * @param ufi Unique Flight Identifier according to AIDX specification
     * @return detail of flight {@link FlightDto}
     * @throws FlightNotFoundException in case when flight doesn't exists
     */
    @Transactional
    public FlightDto getFlight(final String ufi) throws FlightNotFoundException {

        Assert.isTrue(StringUtils.isNotBlank(ufi), "UFI cannot be blank or empty!");

        return FlightDtoMapper.mapFlightToFlightDto(flightRepository.getFlight(ufi));
    }


    /**
     * Create new flight. Status of the newly created flight will be set to CREATED.
     *
     * @param flightDto flight information. {@link FlightDto}
     * @return newly created {@link FlightDto} with updated information like UFI, FlightStatus, etc.
     */
    @Transactional
    public FlightDto createFlight(final FlightDto flightDto) throws AircraftNotFoundException, FlightAlreadyExistsException {

        Assert.notNull(flightDto, "Flight metadata cannot be null!");
        Assert.notNull(flightDto.getAircraft(), "Aircraft cannot be null");

        Flight flight = flightFactory.createFlight(
                flightDto.getAircraft().getRegistrationMark(),
                flightDto.getFlightNumber(),
                flightDto.getOriginDate(),
                flightDto.getDepartureAirport(),
                flightDto.getArrivalAirport(),
                flightDto.getRepeatNumber());

        if (flightRepository.exists(flight.getUfi())) {
            throw new FlightAlreadyExistsException(String.format("Flight with UFI '%s' already exists!",
                    flight.getUfi()));
        }

        flightRepository.persistFlight(flight);

        return FlightDtoMapper.mapFlightToFlightDto(flight);
    }

    /**
     * Update flight metadata. Flight must be in CREATED state.
     *
     * @param ufi       original UFI of the updated flight
     * @param flightDto metadataAboutFlight {@link FlightDto}
     * @return newly created {@link FlightDto} with updated information like UFI, FlightStatus, etc.
     */
    @Transactional
    public FlightDto updateFlight(final String ufi, final FlightDto flightDto) throws AircraftNotFoundException,
            FlightNotFoundException, FlightAlreadyExistsException, IncorrectStateOfFlightException {

        Assert.isTrue(StringUtils.isNotBlank(ufi), "Unique flight identifier cannot be null or empty!");
        Assert.notNull(flightDto, "Flight cannot be null!");

        // Check if original flight exists
        Flight flight = flightRepository.getFlight(ufi);
        if (!FlightStatus.CREATED.equals(flight.getStatus())) {
            throw new IncorrectStateOfFlightException(
                    String.format("Flight wit UFI '%s' is not in CREATED state and cannot be updated!", ufi));
        }

        // Create target flight with updated information
        flight = flightFactory.createFlight(
                flightDto.getAircraft().getRegistrationMark(),
                flightDto.getFlightNumber(),
                flightDto.getOriginDate(),
                flightDto.getDepartureAirport(),
                flightDto.getArrivalAirport(),
                flightDto.getRepeatNumber());

        // Check if target flight already exists
        if (!ufi.equals(flight.getUfi()) && flightRepository.exists(flight.getUfi())) {
            throw new FlightAlreadyExistsException(String.format("Flight with UFI '%s' already exists!", ufi));
        }

        flightRepository.deleteFlight(ufi);
        flightRepository.persistFlight(flight);

        return FlightDtoMapper.mapFlightToFlightDto(flight);
    }

    /**
     * Delete entire flight.
     *
     * @param ufi unique flight identifier
     * @throws FlightNotFoundException in case when flight not exists
     */
    @Transactional
    public void deleteFlight(final String ufi) throws FlightNotFoundException {

        Assert.isTrue(StringUtils.isNotBlank(ufi), "Unique flight identifier cannot be null or empty!");

        if (!flightRepository.exists(ufi)) {
            throw new FlightNotFoundException(String.format("Flight with UFI '%s' doesn't exist!", ufi));
        }

        flightRepository.deleteFlight(ufi);
    }

    /**
     * Upload paxlist (passenger list) for the flight. Flight must be in state {@link FlightStatus#CREATED}
     * or {@link FlightStatus#PREPARED}.
     *
     * @param ufi     unique flight identifier
     * @param paxlist paxlist as XML string
     * @return updated {@link FlightDto}
     * @throws FlightNotFoundException         in case when flight doesn't exist
     * @throws IncorrectStateOfFlightException in case when flight is in incorrect state
     */
    @Transactional
    public FlightDto uploadPaxlist(final String ufi, final PaxlistDto paxlist)
            throws FlightNotFoundException, IncorrectStateOfFlightException {

        Assert.isTrue(StringUtils.isNotBlank(ufi), "Unique flight identifier cannot be null or empty!");
        Assert.notNull(paxlist, "Paxlist cannot be null!");
        Assert.notNull(paxlist.getPaxlist(), "Paxlist cannot be null!");
        Assert.notEmpty(paxlist.getPaxlist(), "Paxlist cannot be empty!");

        Flight flight = flightRepository.getFlight(ufi);
        flight.importPaxlist(paxlist.getPaxlist().stream()
                .map(pax -> paxFactory.cratePax(pax.getName(),
                        pax.getSurname(), pax.getSex(), pax.getCodeId(), pax.getGroupId(),
                        pax.getIdNumber(), pax.getBirthDate(), pax.getFFT(),
                        pax.getSegment(), baggagePredicationService.predicatedBaggageWeight(),
                        null))
                .collect(Collectors.toList()));

        // persist flight
        flightRepository.persistFlight(flight);

        return FlightDtoMapper.mapFlightToFlightDto(flight);
    }

    /**
     * Delete paxlist (passenger list) from the flight. Flight must be in state {@link FlightStatus#CREATED}
     * or {@link FlightStatus#PREPARED}.
     *
     * @param ufi unique flight indentifier
     * @return updated {@link FlightDto}
     * @throws FlightNotFoundException
     * @throws IncorrectStateOfFlightException
     */
    @Transactional
    public FlightDto deletePaxlist(final String ufi)
            throws FlightNotFoundException, IncorrectStateOfFlightException {

        Assert.isTrue(StringUtils.isNotBlank(ufi), "Unique flight identifier cannot be null or empty!");

        Flight flight = flightRepository.getFlight(ufi);
        flight.deletePaxlist();

        flightRepository.persistFlight(flight);

        return FlightDtoMapper.mapFlightToFlightDto(flight);
    }

    /**
     * Confirm paxlist for the flight. The flight must be in PREPARED state.
     *
     * @param ufi unique flight identifier
     * @return updated flight
     * @throws FlightNotFoundException         in case when flight not found
     * @throws IncorrectStateOfFlightException in case when the flight is in incorrect state
     */
    @Transactional
    public FlightDto confirmPaxlist(final String ufi)
            throws FlightNotFoundException, IncorrectStateOfFlightException {

        Assert.isTrue(StringUtils.isNotBlank(ufi), "Unique flight identifier cannot be null or empty!");

        Flight flight = flightRepository.getFlight(ufi);
        flight.confirmPaxlist();

        flightRepository.persistFlight(flight);
        return FlightDtoMapper.mapFlightToFlightDto(flight);
    }

    /**
     * Get pax list for the flight.
     *
     * @param ufi unique flight identifier
     * @return List of {@link PaxDto}
     */
    @Transactional
    public List<PaxDto> getPaxlist(final String ufi) throws FlightNotFoundException {

        Assert.isTrue(StringUtils.isNotBlank(ufi), "Unique flight identifier cannot be null or empty!");

        Flight flight = flightRepository.getFlight(ufi);

        if (flight.getPaxlist() == null) {
            return Collections.emptyList();
        }

        return flight.getPaxlist().stream()
                .map(PaxDtoMapper::convertPaxToPaxDto)
                .collect(Collectors.toList());
    }

    /**
     * Get actual flight setup wich contains information like seatmap with assigned passengers,
     * weight of each seat sections and baggage compartment, balancing status etc.
     *
     * @param ufi unique flight identifier
     * @return actual flight setup {@link FlightSetupDto}
     * @throws FlightNotFoundException in case when flight not found
     */
    @Transactional
    public FlightSetupDto getActualFlightSetup(final String ufi) throws FlightNotFoundException {

        Assert.isTrue(StringUtils.isNotBlank(ufi), "Unique flight identifier cannot be null or empty!");

        Flight flight = flightRepository.getFlight(ufi);

        return collectFlightSetupInformation(flight);
    }

    /**
     * Assign final seat for passengers and update his baggage information.
     * This action is possible when the flight is in {@link FlightStatus#PROCESSING} state.
     *
     * @param ufi unique flight status
     * @param pax passenger {@link PaxDto} who will be confirmed
     * @return updated information about pax {@link PaxDto}
     * @throws FlightNotFoundException         in case when flight not found
     * @throws IncorrectStateOfFlightException in case when flight is in incorrect state
     * @throws PaxNotFoundException            in case when pax which should be confirmed doesn't exist
     * @throws SeatNotFoundException           in case when seat where the pax should be seated doesn't exist
     */
    @Transactional
    public PaxDto confirmPax(final String ufi, final PaxDto pax)
            throws FlightNotFoundException, IncorrectStateOfFlightException,
            PaxNotFoundException, SeatNotFoundException {

        Assert.isTrue(StringUtils.isNotBlank(ufi), "Unique flight identifier cannot be null or empty!");
        Assert.notNull(pax, "Passengers cannot be null!");

        Flight flight = flightRepository.getFlight(ufi);
        Pax result = flight.confirmPax(
                pax.getIdNumber(),
                pax.getSeat(),
                pax.getCodeId(),
                pax.getFFT(),
                pax.getBaggageWeight());

        flightRepository.persistFlight(flight);

        return PaxDtoMapper.convertPaxToPaxDto(result);
    }

    /**
     * Close the flight and remove all unassigned passengers from the pax list.
     *
     * Flight must be in {@link FlightStatus#BALANCED} or {@link FlightStatus#PROCESSING} state.
     *
     * @param ufi unique flight identifier
     * @return updated flight
     * @throws FlightNotFoundException in case when flight doesn't exists
     * @throws IncorrectStateOfFlightException in case when flight is in illegal state
     */
    @Transactional
    public FlightDto closeFlight(final String ufi) throws FlightNotFoundException, IncorrectStateOfFlightException {

        Assert.isTrue(StringUtils.isNotBlank(ufi), "Unique flight identifier cannot be null or empty!");

        Flight flight = flightRepository.getFlight(ufi);
        flight.closeFlight();

        flightRepository.persistFlight(flight);

        return FlightDtoMapper.mapFlightToFlightDto(flight);
    }

    /**
     * Collect actual setup information about flight.
     *
     * @param flight flight
     * @return flight setup
     */
    private FlightSetupDto collectFlightSetupInformation(final Flight flight) {

        Assert.notNull(flight, "Flight cannot be null!");

        // calculate actual seat map
        Map<String, SeatDto> seatmap = new HashMap<>();
        flight.getAircraft().getSeatMap()
                .forEach((seatnum, seat) -> seatmap.put(seatnum, SeatDtoMapper.convertToSeatDto(seat)));

        // calculate assigned seats and pax left count
        int assignedSeatsCount = 0;
        int paxLeftCount = 0;
        if (flight.getPaxlist() != null) {
            for (Pax pax : flight.getPaxlist()) {
                if (pax.isPredictedSeat()) {
                    paxLeftCount++;
                } else {
                    assignedSeatsCount++;
                }
            }
        }

        return new FlightSetupDto.Builder(
                // assigned seats
                assignedSeatsCount,
                // pax left
                paxLeftCount,
                // actual weights for each section
                flight.getSectionWeightMap(),
                // actual weights for each compartments
                flight.getCompWeightMap(),
                // actual seat map
                seatmap)
                .build();
    }
}
