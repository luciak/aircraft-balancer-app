package org.luciak.acb.services.flight;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.luciak.acb.services.aircraft.SeatDto;

import java.util.Map;

/**
 * Data transfer object witch contains actual setup of flight
 * like weight on each seating sections,  baggage compartments, actual
 * seat map with assigned passengers etc.
 *
 * @author Lucia Kleinova
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public final class FlightSetupDto {

    /* **************************************************************************************************************
     * CLASS MEMBERS
     * **************************************************************************************************************/

    /**
     * Count of passengers which have been assigned to the seat
     */
    private final int assignedSeats;

    /**
     * Actual count of passengers which have not been assigned for the seat
     */
    private final int paxLeft;
    /**
     * Map of seat sections which contains sum of passenger weight for each sections.
     * <p>
     * Key: section ID, Value: sum of passenger weight in section [kg]
     */
    private final Map<String, Integer> sectionWeightMap;

    /**
     * Map of compartments which contains sum of baggage wight for each compartments.
     * <p>
     * Key: compartment ID, Value: sum of baggage weight in compartment [kg]
     */
    private final Map<String, Integer> compWeightMap;

    /**
     * Map of seats in aircraft with information about passenger who occupied the seat.
     * <p>
     * Key: number of seat, Value: detail about seat and passenger on it
     */
    private final Map<String, SeatDto> seatMap;

    /* **************************************************************************************************************
     * CONVENTIONAL METHODS (getters, constructors, hashCode, equals etc.)
     * **************************************************************************************************************/

    /**
     * Get actual count of seats which have been assigned for the passengers
     * @return actual count of seats assigned for passengers
     */
    public int getAssignedSeats() {
        return assignedSeats;
    }

    /**
     * Get actual count of passengers which have not been assigned for the seat
     * @return
     */
    public int getPaxLeft() {
        return paxLeft;
    }

    /**
     * Get actual section map which contains sum of passenger weight for each sections.
     *
     * @return actual sections weight map
     */
    public Map<String, Integer> getSectionWeightMap() {
        return sectionWeightMap;
    }

    /**
     * Get actual compartment map which contains sum of baggage weight for each compartments.
     *
     * @return actual compartments weight map
     */
    public Map<String, Integer> getCompWeightMap() {
        return compWeightMap;
    }

    /**
     * Get actual seat map which contains information about status of all seats and passengers which occupied the seat.
     *
     * @return actual seat map
     */
    public Map<String, SeatDto> getSeatMap() {
        return seatMap;
    }

    public FlightSetupDto(Builder builder) {

        this.assignedSeats = builder.assignedSeats;
        this.paxLeft = builder.paxLeft;
        this.sectionWeightMap = builder.sectionWeightMap;
        this.compWeightMap = builder.compWeightMap;
        this.seatMap = builder.seatMap;
    }

    /* **************************************************************************************************************
     * OBJECT BUILDER
     * **************************************************************************************************************/

    static class Builder {

        private final int assignedSeats;

        private final int paxLeft;

        private final Map<String, Integer> sectionWeightMap;

        private final Map<String, Integer> compWeightMap;

        private final Map<String, SeatDto> seatMap;

        Builder(final int assignedSeats,
                final int paxLeft,
                final Map<String, Integer> sectionWeightMap,
                final Map<String, Integer> compWeightMap,
                final Map<String, SeatDto> seatMap) {

            this.assignedSeats = assignedSeats;
            this.paxLeft = paxLeft;
            this.sectionWeightMap = sectionWeightMap;
            this.compWeightMap = compWeightMap;
            this.seatMap = seatMap;
        }

        FlightSetupDto build() {

            return new FlightSetupDto(this);
        }
    }
}
