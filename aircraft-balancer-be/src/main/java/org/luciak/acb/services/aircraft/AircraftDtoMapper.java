package org.luciak.acb.services.aircraft;

import org.luciak.acb.domain.aircraft.Aircraft;
import org.springframework.util.Assert;

/**
 * Aircraft's configuration domain object to DTO mapper.
 * <p>
 * <p>
 * This class contains mappers method from aircraft configuration domain object {@link Aircraft}
 * to all particular DTO objects.
 * <p>
 * <p>
 * NOTE: whole class and their methods are package-private and accessible from the service package only.
 *
 * @author Lucia Kleinova
 */
public final class AircraftDtoMapper {

    /**
     * Map general information about aircraft configuration from {@link Aircraft} to {@link AircraftDto}.
     *
     * @param aircraft {@link Aircraft}
     * @return {@link AircraftDto} contained general information about aircraft configuration
     */
    public static AircraftDto mapGeneralInfoToAircraftDto(final Aircraft aircraft) {

        Assert.notNull(aircraft, "Aircraft configuration cannot be null!");

        return initializeGeneralAircraftDtoBuilder(aircraft)
                .build();
    }

    /**
     * Map detailed information about aircraft configuration from {@link Aircraft} to {@link AircraftDto}.
     *
     * @param aircraft {@link Aircraft}
     * @return {@link AircraftDto} contained detailed information about aircraft configuration
     */
    static AircraftDto mapDetailedInfoToAircraftDto(final Aircraft aircraft) {

        Assert.notNull(aircraft, "Aircraft configuration cannot be null!");

        return initializeGeneralAircraftDtoBuilder(aircraft)
                .build();
    }

    private static AircraftDto.Builder initializeGeneralAircraftDtoBuilder(final Aircraft aircraft) {

        Assert.notNull(aircraft, "Aircraft configuration cannot be null!");

        return new AircraftDto.Builder(aircraft.getRegistrationMark())
                .withModelNumber(aircraft.getModelNumber())
                .withVersion(aircraft.getVersion())
                .withCfgCode(aircraft.getCfgCode());
    }

}
