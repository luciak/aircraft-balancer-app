package org.luciak.acb.services.flight;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import org.luciak.acb.api.xmlutils.LocalDateAdapter;
import org.luciak.acb.domain.flight.Sex;
import org.springframework.util.Assert;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.time.LocalDate;

/**
 * Passenger data transer object.
 *
 * @author Lucia Kleinova
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonDeserialize(builder = PaxDto.Builder.class)
@XmlRootElement(name = "pax")
public class PaxDto {

    /* **************************************************************************************************************
     * CLASS MEMBERS
     * **************************************************************************************************************/

    /**
     * Passenger name.
     */
    @XmlElement(name = "name")
    private String name;

    /**
     * Passenger surname.
     */
    @XmlElement(name = "surname")
    private String surname;

    /**
     * Passenger sex.
     */
    @XmlElement(name = "sex")
    private Sex sex;

    /**
     * Passenger code ID which defines type of passenger and his restrictions.
     */
    @XmlElement(name = "code_id")
    private String codeId;

    /**
     * ID of passengers group.
     */
    @XmlElement(name = "group_id")
    private Integer groupId;

    /**
     * ID number of passenger (ID card or passport).
     */
    @XmlElement(name = "id_number")
    private String idNumber;

    /**
     * Passenger birth date.
     */
    @XmlElement(name = "birth_date")
    @XmlJavaTypeAdapter(LocalDateAdapter.class)
    private LocalDate birthDate;

    /**
     * Frequent flyer travel card number.
     */
    @XmlElement(name = "FFT")
    private String FFT;

    /**
     * All legs of the flight.
     */
    @XmlElement(name = "segment")
    private String segment;

    /**
     * Seat number.
     */
    private String seat;

    /**
     * Predicated seat.
     */
    private boolean predicatedSeat;

    /**
     * Seat section
     */
    private String seatSection;

    /**
     * Baggage compartment.
     */
    private String baggageComp;

    /**
     * Baggage weight.
     */
    private Integer baggageWeight;


    /* **************************************************************************************************************
     * CONVENTIONAL METHODS (getters, constructors, hashCode, equals etc.)
     * **************************************************************************************************************/

    /**
     * Get passenger name.
     *
     * @return passenger name
     */
    public String getName() {
        return name;
    }

    /**
     * Get passenger surname.
     *
     * @return passenger surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Get passenger sex.
     *
     * @return passenger sex.
     */
    public Sex getSex() {
        return sex;
    }

    /**
     * Get passenger code id which defines type of passenger.
     *
     * @return passenger code id
     */
    public String getCodeId() {
        return codeId;
    }

    /**
     * Get group ID of passenger.
     *
     * @return group ID
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * Get passenger identification number (ID or Passport number).
     *
     * @return identification number
     */
    public String getIdNumber() {
        return idNumber;
    }

    /**
     * Get passenger birth date.
     *
     * @return passenger birth date
     */
    public LocalDate getBirthDate() {
        return birthDate;
    }

    /**
     * Get Frequent Flyer Traveler card number.
     *
     * @return Frequent Flyer Traveler card number
     */
    public String getFFT() {
        return FFT;
    }

    /**
     * Get all legs of the flight.
     *
     * @return all legs of the flight
     */
    public String getSegment() {
        return segment;
    }

    /**
     * Get seat number.
     *
     * @return seat number.
     */
    public String getSeat() {
        return seat;
    }

    /**
     * Get predicted seat.
     *
     * @return predicted seat
     */
    public boolean isPredicatedSeat() {
        return predicatedSeat;
    }

    /**
     * Get seat section.
     *
     * @return seat section
     */
    public String getSeatSection() {
        return seatSection;
    }

    /**
     * Get baggage compartment.
     *
     * @return baggage compartment
     */
    public String getBaggageComp() {
        return baggageComp;
    }

    /**
     * Get baggage weight.
     *
     * @return baggage weight
     */
    public Integer getBaggageWeight() {
        return baggageWeight;
    }

    /**
     * Private constructor for internal using of JAXB unmarshaller.
     */
    @SuppressWarnings("unused")
    private PaxDto() {
    }

    private PaxDto(Builder builder) {

        this.name = builder.name;
        this.surname = builder.surname;
        this.sex = builder.sex;
        this.idNumber = builder.idNumber;
        this.codeId = builder.codeId;
        this.groupId = builder.groupId;
        this.birthDate = builder.birthDate;
        this.FFT = builder.FFT;
        this.segment = builder.segment;
        this.seat = builder.seat;
        this.predicatedSeat = builder.predicatedSeat;
        this.seatSection = builder.seatSection;
        this.baggageComp = builder.baggageComp;
        this.baggageWeight = builder.baggageWeight;
    }


    /* **************************************************************************************************************
     * OBJECT BUILDER
     * **************************************************************************************************************/

    /**
     * Pax DTO builder.
     */
    @JsonPOJOBuilder
    static class Builder {

        private final String name;

        private final String surname;

        private final Sex sex;

        private final String idNumber;

        private String codeId;

        private Integer groupId;

        private LocalDate birthDate;

        private String FFT;

        private String segment;

        private String seat;

        private boolean predicatedSeat;

        private String seatSection;

        private String baggageComp;

        private Integer baggageWeight;

        Builder(String name, String surname, Sex sex, String idNumber) {

            this.name = name;
            this.surname = surname;
            this.sex = sex;
            this.idNumber = idNumber;
        }

        Builder withCodeId(final String codeId) {

            this.codeId = codeId;
            return this;
        }

        Builder withGroupId(final Integer groupId) {

            this.groupId = groupId;
            return this;
        }

        Builder withBirtDate(final LocalDate birthDate) {

            this.birthDate = birthDate;
            return this;
        }

        Builder withFFT(final String FFT) {

            this.FFT = FFT;
            return this;
        }

        Builder withSegment(final String segment) {

            this.segment = segment;
            return this;

        }

        Builder withSeat(final String seat) {

            this.seat = seat;
            return this;
        }

        Builder withPredicatedSeat(final boolean predicatedSeat) {

            this.predicatedSeat = predicatedSeat;
            return this;
        }

        Builder withSeatSection(final String seatSection) {

            this.seatSection = seatSection;
            return this;
        }

        Builder withBaggageComp(final String baggageComp) {

            this.baggageComp = baggageComp;
            return this;
        }

        Builder withBaggageWeight(final Integer baggageWeight) {

            Assert.isTrue(baggageWeight < 100, "Baggage weight cannot be higher then 99kg!");

            this.baggageWeight = baggageWeight;
            return this;
        }

        PaxDto build() {

            return new PaxDto(this);
        }
    }
}
