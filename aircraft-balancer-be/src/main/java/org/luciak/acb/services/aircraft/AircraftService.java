package org.luciak.acb.services.aircraft;

import org.apache.commons.lang3.StringUtils;
import org.luciak.acb.exceptions.AircraftNotFoundException;
import org.luciak.acb.repositories.aircraft.AircraftRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Aircraft configuration service.
 *
 * @author Lucia Kleinova
 */
@Service
public class AircraftService {

    /**
     * Aircraft repository managing information about aircraft configurations.
     */
    private final AircraftRepository aircraftRepository;

    @Autowired
    public AircraftService(final AircraftRepository aircraftRepository) {

        this.aircraftRepository = aircraftRepository;
    }

    /**
     * Find all available aircraft configurations.
     *
     * @return list of summary information about available aircraft configurations {@link AircraftDto}
     */
    @Transactional
    public List<AircraftDto> findAircraftConfigurations() {

        return aircraftRepository.findAircraftConfigurations()
                .stream()
                .map(AircraftDtoMapper::mapGeneralInfoToAircraftDto)
                .collect(Collectors.toList());
    }

    /**
     * Get detail of aircraft configuration.
     *
     * @param registrationMark registration mark of aircraft
     * @return detail of aircraft configuration {@link AircraftDto}
     * @throws AircraftNotFoundException in case when aircraft doesn't exists
     */
    @Transactional
    public AircraftDto getAircraftConfiguration(final String registrationMark) throws AircraftNotFoundException {

        Assert.isTrue(StringUtils.isNotBlank(registrationMark),
                "Registration mark of aircraft cannot be null or empty!");

        return AircraftDtoMapper.mapDetailedInfoToAircraftDto(
                aircraftRepository.getAircraftConfiguration(registrationMark));
    }

    /**
     * Get aircraft seat map SVG.
     *
     * @param registrationMark aircraft registration mark
     * @return aircraft seat map SVG
     * @throws AircraftNotFoundException in case when aircraft not found
     */
    @Transactional
    public String getSeatMapSvg(final String registrationMark) throws AircraftNotFoundException {

        Assert.isTrue(StringUtils.isNotBlank(registrationMark),
                "Registration mark of aircraft cannot be null or empty!");

        return aircraftRepository.getAircraftConfiguration(registrationMark).getSeatMapSvg();
    }
}
