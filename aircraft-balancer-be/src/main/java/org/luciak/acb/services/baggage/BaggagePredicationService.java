package org.luciak.acb.services.baggage;

/**
 * Interface of baggage predication service
 */
public interface BaggagePredicationService {

    /**
     * Get predicated baggage weight.
     *
     * @return predicated baggage weight
     */
    int predicatedBaggageWeight();
}
