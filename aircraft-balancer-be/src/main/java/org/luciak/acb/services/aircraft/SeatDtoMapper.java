package org.luciak.acb.services.aircraft;

import org.luciak.acb.domain.aircraft.Seat;
import org.luciak.acb.services.flight.PaxDtoMapper;
import org.springframework.util.Assert;

public final class SeatDtoMapper {

    public static SeatDto convertToSeatDto(final Seat seat) {

        Assert.notNull(seat, "Seat cannot be null!");

        return new SeatDto.Builder(seat.getNumber(),
                seat.getSeatOrder(),
                seat.getSection(),
                seat.getRestriction())
                .withPax(seat.getPax() != null ? PaxDtoMapper.convertPaxToPaxDto(seat.getPax()) : null)
                .build();
    }
}
