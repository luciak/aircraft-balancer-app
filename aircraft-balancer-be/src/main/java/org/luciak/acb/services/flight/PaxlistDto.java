package org.luciak.acb.services.flight;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import java.util.List;

@XmlRootElement(name = "passengers")
@XmlSeeAlso(PaxDto.class)
public class PaxlistDto {

    @XmlElement(name = "pax")
    private List<PaxDto> paxlist;

    List<PaxDto> getPaxlist() {
        return paxlist;
    }
}
