package org.luciak.acb.services.flight;

import org.luciak.acb.domain.aircraft.Aircraft;
import org.luciak.acb.domain.flight.Flight;
import org.luciak.acb.domain.flight.FlightInfo;
import org.luciak.acb.services.aircraft.AircraftDtoMapper;
import org.springframework.util.Assert;

/**
 * Flight configuration domain object to DTO mapper.
 * <p>
 * <p>
 * This class contains mappers method from flight configuration domain object {@link Flight}
 * to all particular DTO objects.
 * <p>
 * <p>
 * NOTE: whole class and their methods are package-private and accessible from the service package only.
 *
 * @author Lucia Kleinova
 */
final class FlightDtoMapper {

    /**
     * Map flight meta data from flight information domain object to flight DTO.
     *
     * @param flightInfo {@link FlightInfo}
     * @return flight metadata in {@link FlightDto}
     */
    static FlightDto mapFlightInfoToFlightDto(FlightInfo flightInfo) {

        Assert.notNull(flightInfo, "Flight information cannot be null!");

        return new FlightDto.Builder(
                AircraftDtoMapper.mapGeneralInfoToAircraftDto(
                        new Aircraft.Builder(flightInfo.getAircraftRegistrationMark()).build()),
                flightInfo.getFlightNumber(),
                flightInfo.getOriginDate(),
                flightInfo.getDepartureAirport(),
                flightInfo.getArrivalAirport())
                .withUfi(flightInfo.getUfi())
                .withRepeatNumber(flightInfo.getRepeatNumber())
                .withStatus(flightInfo.getStatus())
                .build();
    }

    static FlightDto mapFlightToFlightDto(Flight flight) {

        Assert.notNull(flight, "Flight cannot be null!");

        return new FlightDto.Builder(
                AircraftDtoMapper.mapGeneralInfoToAircraftDto(flight.getAircraft()),
                flight.getFlightNumber(),
                flight.getOriginDate(),
                flight.getDepartureAirport(),
                flight.getArrivalAirport())
                .withUfi(flight.getUfi())
                .withRepeatNumber(flight.getRepeatNumber())
                .withPaxSummary(flight.getPaxSummary())
                .withStatus(flight.getStatus())
                .build();
    }
}
