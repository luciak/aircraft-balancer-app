package org.luciak.acb.services.aircraft;

import org.luciak.acb.services.flight.PaxDto;

/**
 * Data transfer object which contains information about seat.
 *
 * @author Lucia Kleinova
 */
public class SeatDto {

    /* **************************************************************************************************************
     * CLASS MEMBERS
     * **************************************************************************************************************/

    /**
     * Seat number.
     */
    private final String number;

    /**
     * Seat order.
     */
    private final Integer seatOrder;

    /**
     * Section of the seat.
     */
    private final String section;

    /**
     * Seat restriction flag.
     */
    private final Boolean restriction;

    /**
     * Passenger occupying the seat.
     */
    private final PaxDto pax;


    /* **************************************************************************************************************
     * CONVENTIONAL METHODS (getters, constructors, hashCode, equals etc.)
     * **************************************************************************************************************/

    /**
     * Get seat number.
     *
     * @return seat number
     */
    public String getNumber() {
        return number;
    }

    /**
     * Get seat order.
     *
     * @return seat order
     */
    public Integer getSeatOrder() {
        return seatOrder;
    }

    /**
     * Get seat section.
     *
     * @return seat section
     */
    public String getSection() {
        return section;
    }

    /**
     * Get seat restriction.
     *
     * @return seat restriction
     */
    public Boolean getRestriction() {
        return restriction;
    }

    /**
     * Get information about passenger who occupied the seat.
     *
     * @return {@link PaxDto} who occupied the seat. Return NULL if seat is empty
     */
    public PaxDto getPax() {
        return pax;
    }

    /* **************************************************************************************************************
     * CONVENTIONAL METHODS (getters, constructors, hashCode, equals etc.)
     * **************************************************************************************************************/

    public SeatDto(Builder builder) {

        this.number = builder.number;
        this.seatOrder = builder.seatOrder;
        this.section = builder.section;
        this.restriction = builder.restriction;
        this.pax = builder.pax;
    }

    /* **************************************************************************************************************
     * OBJECT BUILDER
     * **************************************************************************************************************/

    static class Builder {

        private final String number;

        private final Integer seatOrder;

        private final String section;

        private final Boolean restriction;

        private PaxDto pax;

        Builder(String number, Integer seatOrder, String section, Boolean restriction) {

            this.number = number;
            this.seatOrder = seatOrder;
            this.section = section;
            this.restriction = restriction;
        }

        Builder withPax(PaxDto pax) {

            this.pax = pax;
            return this;
        }

        SeatDto build() {

            return new SeatDto(this);
        }

    }
}
