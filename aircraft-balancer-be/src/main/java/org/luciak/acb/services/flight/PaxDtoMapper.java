package org.luciak.acb.services.flight;

import org.luciak.acb.domain.flight.Pax;
import org.springframework.util.Assert;

/**
 * Pax domain object to DTO mapper.
 * <p>
 * <p>
 * This class contains mappers method from {@link Pax} domain object
 * to all particular DTO objects.
 * <p>
 * <p>
 * NOTE: whole class and their methods are package-private and accessible from the service package only.
 *
 * @author Lucia Kleinova
 */
public final class PaxDtoMapper {

    public static PaxDto convertPaxToPaxDto(final Pax pax) {

        Assert.notNull(pax, "Passenger cannot be null!");

        return new PaxDto.Builder(pax.getName(), pax.getSurname(), pax.getSex(), pax.getIdNumber())
                .withBirtDate(pax.getBirthDate())
                .withCodeId(pax.getCodeId())
                .withGroupId(pax.getGroupId())
                .withFFT(pax.getFFT())
                .withSegment(pax.getSegment())
                .withSeat(pax.getSeat())
                .withPredicatedSeat(pax.isPredictedSeat())
                .withSeatSection(pax.getSeatSection())
                .withBaggageComp(pax.getBaggageComp())
                .withBaggageWeight(pax.getBaggageWeight())
                .build();
    }

}
