package org.luciak.acb.domain.aircraft;

import org.apache.commons.lang3.StringUtils;
import org.luciak.acb.domain.flight.Pax;
import org.luciak.acb.exceptions.NoEmptySeatException;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Aircraft configuration domain object.
 *
 * @author Lucia Kleinova
 */
public final class Aircraft {

    /* **************************************************************************************************************
     * CLASS MEMBERS
     * **************************************************************************************************************/

    /**
     * Unique registration mark of aircraft
     */
    private final String registrationMark;

    /**
     * Model number of aircraft
     */
    private final String modelNumber;

    /**
     * Version of aircraft
     */
    private final String version;

    /**
     * Configuration code of aircraft
     */
    private final String cfgCode;

    /**
     * Seat map SVG.
     */
    private final String seatMapSvg;

    /**
     * Seat map.
     * <p>
     * Key: seat number; Value: {@link Seat}
     */
    private final Map<String, Seat> seatMap;

    /**
     * Compartment map.
     */
    private final List<Comp> compMap;


    /* **************************************************************************************************************
     * DOMAIN LOGIC
     * **************************************************************************************************************/

    /**
     * Merge aircraft seatmap with information about occupied seats from the paxlist.
     * <p>
     * This method is called when the flight domain object is created, loaded or reinitialized.
     * Information about predicated and finally assigned seats will remain unchanged.
     *
     * @param paxlist list of {@link Pax}
     */
    public void initializeSeats(final List<Pax> paxlist) {

        Assert.notNull(paxlist, "Paxlist cannot be null!");

        // clear all previous information about occupied seats
        seatMap.values().forEach(Seat::unassignPax);

        // assign passenger to seat which occupied
        paxlist.forEach(pax -> {

            if (pax.getSeat() != null) {

                seatMap.computeIfPresent(pax.getSeat(), (k, seat) -> {
                    seat.assignPax(pax);
                    return seat;
                });
            }
        });
    }

    /**
     * Predicate seats for the group of passengers.
     * <p>
     * The group will be seated close each other if it is possible.
     *
     * @param groupToSeat list of {@link Pax} in the group
     */
    public void predicateSeatForGroup(final List<Pax> groupToSeat) {

        Assert.notNull(groupToSeat, "Group of passengers cannot be null!");
        Assert.notEmpty(groupToSeat, "Seat map cannot be empty!");

        // create sorted seatmap by seatOrder
        List<Seat> sortedSeatMap = new ArrayList<>(seatMap.values());
        sortedSeatMap.sort(Seat::compareTo);

        // find first empty seat with size of group
        int firstSeatIdx = getFirstFreeSeatIdxForGroup(sortedSeatMap, groupToSeat.size());

        // no empty seat for group has been found, group must be divided
        if (firstSeatIdx == -1) {

            // FIXME
            throw new NoEmptySeatException("There are no empty seats!");
        }

        // assign predicated seat for all passengers of group
        for (int i = 0; i < groupToSeat.size(); i++) {

            assignPredicatedSeatForPax(groupToSeat.get(i), sortedSeatMap.get(firstSeatIdx + i));
        }
    }

    /**
     * Assign seat for pax and vice versa.
     *
     * @param seat {@link Seat} to be assigned for pax
     * @param pax
     */
    private void assignPredicatedSeatForPax(final Pax pax, final Seat seat) {

        Assert.notNull(pax, "Pax cannot be empty!");
        Assert.notNull(seat, "Seat cannot be null!");

        pax.assignPredicatedSeat(seat.getNumber(), seat.getSection());
        seat.assignPax(pax);
    }

    /**
     * Find first index of seat in sorted seat map where is possible to seat whole group of passenger.
     *
     * @param sortedSeatMap sorted seatmap
     * @param groupSize     size of passegers group
     * @return index of first seat from sorted seatmap or -1 if no free seats for whole group has been found
     */
    private int getFirstFreeSeatIdxForGroup(final List<Seat> sortedSeatMap, final int groupSize) {

        Assert.notNull(sortedSeatMap, "Seatmap cannot be null!");
        Assert.notEmpty(sortedSeatMap, "Seatmap cannot be empty!");
        Assert.isTrue(groupSize > 0, "Group size must be greater than 0!");

        for (int i = 0; i < sortedSeatMap.size(); i++) {

            // move to the first free seat
            if (sortedSeatMap.get(i).getPax() != null) {

                continue;
            }

            // check if there are some space to the end of aircraft
            if (i + groupSize > sortedSeatMap.size()) {

                return -1;
            }

            // check if there are following free seats for group
            boolean isFreeForGroup = true;
            for (int offset = 1; offset < groupSize; offset++) {

                // occupied following seat
                if (sortedSeatMap.get(i + offset).getPax() != null) {

                    isFreeForGroup = false;
                    break;
                }
            }

            // return located first seat index
            if (isFreeForGroup) {

                return i;
            }
        }
        return -1;
    }

    /* **************************************************************************************************************
     * CONVENTIONAL METHODS (getters, constructors, hashCode, equals etc.)
     * **************************************************************************************************************/

    /**
     * Get unique registration mark of aircraft.
     *
     * @return unique registration mark of aircraft
     */
    public String getRegistrationMark() {
        return registrationMark;
    }

    /**
     * Get model number of aircraft.
     *
     * @return model number of aircraft
     */
    public String getModelNumber() {
        return modelNumber;
    }

    /**
     * Get version of aircraft.
     *
     * @return version of aircraft
     */
    public String getVersion() {
        return version;
    }

    /**
     * Get configuration code of aircraft.
     *
     * @return configuration code of aircraft
     */
    public String getCfgCode() {
        return cfgCode;
    }

    /**
     * Get seat map SVG.
     *
     * @return seat map SVG
     */
    public String getSeatMapSvg() {
        return seatMapSvg;
    }

    /**
     * Get seat map. Key: seat number; Value: {@link Seat}
     *
     * @return seat map
     */
    public Map<String, Seat> getSeatMap() {
        return seatMap;
    }

    /**
     * Get compartment map.
     *
     * @return compartment map
     */
    public List<Comp> getCompMap() {
        return compMap;
    }

    /**
     * Private Constructor. Use Aircraft.Builder instead.
     *
     * @param builder {@link Aircraft.Builder}
     */
    private Aircraft(final Builder builder) {

        this.registrationMark = builder.registrationMark;
        this.modelNumber = builder.modelNumber;
        this.version = builder.version;
        this.cfgCode = builder.cfgCode;
        this.seatMapSvg = builder.seatMapSvg;
        this.compMap = builder.compMap;

        // initialize seat map
        this.seatMap = new HashMap<>();
        if (builder.seatMap != null) {
            builder.seatMap.stream()
                    .forEach(seat -> this.seatMap.put(seat.getNumber(), seat));
        }
    }


    /* **************************************************************************************************************
     * OBJECT BUILDER
     * **************************************************************************************************************/

    /**
     * Aircraft builder.
     */
    public static class Builder {

        private final String registrationMark;

        private String modelNumber;

        private String version;

        private String cfgCode;

        private String seatMapSvg;

        private List<Seat> seatMap;

        private List<Comp> compMap;

        public Builder(final String registrationMark) {

            Assert.isTrue(StringUtils.isNotBlank(registrationMark), "Registration mark cannot be null or empty!");
            this.registrationMark = registrationMark;
        }

        public Builder withModelNumber(final String modelNumber) {

            this.modelNumber = modelNumber;
            return this;
        }

        public Builder withVersion(final String version) {

            this.version = version;
            return this;
        }

        public Builder withCfgCode(final String cfgCode) {

            this.cfgCode = cfgCode;
            return this;
        }

        public Builder withSeatMapSvg(final String seatMapSvg) {

            this.seatMapSvg = seatMapSvg;
            return this;
        }

        public Builder withSeatMap(final List<Seat> seatMap) {

            this.seatMap = seatMap;
            return this;
        }

        public Builder withCompMap(final List<Comp> compMap) {

            this.compMap = compMap;
            return this;
        }

        public Aircraft build() {

            return new Aircraft(this);
        }
    }
}
