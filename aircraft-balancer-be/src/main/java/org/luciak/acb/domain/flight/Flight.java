package org.luciak.acb.domain.flight;

import org.apache.commons.lang3.StringUtils;
import org.luciak.acb.domain.aircraft.Aircraft;
import org.luciak.acb.domain.aircraft.Comp;
import org.luciak.acb.domain.aircraft.Seat;
import org.luciak.acb.exceptions.FlightNotFoundException;
import org.luciak.acb.exceptions.IncorrectStateOfFlightException;
import org.luciak.acb.exceptions.NoSpaceLeftInBaggageCompsException;
import org.luciak.acb.exceptions.PaxNotFoundException;
import org.luciak.acb.exceptions.RestrictedSeatException;
import org.luciak.acb.exceptions.SeatIsAlreadyOccupied;
import org.luciak.acb.exceptions.SeatNotFoundException;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public final class Flight {

    /* **************************************************************************************************************
     * CLASS MEMBERS
     * **************************************************************************************************************/

    /**
     * Unique flight identifier according to AIDX specification.
     */
    private final String ufi;

    /**
     * Aircraft.
     */
    private final Aircraft aircraft;

    /**
     * Flight number.
     */
    private final String flightNumber;

    /**
     * UTC scheduled date of departure of a flight.
     */
    private final LocalDate originDate;

    /**
     * Departure airport code (IATA or ICAO).
     */
    private final String departureAirport;

    /**
     * Arrival airport code (IATA or ICAO).
     */
    private final String arrivalAirport;

    /**
     * Optional repeat number.
     */
    private final Integer repeatNumber;

    /**
     * Flight status.
     */
    private FlightStatus status;

    /**
     * Passenger list.
     */
    private final List<Pax> paxlist;

    /**
     * Passengers summary. This field is calculated.
     */
    private final Map<Sex, Integer> paxSummary;


    /* **************************************************************************************************************
     * DOMAIN LOGIC
     * **************************************************************************************************************/

    /**
     * Import paxlist for flight. Flight must be in state {@link FlightStatus#CREATED}
     * or {@link FlightStatus#PREPARED}.
     *
     * @param paxlist list of {@link Pax}
     * @return flight with imported paxlist
     * @throws IncorrectStateOfFlightException in case when flight is in incorrect state
     */
    public Flight importPaxlist(final List<Pax> paxlist) throws IncorrectStateOfFlightException {

        Assert.notNull(paxlist, "Passenger list cannot be null!");
        Assert.notEmpty(paxlist, "Passenger list cannot be emtpy!");

        if (!FlightStatus.CREATED.equals(status) && !FlightStatus.PREPARED.equals(status)) {
            throw new IncorrectStateOfFlightException(
                    "There is possible to upload paxlist only for the flight in CREATED or PREPARED state!");
        }

        addPaxlist(paxlist);
        this.status = FlightStatus.PREPARED;

        return this;
    }

    /**
     * Remove existing passenger list. Flight must be in state {@link FlightStatus#CREATED}
     * or {@link FlightStatus#PREPARED}.
     *
     * @throws IncorrectStateOfFlightException in case when flight is in incorrect state
     */
    public Flight deletePaxlist() throws IncorrectStateOfFlightException {

        if (!FlightStatus.CREATED.equals(status) && !FlightStatus.PREPARED.equals(status)) {
            throw new IncorrectStateOfFlightException(
                    "There is possible to upload paxlist only for the flight in CREATED or PREPARED state!");
        }

        clearPaxlist();
        this.status = FlightStatus.CREATED;

        return this;
    }

    /**
     * Confirm uploaded paxlist and change state of the flight to PROCESSING.
     * Original status must be PREPARED.
     *
     * @return updated flight
     * @throws IncorrectStateOfFlightException in case when flight is in incorrect state
     */
    public Flight confirmPaxlist() throws IncorrectStateOfFlightException {

        if (!FlightStatus.PREPARED.equals(status)) {

            throw new IncorrectStateOfFlightException(
                    "There is possible to confirm paxlist only for the flight in PREPARED state!");
        }

        // predicate passenger seats
        predicatePassengerSeats();

        // predicate baggage compartments for passengers
        predicatePassengerCompartment();

        // change flight status
        this.status = FlightStatus.PROCESSING;

        return this;
    }

    /**
     * Assign final seat for passengers and update his baggage information, codeId and FFT.
     * <p>
     * If passenger changed the seat, information about old seat will have to be cleared
     * and seating prediction process will have to be re-run.
     *
     * @param idNumber      identification number (number of ID card or passport) [required]
     * @param seatNumber    seat number [required]
     * @param codeId        code id [optional]
     * @param FFT           frequent flyer traveler number [optional]
     * @param baggageWeight baggage weight [optional]
     * @return updated flight
     * @throws IncorrectStateOfFlightException in case when the flight is in incorrect state
     * @throws PaxNotFoundException            in case when pax which should be confirmed doesn't exist
     * @throws SeatNotFoundException           in case when seat where the pax should be seated doesn't exist
     */
    public Pax confirmPax(final String idNumber,
                          final String seatNumber,
                          final String codeId,
                          final String FFT,
                          final Integer baggageWeight)
            throws IncorrectStateOfFlightException, PaxNotFoundException, SeatNotFoundException {

        Assert.isTrue(StringUtils.isNotBlank(idNumber), "ID number of passenger cannot be null or blank!");
        Assert.isTrue(StringUtils.isNotBlank(seatNumber), "Seat number cannot be null or blank!");

        if (!FlightStatus.PROCESSING.equals(status)) {

            throw new IncorrectStateOfFlightException(
                    "There is possible to confirm passengers seats and baggage only for the flight in PROCESSING state");
        }

        // locate passenger
        Pax pax = this.paxlist.stream()
                .filter(item -> idNumber.equals(item.getIdNumber()))
                .findFirst()
                .orElseThrow(() -> new PaxNotFoundException(
                        String.format("Passenger with idNumber '%s' doesn't exist!", idNumber)));

        // locate final seat
        Seat finalSeat = this.getAircraft().getSeatMap().get(seatNumber);
        if (finalSeat == null) {
            throw new SeatNotFoundException(String.format("Seat with number '%s' doesn't exist!", seatNumber));
        }

        // final seat is restricted and passenger is restricted too
        if (finalSeat.getRestriction() && pax.isRestricted()) {
            throw new RestrictedSeatException("Restricted passenger cannot occupied the restricted seat!");
        }

        // locate original seat if has been assigned or predicated before
        Seat originalSeat =
                pax.getSeat() != null
                        ? this.getAircraft().getSeatMap().get(pax.getSeat())
                        : null;

        // detect if seat has been changed, if yes, seating predication process will have to be re-run
        boolean seatHasBeenChanged = originalSeat == null || !originalSeat.getNumber().equals(finalSeat.getNumber());

        // final seat is already occupied by another passenger
        if (seatHasBeenChanged && finalSeat.getPax() != null && !finalSeat.getPax().isPredictedSeat()) {
            throw new SeatIsAlreadyOccupied(
                    String.format("Seat number '%s' is already occupied by another passenger!", seatNumber));
        }

        // update pax information
        // there is not needed to update previous predicated passenger on this seat,
        // this information will be updated during seating predication process
        pax.confirm(finalSeat.getNumber(), finalSeat.getSection(), codeId, FFT, baggageWeight);

        // re-run seating prediction process if needed or just update the pax on the seat
        if (seatHasBeenChanged) {

            // re-run seating prediction
            // this method unassign all predicted seats from paxlist,
            // clear current seatmap configuration and predicate new seat for each not confirmed pax
            predicatePassengerSeats();
        } else {

            // update seat information
            // there is not needed to updated original seat,
            // this information will be updated during seating predication process
            finalSeat.assignPax(pax);
        }

        // detect if package weight has been changed and
        // re-run baggage loading predication process if needed or do nothing
        if (pax.getBaggageWeight() == baggageWeight) {

            // re-run baggage loading predication process
            predicatePassengerCompartment();
        }

        return pax;
    }

    /**
     * Assign passenger list for the flight. This method is called internally
     * when existing flight with paxlist is loaded or when the new paxlist for the flight is imported from API.
     *
     * @param paxlist list of {@link Pax}
     */
    private void addPaxlist(final List<Pax> paxlist) {

        Assert.notNull(paxlist, "Passenger list cannot be null!");

        clearPaxlist();

        this.paxlist.addAll(paxlist);
        this.paxlist.forEach(pax -> {

            Assert.notNull(pax, "Passenger cannot be null!");
            paxSummary.put(pax.getSex(), paxSummary.get(pax.getSex()) + 1);
        });
    }

    /**
     * Remove existing passenger list from the flight.
     * <p>
     * This method is called internally when paxlist is re-imported from API or when paxlist is deleted.
     */
    private void clearPaxlist() {

        this.paxlist.clear();

        // clean pax summary
        paxSummary.keySet().forEach(sex -> paxSummary.put(sex, 0));
    }

    /**
     * Predicate passenger seats and baggage compartment.
     * <p>
     * This method is called internally when the imported paxlist is confirmed for flight.
     * There is possible to call this method repeatedly.
     * The old predicated seat will be cleared before predication process.
     * The reserved seat will remain unchanged.
     */
    private void predicatePassengerSeats() {

        // unassign predicated seats from paxlist; reserved seat will remain unchanged
        paxlist.forEach(Pax::unassignPredicatedSeat);

        // reinitialize predicated seats from aircraft and pax before automatic seating process.
        getAircraft().initializeSeats(paxlist);

        // initialize map of unseated group (key = pax.groupId; values are list of Pax)
        final Map<Integer, List<Pax>> unseatedGroups = new HashMap<>();
        paxlist.forEach(pax -> {

            // add pax into the unseated groups, if the pax has already assigned final or predicated seat, skip him
            if (pax.getSeat() == null) {

                // if the group doesn't contain any pax, create a new empty list
                unseatedGroups.computeIfAbsent(pax.getGroupId(), groupId -> new ArrayList<>())
                        // add pax to list of unseated pax in group
                        .add(pax);
            }
        });

        // finish if no groups have been found
        if (unseatedGroups.isEmpty()) {

            return;
        }

        // at first, try to seat restricted pax with whole group
        Iterator<Integer> groupIterator = unseatedGroups.keySet().iterator();
        while (groupIterator.hasNext()) {

            // assign seats for whole group
            getAircraft().predicateSeatForGroup(unseatedGroups.get(groupIterator.next()));
            // remove group from the unseated group map
            groupIterator.remove();
        }
    }

    /**
     * Predicate baggage compartment for all passengers.
     */
    private void predicatePassengerCompartment() {

        // clear previous baggage predication
        paxlist.forEach(Pax::unassignPredicatedBaggage);

        List<Pax> unpredicatedPax = paxlist.stream().collect(Collectors.toList());

        // run over all aircraft compartments
        for (Comp comp : aircraft.getCompMap()) {

            int compWeight = 0;

            // go over all passengers
            Iterator<Pax> iterator = unpredicatedPax.iterator();
            while (iterator.hasNext()) {

                // get pax
                Pax pax = iterator.next();

                // check if there are enough free space in compartment
                if (compWeight + pax.getBaggageWeight() > comp.getMaxCapacity()) {

                    // if not, select next compartment to baggage loading
                    break;
                }

                // add baggage in compartment
                compWeight += pax.getBaggageWeight();
                // assign baggage compartment to passenger
                pax.assignPredicatedBaggageComp(comp.getNumber());
                // remove predicated pax from the unpredicatedPax list
                iterator.remove();
            }
        }

        // there is no space left for loading baggages
        if (unpredicatedPax.size() > 0) {
            throw new NoSpaceLeftInBaggageCompsException("There is no space left in compartments to load all baggage!");
        }
    }

    /**
     * Calculate and return actual section map which contains sum of passenger weight for each sections.
     * <p>
     * Key: section ID, Value: sum of passengers weight in the section
     *
     * @return section map
     */
    public Map<String, Integer> getSectionWeightMap() {

        Map<String, Integer> sectionWeightMap = new HashMap<>();
        this.paxlist.forEach(pax -> {
            sectionWeightMap.computeIfAbsent(pax.getSeatSection(), (key) -> Integer.valueOf(0));
            sectionWeightMap.computeIfPresent(pax.getSeatSection(), (key, value) -> value + pax.getSex().getWeight());
        });
        return sectionWeightMap;
    }

    /**
     * Calculate and return actual compartment map which contains sum of baggage weight for each compartments.
     * <p>
     * Key: compartment ID, Value: sum of baggage weight in the compartment
     *
     * @return section map
     */
    public Map<String, Integer> getCompWeightMap() {

        Map<String, Integer> compWeightMap = new HashMap<>();
        this.paxlist.forEach(pax -> {
            if (pax.getBaggageComp() != null) {
                compWeightMap.computeIfAbsent(pax.getBaggageComp(), (key) -> Integer.valueOf(0));
                compWeightMap.computeIfPresent(pax.getBaggageComp(), (key, value) -> value + pax.getBaggageWeight());
            }
        });
        return compWeightMap;
    }

    /**
     * Close the flight and remove all unassigned passengers from the pax list.
     * <p>
     * Flight must be in {@link FlightStatus#BALANCED} or {@link FlightStatus#PROCESSING} state.
     *
     * @throws IncorrectStateOfFlightException in case when flight is in incorrect state.
     */
    public void closeFlight() throws IncorrectStateOfFlightException {

        if (!FlightStatus.PROCESSING.equals(status) && !FlightStatus.BALANCED.equals(status)) {

            throw new IncorrectStateOfFlightException(
                    "There is possible to close fight only for the flight in PROCESSING or BALANCED state");
        }

        // remove all non assigned passengers from the paxlist
        List<Pax> clearedPaxList = paxlist.stream()
                .filter(pax -> !pax.isPredictedSeat())
                .collect(Collectors.toList());

        // update paxlist
        addPaxlist(clearedPaxList);

        // finalize passenger seats
        predicatePassengerSeats();

        // finalize baggage compartments for passengers
        predicatePassengerCompartment();

        // close the flight
        status = FlightStatus.CLOSED;
    }

    /* **************************************************************************************************************
     * CONVENTIONAL METHODS (getters, constructors, hashCode, equals etc.)
     * **************************************************************************************************************/

    /**
     * Get Unique Flight Identifier.
     *
     * @return unique flight identifier
     */
    public String getUfi() {
        return ufi;
    }

    /**
     * Get aircraft.
     *
     * @return aircraft
     */
    public Aircraft getAircraft() {
        return aircraft;
    }

    /**
     * Get aircraft flight number using format:
     * <p>
     * <ul>
     * <li>Airline code (IATA or ICAO)</li>
     * <li>Flight number (IATA)</li>
     * <li>Operational suffix (optional)</li>
     * </ul>
     *
     * @return aircraft flight number
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Get UTC origin departure date of a flight.
     *
     * @return origin departure date of a flight
     */
    public LocalDate getOriginDate() {
        return originDate;
    }

    /**
     * Get departure airport code (IATA or ICAO).
     *
     * @return departure airport code
     */
    public String getDepartureAirport() {
        return departureAirport;
    }

    /**
     * Get arrival airport code (IATA or ICAO).
     *
     * @return arrival airport code
     */
    public String getArrivalAirport() {
        return arrivalAirport;
    }

    /**
     * Get repeat number (optional).
     *
     * @return repeat number
     */
    public Integer getRepeatNumber() {
        return repeatNumber;
    }

    /**
     * Get paxlist.
     *
     * @return List of {@link Pax}
     */
    public List<Pax> getPaxlist() {
        return paxlist;
    }

    /**
     * Get paxlist summary.
     *
     * @return paxlist summary
     */
    public Map<Sex, Integer> getPaxSummary() {
        return paxSummary;
    }

    /**
     * Get flight status {@link FlightStatus}
     *
     * @return flight status
     */
    public FlightStatus getStatus() {
        return status;
    }

    /**
     * Private constructor. Use Flight.Builder instead.
     *
     * @param builder {@link Flight.Builder}
     */
    private Flight(Flight.Builder builder) {

        this.ufi = builder.ufi;
        this.aircraft = builder.aircraft;
        this.flightNumber = builder.flightNumber;
        this.originDate = builder.originDate;
        this.departureAirport = builder.departureAirport;
        this.arrivalAirport = builder.arrivalAirport;
        this.repeatNumber = builder.repeatNumber;
        this.status = builder.status;

        // initialize paxSummary
        this.paxSummary = new HashMap<>();
        for (Sex value : Sex.values()) {
            this.paxSummary.put(value, 0);
        }

        // initialize paxlist
        this.paxlist = new ArrayList<>();
        if (builder.paxlist != null) {
            addPaxlist(builder.paxlist);
        }

        // initialize seats
        this.aircraft.initializeSeats(this.paxlist);
    }


    /* **************************************************************************************************************
     * OBJECT BUILDER
     * **************************************************************************************************************/

    /**
     * Flight entity builder.
     */
    public static class Builder {

        private final String ufi;

        private final Aircraft aircraft;

        private final String flightNumber;

        private final LocalDate originDate;

        private final String departureAirport;

        private final String arrivalAirport;

        private Integer repeatNumber;

        private List<Pax> paxlist;

        private FlightStatus status;

        public Builder(final String ufi,
                       final Aircraft aircraft,
                       final String flightNumber,
                       final LocalDate originDate,
                       final String departureAirport,
                       final String arrivalAirport) {

            Assert.isTrue(StringUtils.isNotBlank(ufi),
                    "Unique flight identifier cannot be blank or null!");
            Assert.notNull(aircraft,
                    "Aircraft configuration cannot be null!");
            Assert.isTrue(StringUtils.isNotBlank(flightNumber),
                    "Flight number cannot be blank or null!");
            Assert.notNull(originDate,
                    "Origin date cannot be blank or null!");
            Assert.isTrue(StringUtils.isNotBlank(departureAirport),
                    "Departure airport code cannot be blank or null!");
            Assert.isTrue(StringUtils.isNotBlank(arrivalAirport),
                    "Arrival airport code cannot be blank or null!");

            this.ufi = ufi;
            this.aircraft = aircraft;
            this.flightNumber = flightNumber;
            this.originDate = originDate;
            this.departureAirport = departureAirport;
            this.arrivalAirport = arrivalAirport;
        }

        public Builder withRepeatNumber(Integer repeatNumber) {

            this.repeatNumber = repeatNumber;
            return this;
        }

        public Builder withPaxlist(List<Pax> paxlist) {

            this.paxlist = paxlist;
            return this;
        }

        public Builder withStatus(FlightStatus status) {

            this.status = status;
            return this;
        }

        public Flight build() {

            return new Flight(this);
        }
    }
}
