package org.luciak.acb.domain.flight;

public enum Sex {

    /**
     * Male.
     */
    M(84),

    /**
     * Female.
     */
    F(84),

    /**
     * Child
     */
    CHD(35);

    private Integer weight;

    Sex(Integer weight) {
        this.weight = weight;
    }

    /**
     * Get weight of pax by sex.
     *
     * @return pax weight
     */
    public Integer getWeight() {
        return weight;
    }
}
