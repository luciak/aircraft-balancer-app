package org.luciak.acb.domain.flight;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import java.time.LocalDate;

public final class FlightInfo {

    /* **************************************************************************************************************
     * CLASS MEMBERS
     * **************************************************************************************************************/

    /**
     * Unique flight identifier according to AIDX specification.
     */
    private final String ufi;

    /**
     * Aircraft registration mark.
     */
    private final String aircraftRegistrationMark;

    /**
     * Flight number.
     */
    private final String flightNumber;

    /**
     * UTC scheduled date of departure of a flight.
     */
    private final LocalDate originDate;

    /**
     * Departure airport code (IATA or ICAO).
     */
    private final String departureAirport;

    /**
     * Arrival airport code (IATA or ICAO).
     */
    private final String arrivalAirport;

    /**
     * Optional repeat number.
     */
    private final Integer repeatNumber;

    /**
     * Flight status.
     */
    private final FlightStatus status;


    /* **************************************************************************************************************
     * CONVENTIONAL METHODS (getters, constructors, hashCode, equals etc.)
     * **************************************************************************************************************/

    /**
     * Get Unique Flight Identifier.
     *
     * @return unique flight identifier
     */
    public String getUfi() {
        return ufi;
    }

    /**
     * Get aircraft registration mark.
     *
     * @return aircraft registration mark
     */
    public String getAircraftRegistrationMark() {
        return aircraftRegistrationMark;
    }

    /**
     * Get aircraft flight number using format:
     * <p>
     * <ul>
     * <li>Airline code (IATA or ICAO)</li>
     * <li>Flight number (IATA)</li>
     * <li>Operational suffix (optional)</li>
     * </ul>
     *
     * @return aircraft flight number
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Get UTC origin departure date of a flight.
     *
     * @return origin departure date of a flight
     */
    public LocalDate getOriginDate() {
        return originDate;
    }

    /**
     * Get departure airport code (IATA or ICAO).
     *
     * @return departure airport code
     */
    public String getDepartureAirport() {
        return departureAirport;
    }

    /**
     * Get arrival airport code (IATA or ICAO).
     *
     * @return arrival airport code
     */
    public String getArrivalAirport() {
        return arrivalAirport;
    }

    /**
     * Get repeat number (optional).
     *
     * @return repeat number
     */
    public Integer getRepeatNumber() {
        return repeatNumber;
    }

    /**
     * Get flight status {@link FlightStatus}
     *
     * @return flight status
     */
    public FlightStatus getStatus() {
        return status;
    }

    /**
     * Private constructor. Use Flight.Builder instead.
     *
     * @param builder {@link FlightInfo.Builder}
     */
    private FlightInfo(FlightInfo.Builder builder) {

        this.ufi = builder.ufi;
        this.aircraftRegistrationMark = builder.aircraftRegistrationMark;
        this.flightNumber = builder.flightNumber;
        this.originDate = builder.originDate;
        this.departureAirport = builder.departureAirport;
        this.arrivalAirport = builder.arrivalAirport;
        this.repeatNumber = builder.repeatNumber;
        this.status = builder.status;
    }


    /* **************************************************************************************************************
     * OBJECT BUILDER
     * **************************************************************************************************************/

    /**
     * Flight builder.
     */
    public static class Builder {

        private final String ufi;

        private final String aircraftRegistrationMark;

        private final String flightNumber;

        private final LocalDate originDate;

        private final String departureAirport;

        private final String arrivalAirport;

        private Integer repeatNumber;

        private FlightStatus status;

        public Builder(final String ufi,
                final String aircraftRegistrationMark,
                final String flightNumber,
                final LocalDate originDate,
                final String departureAirport,
                final String arrivalAirport) {

            Assert.isTrue(StringUtils.isNotBlank(ufi),
                    "Unique flight identifier cannot be blank or null!");
            Assert.isTrue(StringUtils.isNotBlank(aircraftRegistrationMark),
                    "Aircraft registration mark cannot be blank or null!");
            Assert.isTrue(StringUtils.isNotBlank(flightNumber),
                    "Flight number cannot be blank or null!");
            Assert.notNull(originDate,
                    "Origin date cannot be blank or null!");
            Assert.isTrue(StringUtils.isNotBlank(departureAirport),
                    "Departure airport code cannot be blank or null!");
            Assert.isTrue(StringUtils.isNotBlank(arrivalAirport),
                    "Arrival airport code cannot be blank or null!");

            this.ufi = ufi;
            this.aircraftRegistrationMark = aircraftRegistrationMark;
            this.flightNumber = flightNumber;
            this.originDate = originDate;
            this.departureAirport = departureAirport;
            this.arrivalAirport = arrivalAirport;
        }

        public Builder withRepeatNumber(final Integer repeatNumber) {

            this.repeatNumber = repeatNumber;
            return this;
        }

        public Builder withStatus(final FlightStatus status) {

            this.status = status;
            return this;
        }

        public FlightInfo build() {

            return new FlightInfo(this);
        }
    }
}
