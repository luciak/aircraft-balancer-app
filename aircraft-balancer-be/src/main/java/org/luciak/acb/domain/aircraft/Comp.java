package org.luciak.acb.domain.aircraft;

public class Comp {

    /* **************************************************************************************************************
     * CLASS MEMBERS
     * **************************************************************************************************************/

    /**
     * Compartment number.
     */
    private final String number;

    /**
     * Max capacity of compartment [kg].
     */
    private final Integer maxCapacity;

    /**
     * Get compartment order which defines loading priorities.
     */
    private final int compOrder;

    /* **************************************************************************************************************
     * CONVENTIONAL METHODS (getters, constructors, hashCode, equals etc.)
     * **************************************************************************************************************/

    /**
     * Get compartment number.
     *
     * @return compartment number
     */
    public String getNumber() {
        return number;
    }

    /**
     * Get max capacity.
     *
     * @return max capacity
     */
    public Integer getMaxCapacity() {
        return maxCapacity;
    }

    /**
     * Get compartment order which defines loading priorities
     *
     * @return compartment order
     */
    public int getCompOrder() {
        return compOrder;
    }

    /**
     * Private Constructor. Use {@link Comp.Builder} instead.
     *
     * @param builder {@link Seat.Builder}
     */
    private Comp(Builder builder) {

        this.number = builder.number;
        this.maxCapacity = builder.maxCapacity;
        this.compOrder = builder.compOrder;
    }


    /* **************************************************************************************************************
     * OBJECT BUILDER
     * **************************************************************************************************************/

    public static class Builder {

        private final String number;

        private final Integer maxCapacity;

        private final int compOrder;

        public Builder(final String number, final Integer maxCapacity, final int compOrder) {

            this.number = number;
            this.maxCapacity = maxCapacity;
            this.compOrder = compOrder;
        }

        public Comp build() {

            return new Comp(this);
        }
    }
}
