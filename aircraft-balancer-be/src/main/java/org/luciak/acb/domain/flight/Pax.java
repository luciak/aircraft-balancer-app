package org.luciak.acb.domain.flight;

import org.apache.commons.lang3.StringUtils;
import org.luciak.acb.exceptions.SeatIsAlreadyOccupied;
import org.springframework.util.Assert;

import java.time.LocalDate;

public final class Pax {

    /* **************************************************************************************************************
     * CLASS MEMBERS
     * **************************************************************************************************************/

    /**
     * Passenger name.
     */
    private final String name;

    /**
     * Passenger surname.
     */
    private final String surname;

    /**
     * Passenger sex.
     */
    private final Sex sex;

    /**
     * Passenger code ID which defines type of passenger and his restrictions.
     */
    private String codeId;

    /**
     * ID of passengers group.
     */
    private final Integer groupId;

    /**
     * ID number of passenger (ID card or passport).
     */
    private final String idNumber;

    /**
     * Passenger birth date.
     */
    private final LocalDate birthDate;

    /**
     * Frequent flyer travel card number.
     */
    private String FFT;

    /**
     * All legs of the flight.
     */
    private final String segment;

    /**
     * Seat number.
     */
    private String seat;

    /**
     * Predicted seat.
     */
    private boolean predicatedSeat;

    /**
     * Seat section.
     */
    private String seatSection;

    /**
     * Baggage compartment.
     */
    private String baggageComp;

    /**
     * Baggage weight.
     */
    private int baggageWeight;


    /* **************************************************************************************************************
     * DOMAIN LOGIC
     * **************************************************************************************************************/

    /**
     * Assign seat of the passenger.
     *
     * @param seat seat number
     * @param section section of the seat
     */
    public void assignPredicatedSeat(final String seat, final String section) {

        Assert.isTrue(StringUtils.isNotBlank(seat),
                "Seat cannot be null or blank!");
        Assert.isTrue(StringUtils.isNotBlank(section),
                "Sections of seat cannot be null or blank!");

        if (!predicatedSeat && this.seat != null) {

            throw new SeatIsAlreadyOccupied(
                    String.format("Seat '%s' is already occupied by other passenger!", seat));
        }

        this.seat = seat;
        this.seatSection = section;
        this.predicatedSeat = true;
    }

    /**
     * Unassign predicated seat of the passenger.
     */
    public void unassignPredicatedSeat() {

        if (predicatedSeat) {

            this.seat = null;
            this.seatSection = null;
        }
    }

    public void assignPredicatedBaggageComp(final String compartment) {

        Assert.isTrue(StringUtils.isNotBlank(compartment), "Baggage compartment cannot be null or blank!");

        this.baggageComp = compartment;
    }

    public void unassignPredicatedBaggage() {

        this.baggageComp = null;
    }

    /**
     * Confirm and update information about passenger and his seat and baggage weight.
     *
     * @param seatNumber final seat number
     * @param section final seat section
     * @param codeId final codeId
     * @param FFT final frequent flyer traveler card number
     * @param baggageWeight final baggage wight
     */
    public void confirm(final String seatNumber, final String section,
                        final String codeId, final String FFT,
                        final Integer baggageWeight) {

        Assert.isTrue(StringUtils.isNotBlank(seatNumber), "Seat number cannot be null or blank!");
        Assert.isTrue(StringUtils.isNotBlank(section), "Seat section cannot be null or blank!");

        this.seat = seatNumber;
        this.seatSection = section;
        this.predicatedSeat = false;
        this.codeId = codeId;
        this.FFT = FFT;
        this.baggageWeight = baggageWeight;
    }

    /**
     * Calculate restricted flag of the passenger. If the passenger has restricted flat,
     * he cannot occupied restricted seat.
     *
     * @return restricted flag
     */
    public boolean isRestricted() {

        if (StringUtils.isNotBlank(this.codeId) || Sex.CHD.equals(this.sex)) {

            return true;
        }

        return false;
    }


    /* **************************************************************************************************************
     * CONVENTIONAL METHODS (getters, constructors, hashCode, equals etc.)
     * **************************************************************************************************************/

    /**
     * Get passenger name.
     *
     * @return passenger name
     */
    public String getName() {
        return name;
    }

    /**
     * Get passenger surname.
     *
     * @return passenger surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Get passenger sex.
     *
     * @return passenger sex
     */
    public Sex getSex() {
        return sex;
    }

    /**
     * Get code ID of passenger which defines type of passenger.
     *
     * @return code ID
     */
    public String getCodeId() {
        return codeId;
    }

    /**
     * Get group ID of passengers.
     *
     * @return group ID
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * Get ID number of passenger (ID card number or passport number).
     *
     * @return id number
     */
    public String getIdNumber() {
        return idNumber;
    }

    /**
     * Get passenger birth date.
     *
     * @return passenger birth date
     */
    public LocalDate getBirthDate() {
        return birthDate;
    }

    /**
     * Get frequent flyer card number.
     *
     * @return frequent flyer card number
     */
    public String getFFT() {
        return FFT;
    }

    /**
     * Get all legs of the flight.
     *
     * @return all legs of the flight
     */
    public String getSegment() {
        return segment;
    }

    /**
     * Get seat number.
     *
     * @return seat number
     */
    public String getSeat() {
        return seat;
    }

    /**
     * Get flag if seat is predicteded.
     *
     * @return predicted seat flag
     */
    public boolean isPredictedSeat() {
        return predicatedSeat;
    }

    /**
     * Get seat section.
     *
     * @return seat section
     */
    public String getSeatSection() {
        return seatSection;
    }

    /**
     * Get baggage compartment.
     *
     * @return baggage compartmet
     */
    public String getBaggageComp() {
        return baggageComp;
    }

    /**
     * Get baggage weight.
     *
     * @return baggage weight
     */
    public int getBaggageWeight() {
        return baggageWeight;
    }

    /**
     * Private constructor. Use Pax.Builder instead.
     *
     * @param builder {@link Pax.Builder}
     */
    private Pax(Builder builder) {

        this.name = builder.name;
        this.surname = builder.surname;
        this.sex = builder.sex;
        this.codeId = builder.codeId;
        this.groupId = builder.groupId;
        this.idNumber = builder.idNumber;
        this.birthDate = builder.birthDate;
        this.FFT = builder.FFT;
        this.segment = builder.segment;
        this.seat = builder.seat;
        this.predicatedSeat = builder.predicatedSeat;
        this.seatSection = builder.seatSection;
        this.baggageComp = builder.baggageComp;
        this.baggageWeight = builder.baggageWeight;
    }

    @Override
    public String toString() {
        return "Pax{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", idNumber='" + idNumber + '\'' +
                ", seat='" + seat + '\'' +
                ", predicatedSeat='" + predicatedSeat + '\'' +
                ", seatSection='" + seatSection + '\'' +
                ", baggageComp='" + baggageComp + '\'' +
                ", baggageWeight=" + baggageWeight +
                '}';
    }


    /* **************************************************************************************************************
     * OBJECT BUILDER
     * **************************************************************************************************************/

    /**
     * Pax builder.
     */
    public static class Builder {

        private final String name;

        private final String surname;

        private final Sex sex;

        private String codeId;

        private Integer groupId;

        private String idNumber;

        private LocalDate birthDate;

        private String FFT;

        private String segment;

        private String seat;

        private boolean predicatedSeat;

        private String seatSection;

        private String baggageComp;

        private int baggageWeight;

        public Builder(String name, String surname, Sex sex, String idNumber) {

            Assert.isTrue(StringUtils.isNotBlank(name),
                    "Pax name cannot be blank or null!");
            Assert.isTrue(StringUtils.isNotBlank(surname),
                    "Pax surname cannot be blank or null!");
            Assert.notNull((sex),
                    "Pax sex cannot be null!");
            Assert.isTrue(StringUtils.isNotBlank(idNumber),
                    "Pax identification number cannot be blank or null!");

            this.name = name;
            this.surname = surname;
            this.sex = sex;
            this.idNumber = idNumber;
        }

        public Builder withCodeId(final String codeId) {

            this.codeId = codeId;
            return this;
        }

        public Builder withGroupId(final Integer groupId) {

            this.groupId = groupId;
            return this;
        }

        public Builder withBirthDate(final LocalDate birthDate) {

            this.birthDate = birthDate;
            return this;
        }

        public Builder withFFT(final String FFT) {

            this.FFT = FFT;
            return this;
        }

        public Builder withSegment(final String segment) {

            this.segment = segment;
            return this;
        }

        public Builder withSeat(final String seat) {

            this.seat = seat;
            return this;
        }

        public Builder withPredicatedSeat(final boolean predicatedSeat) {

            this.predicatedSeat = predicatedSeat;
            return this;
        }

        public Builder withSeatSection(final String seatSection) {

            this.seatSection = seatSection;
            return this;
        }

        public Builder withBaggageComp(final String baggageComp) {

            this.baggageComp = baggageComp;
            return this;
        }

        public Builder withBaggageWeight(final int baggageWeight) {

            Assert.isTrue(baggageWeight < 100, "Baggage weight cannot be higher then 99kg!");
            this.baggageWeight = baggageWeight;
            return this;
        }

        public Pax build() {

            return new Pax(this);
        }
    }
}
