package org.luciak.acb.domain.aircraft;

import org.luciak.acb.domain.flight.Pax;
import org.springframework.util.Assert;

/**
 * Seat domain object which contains information about seat and passenger which occupied it.
 *
 * @author Lucia Kleinova
 */
public class Seat implements Comparable<Seat> {

    /* **************************************************************************************************************
     * CLASS MEMBERS
     * **************************************************************************************************************/

    /**
     * Seat number.
     */
    private final String number;

    /**
     * Seat order.
     */
    private final Integer seatOrder;

    /**
     * Section of the seat.
     */
    private final String section;

    /**
     * Seat restriction flag.
     */
    private final boolean restriction;

    /**
     * Passenger occupying the seat.
     */
    private Pax pax;


    /* **************************************************************************************************************
     * DOMAIN LOGIC
     * **************************************************************************************************************/

    /**
     * Assign passenger to seat.
     *
     * @param pax {@link Pax}
     */
    public void assignPax(Pax pax) {

        Assert.notNull(pax, "Passenger cannot be null!");

        this.pax = pax;
    }

    /**
     * Unassign passenger from the seat.
     */
    public void unassignPax() {

        this.pax = null;
    }


    /* **************************************************************************************************************
     * CONVENTIONAL METHODS (getters, constructors, hashCode, equals etc.)
     * **************************************************************************************************************/

    /**
     * Get seat number.
     *
     * @return seat number
     */
    public String getNumber() {
        return number;
    }

    /**
     * Get seat order.
     *
     * @return seat order
     */
    public Integer getSeatOrder() {
        return seatOrder;
    }

    /**
     * Get section.
     *
     * @return section
     */
    public String getSection() {
        return section;
    }

    /**
     * Get seat restriction.
     *
     * @return seat restriction
     */
    public boolean getRestriction() {
        return restriction;
    }

    /**
     * Get passenger occupying the seat.
     *
     * @return {@link Pax} occupying the seat
     */
    public Pax getPax() {
        return pax;
    }

    /**
     * Private Constructor. Use {@link Seat.Builder} instead.
     *
     * @param builder {@link Seat.Builder}
     */
    private Seat(Builder builder) {

        this.number = builder.number;
        this.seatOrder = builder.sortOrder;
        this.section = builder.section;
        this.restriction = builder.restriction;
    }

    @Override
    public int compareTo(Seat other) {
        return this.seatOrder.compareTo(other.seatOrder);
    }

    @Override
    public String toString() {
        return "Seat{" +
                "number='" + number + '\'' +
                ", seatOrder=" + seatOrder +
                ", section='" + section + '\'' +
                ", restriction=" + restriction +
                ", pax=" + pax +
                '}';
    }


    /* **************************************************************************************************************
     * OBJECT BUILDER
     * **************************************************************************************************************/

    public static class Builder {

        private final String number;

        private final Integer sortOrder;

        private final String section;

        private final boolean restriction;

        public Builder(String number, Integer sortOrder, String section, boolean restriction) {

            this.number = number;
            this.sortOrder = sortOrder;
            this.section = section;
            this.restriction = restriction;
        }

        public Seat build() {

            return new Seat(this);
        }
    }
}
