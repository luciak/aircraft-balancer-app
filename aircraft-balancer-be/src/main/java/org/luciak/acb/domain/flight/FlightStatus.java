package org.luciak.acb.domain.flight;

public enum FlightStatus {

    /**
     * Particular flight has been created and contains all mandatory flight metadata.
     */
    CREATED,

    /**
     * Particular flight has prepared for balancing process.
     * All information about passengers and their baggage has been uploaded.
     */
    PREPARED,

    /**
     * Balancing process of aircraft is in progress
     */
    PROCESSING,

    /**
     * Aircraft balancing process for the particular flight has been finished
     */
    BALANCED,

    /**
     * Particular flight has been finished
     */
    CLOSED
}
