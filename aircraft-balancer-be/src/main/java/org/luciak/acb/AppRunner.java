package org.luciak.acb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Entry point to the Spring boot application.
 *
 * @author Lucia Kleinova
 */
@SpringBootApplication
public class AppRunner {

    public static void main(String[] args) {

        SpringApplication.run(AppRunner.class, args);
    }
}
