package org.luciak.acb.api;

import org.luciak.acb.exceptions.AircraftNotFoundException;
import org.luciak.acb.services.aircraft.AircraftDto;
import org.luciak.acb.services.aircraft.AircraftService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Aircraft configuration API.
 *
 * @author Lucia Kleinova
 */
@RestController
@RequestMapping("api/aircraft")
public final class AircraftController {

    /**
     * Aircraft configuration service.
     */
    private final AircraftService aircraftService;

    @Autowired
    public AircraftController(final AircraftService aircraftService) {

        this.aircraftService = aircraftService;
    }

    /**
     * Get list of available aircraft configurations.
     *
     * @return list of aircraft configurations
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<AircraftDto> findAircraftConfigurations() {

        return aircraftService.findAircraftConfigurations();
    }

    /**
     * Get detail of the aircraft configuration.
     *
     * @param registrationMark aircraft registration mark
     * @return aircraft configuration detail
     * @throws AircraftNotFoundException in case when aircraft doesn't exists
     */
    @GetMapping(path = "/{registrationMark}", produces = MediaType.APPLICATION_JSON_VALUE)
    public AircraftDto getAircraftConfiguration(@PathVariable("registrationMark") final String registrationMark)
            throws AircraftNotFoundException {

        return aircraftService.getAircraftConfiguration(registrationMark);
    }

    /**
     * Get aircraft seat map SVG.
     *
     * @param registrationMark aircraft registration mark
     * @return aircraft seat map SVG
     * @throws AircraftNotFoundException in case when aircraft not found
     */
    @GetMapping(path = "/{registrationMark}/seatmapsvg", produces = MediaType.APPLICATION_XML_VALUE)
    public String getSeatMapSvg(@PathVariable(name = "registrationMark") final String registrationMark)
            throws AircraftNotFoundException {

        return aircraftService.getSeatMapSvg(registrationMark);
    }
}
