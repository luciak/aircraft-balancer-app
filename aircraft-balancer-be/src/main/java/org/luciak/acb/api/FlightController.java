package org.luciak.acb.api;

import org.luciak.acb.domain.flight.FlightStatus;
import org.luciak.acb.exceptions.AircraftNotFoundException;
import org.luciak.acb.exceptions.FlightAlreadyExistsException;
import org.luciak.acb.exceptions.FlightNotFoundException;
import org.luciak.acb.exceptions.IncorrectStateOfFlightException;
import org.luciak.acb.exceptions.PaxNotFoundException;
import org.luciak.acb.exceptions.SeatNotFoundException;
import org.luciak.acb.exceptions.WrongXMLFormatException;
import org.luciak.acb.services.flight.FlightDto;
import org.luciak.acb.services.flight.FlightService;
import org.luciak.acb.services.flight.PaxDto;
import org.luciak.acb.services.flight.PaxlistDto;
import org.luciak.acb.services.flight.FlightSetupDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.util.List;

/**
 * Flight API
 *
 * @author Lucia Kleinova
 */
@RestController
@RequestMapping("api/flight")
public class FlightController {

    /**
     * Flight service.
     */
    private final FlightService flightService;

    @Autowired
    public FlightController(FlightService flightService) {

        this.flightService = flightService;
    }

    /**
     * Get list of all flights information.
     *
     * @return list of all flights
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<FlightDto> findAllFlights(@RequestParam(name = "status", required = false) final FlightStatus status) {

        if (status == null) {
            return flightService.findAllFlights();
        }
        return flightService.findFlightsByStatus(status);
    }

    /**
     * Get detail of flight.
     *
     * @param ufi unique flight identifier.
     * @return flight detail
     * @throws FlightNotFoundException in case when flight doesn't exists
     */
    @GetMapping(path = "/{ufi}", produces = MediaType.APPLICATION_JSON_VALUE)
    public FlightDto getFlight(@PathVariable(name = "ufi") final String ufi)
            throws FlightNotFoundException {

        return flightService.getFlight(ufi);
    }

    /**
     * Create and save new flight with CREATED state.
     *
     * @param flight flight metadata {@link FlightDto}
     * @return updated flight metadata with filled UFI
     * @throws AircraftNotFoundException    in case when aircraft doesn't exists
     * @throws FlightAlreadyExistsException in case when flight already exists
     */
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public FlightDto createFlight(@RequestBody final FlightDto flight)
            throws AircraftNotFoundException, FlightAlreadyExistsException {

        return flightService.createFlight(flight);
    }

    /**
     * Update existing flight. Only flight with CREATED state may be updated.
     *
     * @param ufi    unique flight identifier
     * @param flight flight metadata {@link FlightDto}
     * @return updated flight metadata
     * @throws AircraftNotFoundException       in case when aircraft doesn't exists
     * @throws FlightNotFoundException         in case when original flight doesn't exists
     * @throws FlightAlreadyExistsException    in case when target flight already exists
     * @throws IncorrectStateOfFlightException in case when flight is in incorrect state and cannot be updated
     */
    @PutMapping(path = "/{ufi}", produces = MediaType.APPLICATION_JSON_VALUE)
    public FlightDto updateFlight(@PathVariable(name = "ufi") final String ufi,
                                  @RequestBody final FlightDto flight)
            throws AircraftNotFoundException, FlightNotFoundException,
            FlightAlreadyExistsException, IncorrectStateOfFlightException {

        return flightService.updateFlight(ufi, flight);
    }

    /**
     * Delete existing flight.
     *
     * @param ufi unique flight identifier
     * @throws FlightNotFoundException in cese when flight doesn't exists
     */
    @DeleteMapping(path = "/{ufi}")
    public void deleteFlight(@PathVariable(name = "ufi") final String ufi) throws FlightNotFoundException {

        flightService.deleteFlight(ufi);
    }

    /**
     * Upload the pasanger list.
     *
     * @param ufi         unique flight identifier
     * @param paxlistFile paxlist file to upload
     * @return updated {@link FlightDto}
     * @throws FlightNotFoundException         in case when flight not found
     * @throws IncorrectStateOfFlightException in case when flight is in incorrect state
     * @throws IOException                     in case of IO problems with XML unmarshalling
     */
    @PostMapping(path = "/{ufi}/paxlist", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public FlightDto uploadPaxlist(@PathVariable(name = "ufi") final String ufi,
                                   @RequestParam(name = "paxlist") final MultipartFile paxlistFile)
            throws FlightNotFoundException, IncorrectStateOfFlightException, IOException {

        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(PaxlistDto.class);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
            PaxlistDto paxlist = (PaxlistDto) unmarshaller.unmarshal(paxlistFile.getInputStream());

            return flightService.uploadPaxlist(ufi, paxlist);
        } catch (JAXBException e) {

            throw new WrongXMLFormatException("Wrong format of uploaded paxlist xml file!");
        }
    }

    /**
     * Delete paxlist from the flight.
     *
     * @param ufi unique flight identifier
     * @return updated {@link FlightDto}
     * @throws FlightNotFoundException         in case when flight not found
     * @throws IncorrectStateOfFlightException in case when flight is in incorrect state
     */
    @DeleteMapping(path = "/{ufi}/paxlist", produces = MediaType.APPLICATION_JSON_VALUE)
    public FlightDto deletePaxlist(@PathVariable(name = "ufi") final String ufi)
            throws FlightNotFoundException, IncorrectStateOfFlightException {

        return flightService.deletePaxlist(ufi);
    }

    /**
     * Confirm paxlist for the flight. The flight must be in PREPARED state.
     *
     * @param ufi unique flight identifier
     * @return
     * @throws FlightNotFoundException         in case when flight not found
     * @throws IncorrectStateOfFlightException in case when flight is in incorrect state
     */
    @PutMapping(path = "/{ufi}/paxlist/confirm", produces = MediaType.APPLICATION_JSON_VALUE)
    public FlightDto confirmPaxlist(@PathVariable(name = "ufi") final String ufi)
            throws FlightNotFoundException, IncorrectStateOfFlightException {

        return flightService.confirmPaxlist(ufi);
    }

    /**
     * Get actual flight setup wich contains actual paxlist and weights of seat sections and compartments.
     *
     * @return List of {@link PaxDto}
     * @throws FlightNotFoundException in case when flight not found
     */
    @GetMapping(path = "/{ufi}/paxlist", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<PaxDto> getPaxlist(@PathVariable(name = "ufi") final String ufi) throws FlightNotFoundException {

        return flightService.getPaxlist(ufi);
    }

    /**
     * Get seatmap with assigned passengers.
     * <p>
     * Key: seat number Value: {@link PaxDto}
     *
     * @param ufi unique flight identifier
     * @return map of seat with assigned passengers
     * @throws FlightNotFoundException in case when flight not found
     */
    @GetMapping(path = "/{ufi}/setup")
    public FlightSetupDto getActualFlightSetup(@PathVariable(name = "ufi") final String ufi)
            throws FlightNotFoundException {

        return flightService.getActualFlightSetup(ufi);
    }

    /**
     * Assign final seat for the passenger and update his baggage weight.
     *
     * @param ufi unique flight identifier
     * @param pax passenger to seat
     * @return actual flight setup wich contains actual paxlist and weights of seat sections and compartments
     * @throws FlightNotFoundException         in case when flight not found
     * @throws IncorrectStateOfFlightException in case when flight is in incorrect state
     * @throws PaxNotFoundException            in case when pax which should be confirmed doesn't exist
     * @throws SeatNotFoundException           in case when seat where the pax should be seated doesn't exist
     */
    @PutMapping(path = "/{ufi}/pax/confirm", produces = MediaType.APPLICATION_JSON_VALUE)
    public PaxDto confirmPax(@PathVariable(name = "ufi") final String ufi,
                             @RequestBody final PaxDto pax)
            throws FlightNotFoundException, IncorrectStateOfFlightException,
            PaxNotFoundException, SeatNotFoundException {

        return flightService.confirmPax(ufi, pax);
    }

    /**
     * Close the flight and remove all unassigned passengers from the pax list.
     * <p>
     * Flight must be in {@link FlightStatus#BALANCED} or {@link FlightStatus#PROCESSING} state.
     *
     * @param ufi unique flight identifier
     * @return updated flight
     * @throws FlightNotFoundException         in case when flight doesn't exists
     * @throws IncorrectStateOfFlightException in case when flight is in illegal state
     */
    @PutMapping(path = "/{ufi}/close")
    public FlightDto closeFlight(@PathVariable(name = "ufi") final String ufi)
            throws FlightNotFoundException, IncorrectStateOfFlightException {

        return flightService.closeFlight(ufi);
    }
}
