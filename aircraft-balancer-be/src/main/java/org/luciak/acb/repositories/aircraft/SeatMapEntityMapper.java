package org.luciak.acb.repositories.aircraft;

import org.luciak.acb.domain.aircraft.Seat;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Aircraft's seat map entity to domain object mapper.
 * <p>
 * <p>
 * This class contains mappers method from aircraft's seat map entity into the domain object {@link Seat}
 * @author Lucia Kleinova
 */
public final class SeatMapEntityMapper {

    /**
     * Convert list of {@link SeatMapEntity} into list of {@link Seat} domain objects.
     *
     * @param seatMapEntities list of {@link SeatMapEntity}
     * @return list of {@link Seat}
     */
    public static List<Seat> mapToSeatMapList(final List<SeatMapEntity> seatMapEntities) {

        if (seatMapEntities == null) {
            return Collections.emptyList();
        }

        return seatMapEntities.stream()
                .map(SeatMapEntityMapper::mapToSeatMap)
                .collect(Collectors.toList());
    }
    /**
     * Convert {@link SeatMapEntity} into {@link Seat} domain object.
     *
     * @param seatMapEntity {@link SeatMapEntity}
     * @return {@link Seat} domain object
     */
    public static Seat mapToSeatMap(final SeatMapEntity seatMapEntity) {

        return new Seat.Builder(seatMapEntity.getNumber(),
                seatMapEntity.getSeatOrder(),
                seatMapEntity.getSection(),
                seatMapEntity.getRestriction())
                .build();
    }
}
