package org.luciak.acb.repositories.flight;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class PaxIdentity implements Serializable {

    /**
     * Unique flight identifier.
     */
    @Column(name = "UFI")
    private String ufi;

    /**
     * Passenger identification number.
     */
    @Column(name = "ID_NUMBER")
    private String idNumber;

    /**
     * Get unique flight identifier.
     *
     * @return unique flight identifier
     */
    public String getUfi() {
        return ufi;
    }

    /**
     * Get passenger identification number.
     *
     * @return passenger identification number
     */
    public String getIdNumber() {
        return idNumber;
    }

    /**
     * Private constructor for internal using of JPA entity manager.
     */
    @SuppressWarnings("unused")
    private PaxIdentity() {
    }

    /**
     * Private constructor. Use {@link Builder#build()} instead.
     *
     * @param builder pax builder
     */
    PaxIdentity(final Builder builder) {

        this.ufi = builder.ufi;
        this.idNumber = builder.idNumber;
    }

    static class Builder {

        private final String ufi;

        private final String idNumber;

        public Builder(final String ufi, final String idNumber) {

            this.ufi = ufi;
            this.idNumber = idNumber;
        }

        PaxIdentity build() {

            return new PaxIdentity(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PaxIdentity that = (PaxIdentity) o;
        return ufi.equals(that.ufi) &&
                idNumber.equals(that.idNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ufi, idNumber);
    }
}
