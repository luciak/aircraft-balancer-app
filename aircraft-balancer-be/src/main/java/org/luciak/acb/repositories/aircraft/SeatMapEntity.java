package org.luciak.acb.repositories.aircraft;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "SEAT_MAP")
public class SeatMapEntity {

    /* **************************************************************************************************************
     * CLASS MEMBERS
     * **************************************************************************************************************/

    @EmbeddedId
    private SeatMapIdentity id;

    /**
     * Seat number.
     */
    @Column(name = "NUMBER", updatable = false, insertable = false)
    private String number;

    /**
     * Aircraft registration mark.
     */
    @Column(name = "AIRCRAFT_REGISTRATION_MARK", updatable = false, insertable = false)
    private String aircraftRegistrationMark;

    /**
     * Seat order.
     */
    @Column(name = "SEAT_ORDER")
    private Integer seatOrder;

    /**
     * Section of the seat.
     */
    @Column(name = "SECTION")
    private String section;

    /**
     * Seat restriction flag.
     */
    @Column(name = "RESTRICTION")
    private boolean restriction;


    /* **************************************************************************************************************
     * CONVENTIONAL METHODS (getters, constructors, hashCode, equals etc.)
     * **************************************************************************************************************/

    /**
     * Get seat identity {@link SeatMapIdentity}.
     *
     * @return {@link SeatMapIdentity}
     */
    public SeatMapIdentity getId() {
        return id;
    }

    /**
     * Get seat number.
     * @return seat number
     */
    public String getNumber() {
        return number;
    }

    /**
     * Get aircraft registration mark.
     *
     * @return aircraft registration mark
     */
    public String getAircraftRegistrationMark() {
        return aircraftRegistrationMark;
    }

    /**
     * Get seat order.
     *
     * @return seat order
     */
    public Integer getSeatOrder() {
        return seatOrder;
    }

    /**
     * Get section of the seat.
     *
     * @return section of the seat
     */
    public String getSection() {
        return section;
    }

    /**
     * Get seat restriction flag.
     *
     * @return seat restriction flag
     */
    public boolean getRestriction() {
        return restriction;
    }

    /**
     * Private constructor for internal using of JPA entity manager.
     */
    @SuppressWarnings("unused")
    private SeatMapEntity() {
    }

    private SeatMapEntity(Builder builder) {

        this.id = new SeatMapIdentity.Builder(builder.number, builder.aircraftRegistrationMark).build();
        this.number = builder.number;
        this.aircraftRegistrationMark = builder.aircraftRegistrationMark;
        this.seatOrder = builder.seatOrder;
        this.section = builder.section;
        this.restriction = builder.restriction;
    }


    /* **************************************************************************************************************
     * OBJECT BUILDER
     * **************************************************************************************************************/

    /**
     * Seat map entity builder
     */
    static class Builder {

        private final String number;

        private String aircraftRegistrationMark;

        private Integer seatOrder;

        private final String section;

        private final boolean restriction;

        Builder(String number, String aircraftRegistrationMark, Integer seatOrder,
                String section, boolean restriction) {

            this.number = number;
            this.aircraftRegistrationMark = aircraftRegistrationMark;
            this.seatOrder = seatOrder;
            this.section = section;
            this.restriction = restriction;
        }

        SeatMapEntity build() {

            return new SeatMapEntity(this);
        }
    }
}
