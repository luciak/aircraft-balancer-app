package org.luciak.acb.repositories.flight;

import org.luciak.acb.domain.flight.Flight;
import org.luciak.acb.domain.flight.FlightInfo;
import org.luciak.acb.repositories.aircraft.AircraftEntityMapper;
import org.springframework.util.Assert;

/**
 * Flight's entity to domain object mapper.
 * <p>
 * <p>
 * This class contains mappers method from flight entityinto the domain object {@link Flight} and {@link FlightInfo}
 * @author Lucia Kleinova
 */
class FlightEntityMapper {

    /**
     * Convert {@link FlightEntity} into {@link FlightInfo} domain object.
     *
     * @param flightEntity {@link FlightEntity}
     * @return {@link FlightInfo} domain object
     */
    static FlightInfo mapToFlightInfo(final FlightEntity flightEntity) {

        Assert.notNull(flightEntity, "Flight entity cannot be null!");

        return new FlightInfo.Builder(flightEntity.getUfi(),
                flightEntity.getAircraftRegistrationMark(),
                flightEntity.getFlightNumber(),
                flightEntity.getOriginDate(),
                flightEntity.getDepartureAirport(),
                flightEntity.getArrivalAirport())
                .withRepeatNumber(flightEntity.getRepeatNumber())
                .withStatus(flightEntity.getStatus())
                .build();
    }

    /**
     * Convert {@link FlightEntity} into {@link Flight} domain object.
     *
     * @param flightEntity {@link FlightEntity}
     * @return {@link Flight} domain object
     */
    static Flight mapToFlight(final FlightEntity flightEntity) {

        Assert.notNull(flightEntity, "Flight entity cannot be null!");

        return new Flight.Builder(flightEntity.getUfi(),
                AircraftEntityMapper.mapToAircraft(flightEntity.getAircraft()),
                flightEntity.getFlightNumber(),
                flightEntity.getOriginDate(),
                flightEntity.getDepartureAirport(),
                flightEntity.getArrivalAirport())
                .withStatus(flightEntity.getStatus())
                .withRepeatNumber(flightEntity.getRepeatNumber())
                .withPaxlist(PaxEntityMapper.mapToPaxlist(flightEntity.getPaxlist()))
                .build();

    }

    /**
     * Convert {@link Flight} domain object to {@link FlightEntity}
     *
     * @param flight {@link Flight} domain object
     * @return {@link FlightEntity}
     */
    static FlightEntity mapToFlightEntity(final Flight flight) {

        Assert.notNull(flight, "Flight cannot be null!");

        return new FlightEntity.Builder(flight.getUfi(),
                AircraftEntityMapper.mapToAircraftEntity(flight.getAircraft()),
                flight.getFlightNumber(),
                flight.getOriginDate(),
                flight.getDepartureAirport(),
                flight.getArrivalAirport())
                .withRepeatNumber(flight.getRepeatNumber())
                .withStatus(flight.getStatus())
                .withPaxlist(PaxEntityMapper.mapToPaxlistEntities(flight.getUfi(), flight.getPaxlist()))
                .build();

    }
}
