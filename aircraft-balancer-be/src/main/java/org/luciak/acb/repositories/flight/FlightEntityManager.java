package org.luciak.acb.repositories.flight;

import org.luciak.acb.domain.flight.FlightStatus;
import org.springframework.data.repository.Repository;

import java.util.List;


/**
 * Spring JPA entity manager for manipulation and persisting flight entities.
 */
interface FlightEntityManager extends Repository<FlightEntity, String> {

    /**
     * Find all flights ordered by origin date.
     *
     * @return List of {@link FlightEntity}
     */
    List<FlightEntity> findAllByOrderByOriginDateAsc();

    /**
     * Find all flights by status ordered by origin date.
     *
     * @param status flight status
     * @return List of {@link FlightEntity}
     */
    List<FlightEntity> findAllByStatusOrderByOriginDateAsc(final FlightStatus status);

    /**
     * Get flight by UFI.
     *
     * @param ufi unique flight identifier
     * @return {@link FlightEntity}
     */
    FlightEntity findByUfi(final String ufi);

    /**
     * Check if flight exists.
     *
     * @param ufi unique flight identifier
     * @return <li>TRUE if exists</li><li>FALSE otherwise</li>
     */
    boolean existsByUfi(final String ufi);

    /**
     * Save flight.
     *
     * @return flight entity {@link FlightEntity}
     */
    FlightEntity saveAndFlush(final FlightEntity flightEntity);

    /**
     * Delete flight.
     *
     * @param ufi unique flight identifier.
     */
    void deleteByUfi(final String ufi);

}
