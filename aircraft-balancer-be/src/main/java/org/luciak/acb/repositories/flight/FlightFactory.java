package org.luciak.acb.repositories.flight;

import org.apache.commons.lang3.StringUtils;
import org.luciak.acb.domain.aircraft.Aircraft;
import org.luciak.acb.domain.flight.Flight;
import org.luciak.acb.domain.flight.FlightStatus;
import org.luciak.acb.exceptions.AircraftNotFoundException;
import org.luciak.acb.repositories.aircraft.AircraftRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import java.time.LocalDate;

/**
 * Flight factory to create new flight domain object.
 *
 * @author Lucia Kleinova
 */
@Component
public class FlightFactory {

    /**
     * Aircraft repository
     */
    private final AircraftRepository aircraftRepository;

    @Autowired
    public FlightFactory(AircraftRepository aircraftRepository) {

        this.aircraftRepository = aircraftRepository;
    }

    /**
     * Create new flight and returns its metadata. Status of the new flight will be CREATED.
     *
     * @param aircraftRegistrationMark aircraft registration mark (MANDATORY)
     * @param flightNumber             flight number {MANDATORY}
     * @param originDate               scheduled UTC departure date (MANDATORY)
     * @param departureAirport         departure airport code (MANDATORY)
     * @param arrivalAirport           arrival airport code (MANDATORY)
     * @param repeatNumber             repeat number (OPTIONAL)
     * @return flight domain object {@link Flight}
     */
    public Flight createFlight(final String aircraftRegistrationMark,
                               final String flightNumber,
                               final LocalDate originDate,
                               final String departureAirport,
                               final String arrivalAirport,
                               final Integer repeatNumber)
            throws AircraftNotFoundException {

        Assert.isTrue(StringUtils.isNotBlank(aircraftRegistrationMark),
                "Aircraft registration mar cannot be null or empty!");
        Assert.notNull(originDate, "Origin date cannot be null!");
        Assert.isTrue(StringUtils.isNotBlank(departureAirport),
                "Departure airport cannot be null or empty!");
        Assert.isTrue(StringUtils.isNotBlank(arrivalAirport),
                "Arrival airport cannot be null or empty!");

        String ufi = UfiGenerator.generateUfi(flightNumber, originDate, departureAirport, arrivalAirport, repeatNumber);

        Aircraft aircraft = aircraftRepository.getAircraftConfiguration(aircraftRegistrationMark);

        return new Flight.Builder(ufi,
                aircraft,
                flightNumber,
                originDate,
                departureAirport,
                arrivalAirport)
                .withRepeatNumber(repeatNumber)
                .withStatus(FlightStatus.CREATED)
                .build();
    }
}
