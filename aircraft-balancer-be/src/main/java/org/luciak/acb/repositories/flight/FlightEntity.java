package org.luciak.acb.repositories.flight;

import org.apache.commons.lang3.StringUtils;
import org.luciak.acb.domain.flight.FlightStatus;
import org.luciak.acb.repositories.aircraft.AircraftEntity;
import org.springframework.util.Assert;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Flight JPA entity.
 *
 * @author Lucia Kleinova
 */
@Entity
@Table(name = "FLIGHT")
public final class FlightEntity {

    /* **************************************************************************************************************
     * CLASS MEMBERS
     * **************************************************************************************************************/

    /**
     * Unique flight identifier according to AIDX specification.
     */
    @Id
    @Column(name = "UFI")
    private String ufi;

    /**
     * Aircraft registration mark.
     */
    @Column(name = "AIRCRAFT_REGISTRATION_MARK")
    private String aircraftRegistrationMark;

    /**
     * Aircraft entity.
     */
    @ManyToOne
    @JoinColumn(name = "AIRCRAFT_REGISTRATION_MARK", nullable = false, insertable = false, updatable = false)
    private AircraftEntity aircraft;

    /**
     * Flight number.
     * <p>
     * <p>
     * Flight number contains following information:
     */
    @Column(name = "FLIGHT_NUMBER")
    private String flightNumber;

    /**
     * UTC scheduled date of departure of a flight.
     */
    @Column(name = "ORIGIN_DATE")
    private LocalDate originDate;

    /**
     * Departure airport code (IATA or ICAO).
     */
    @Column(name = "DEPARTURE_AIRPORT")
    private String departureAirport;

    /**
     * Arrival airport code (IATA or ICAO).
     */
    @Column(name = "ARRIVAL_AIRPORT")
    private String arrivalAirport;

    /**
     * Optional repeat number.
     */
    @Column(name = "REPEAT_NUMBER")
    private Integer repeatNumber;

    /**
     * Paxlist.
     */
    @OneToMany(mappedBy = "flight", cascade = CascadeType.ALL, orphanRemoval = true)
    @OrderBy("NAME ASC, SURNAME ASC")
    private List<PaxEntity> paxlist;

    /**
     * Flight status.
     */
    @Column(name = "STATUS")
    @Enumerated(EnumType.STRING)
    private FlightStatus status;


    /* **************************************************************************************************************
     * CONVENTIONAL METHODS (getters, constructors, hashCode, equals etc.)
     * **************************************************************************************************************/

    /**
     * Get unique flight identifier.
     *
     * @return unique flight identifier
     */
    public String getUfi() {
        return ufi;
    }

    /**
     * Get aircraft configuration.
     *
     * @return aircraft configuration {@link AircraftEntity}
     */
    public AircraftEntity getAircraft() {
        return aircraft;
    }

    /**
     * Get aircraft registration mark.
     *
     * @return aircraft registration mark
     */
    public String getAircraftRegistrationMark() {
        return aircraftRegistrationMark;
    }

    /**
     * Get aircraft flight number using format:
     * <p>
     * <ul>
     * <li>Airline code (IATA or ICAO)</li>
     * <li>Flight number (IATA)</li>
     * <li>Operational suffix (optional)</li>
     * </ul>
     *
     * @return aircraft flight number
     */
    public String getFlightNumber() {
        return flightNumber;
    }

    /**
     * Get UTC origin departure date of a flight.
     *
     * @return origin departure date of a flight
     */
    public LocalDate getOriginDate() {
        return originDate;
    }

    /**
     * Get departure airport code (IATA or ICAO).
     *
     * @return departure airport code
     */
    public String getDepartureAirport() {
        return departureAirport;
    }

    /**
     * Get arrival airport code (IATA or ICAO).
     *
     * @return arrival airport code
     */
    public String getArrivalAirport() {
        return arrivalAirport;
    }

    /**
     * Get repeat number (optional).
     *
     * @return repeat number
     */
    public Integer getRepeatNumber() {
        return repeatNumber;
    }

    /**
     * Get passenger list.
     *
     * @return list of {@link PaxEntity}
     */
    public List<PaxEntity> getPaxlist() {
        return paxlist;
    }

    /**
     * Get flight status {@link FlightStatus}
     *
     * @return flight status
     */
    public FlightStatus getStatus() {
        return status;
    }

    /**
     * Private constructor for internal using of JPA entity manager.
     */
    @SuppressWarnings("unused")
    private FlightEntity() {
    }

    /**
     * Minimal constructor for internal using of Pax mapper.
     *
     * @param ufi unique flight identifier
     */
    FlightEntity(String ufi) {
        this.ufi = ufi;
    }

    /**
     * Private constructor. Use AircraftEntity.Builder instead.
     *
     * @param builder {@link FlightEntity.Builder}
     */
    private FlightEntity(Builder builder) {

        this.ufi = builder.ufi;
        this.aircraft = builder.aircraft;
        this.aircraftRegistrationMark = builder.aircraft.getRegistrationMark();
        this.flightNumber = builder.flightNumber;
        this.originDate = builder.originDate;
        this.departureAirport = builder.departureAirport;
        this.arrivalAirport = builder.arrivalAirport;
        this.repeatNumber = builder.repeatNumber;
        this.status = builder.status;

        this.paxlist = new ArrayList<>();
        if (builder.paxlist != null) {
            this.paxlist.addAll(builder.paxlist);
        }
    }


    /* **************************************************************************************************************
     * OBJECT BUILDER
     * **************************************************************************************************************/

    /**
     * Flight entity builder.
     */
    static class Builder {

        private final String ufi;

        private final AircraftEntity aircraft;

        private final String flightNumber;

        private final LocalDate originDate;

        private final String departureAirport;

        private final String arrivalAirport;

        private Integer repeatNumber;

        private FlightStatus status;

        private List<PaxEntity> paxlist;

        Builder(final String ufi,
                final AircraftEntity aircraft,
                final String flightNumber,
                final LocalDate originDate,
                final String departureAirport,
                final String arrivalAirport) {

            Assert.isTrue(StringUtils.isNotBlank(ufi),
                    "Unique Flight Number cannot be null or empty!");
            Assert.notNull(aircraft, "Aircraft cannot be null!");
            Assert.isTrue(StringUtils.isNotBlank(flightNumber),
                    "Flight number cannot be null or empty!");
            Assert.isTrue(originDate != null,
                    "Origin date of flight cannot be null!");
            Assert.isTrue(StringUtils.isNotBlank(departureAirport),
                    "Flight number cannot be null or empty!");
            Assert.isTrue(StringUtils.isNotBlank(arrivalAirport),
                    "Flight number cannot be null or empty!");

            this.ufi = ufi;
            this.aircraft = aircraft;
            this.flightNumber = flightNumber;
            this.originDate = originDate;
            this.departureAirport = departureAirport;
            this.arrivalAirport = arrivalAirport;
        }

        Builder withRepeatNumber(Integer repeatNumber) {

            this.repeatNumber = repeatNumber;
            return this;
        }

        Builder withStatus(FlightStatus status) {

            this.status = status;
            return this;
        }

        Builder withPaxlist(List<PaxEntity> paxlist) {

            this.paxlist = paxlist;
            return this;
        }

        FlightEntity build() {

            return new FlightEntity(this);
        }

    }
}
