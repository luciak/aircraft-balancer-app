package org.luciak.acb.repositories.flight;

import org.luciak.acb.domain.flight.Pax;
import org.luciak.acb.domain.flight.Sex;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

/**
 * Pax factory to create new passenger domain object.
 *
 * @author Lucia Kleinova
 */
@Component
public class PaxFactory {

    /**
     * Create new pax.
     *
     * @param name passenger name
     * @param surname passenger surname
     * @param sex passenger sex
     * @param codeId Passenger code ID which defines type of passenger and his restrictions
     * @param groupId Passenger group ID
     * @param idNumber Passenger identification number (ID card or passport number)
     * @param birthDate Passenger birth date
     * @param FFT Passenger frequent flyer travel card number
     * @param segment All legs of the flight
     * @param seat Seat number
     * @return {@link Pax}
     */
    public Pax cratePax(final String name,
                        final String surname,
                        final Sex sex,
                        final String codeId,
                        final Integer groupId,
                        final String idNumber,
                        final LocalDate birthDate,
                        final String FFT,
                        final String segment,
                        final Integer baggageWeight,
                        final String seat) {

        return new Pax.Builder(name, surname, sex, idNumber)
                .withCodeId(codeId)
                .withGroupId(groupId)
                .withBirthDate(birthDate)
                .withFFT(FFT)
                .withSegment(segment)
                .withBaggageWeight(baggageWeight == null ? 0 : baggageWeight)
                .withSeat(seat)
                .build();
    }
}
