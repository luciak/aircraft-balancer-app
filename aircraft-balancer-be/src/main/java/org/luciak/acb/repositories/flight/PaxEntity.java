package org.luciak.acb.repositories.flight;

import org.apache.commons.lang3.StringUtils;
import org.luciak.acb.domain.flight.Sex;
import org.springframework.util.Assert;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;

/**
 * Passenger entity.
 *
 * @author Lucia Kleinove
 */
@Entity
@Table(name = "PAX")
public final class PaxEntity {

    /* **************************************************************************************************************
     * CLASS MEMBERS
     * **************************************************************************************************************/

    @EmbeddedId
    private PaxIdentity id;

    /**
     * Unique flight identifier
     */
    @Column(name = "UFI", insertable = false, updatable = false)
    private String ufi;

    /**
     * ID number of passenger (ID card or passport).
     */
    @Column(name = "ID_NUMBER", insertable = false, updatable = false)
    private String idNumber;

    @ManyToOne
    @JoinColumn(name = "UFI", nullable = false, updatable = false, insertable = false)
    private FlightEntity flight;

    /**
     * Passenger name.
     */
    @Column(name = "NAME")
    private String name;

    /**
     * Passenger surname.
     */
    @Column(name = "SURNAME")
    private String surname;

    /**
     * Passenger sex.
     */
    @Column(name = "SEX")
    @Enumerated(EnumType.STRING)
    private Sex sex;

    /**
     * Passenger code ID which defines type of passenger and his restrictions.
     */
    @Column(name = "CODE_ID")
    private String codeId;

    /**
     * ID of passengers group.
     */
    @Column(name = "GROUP_ID")
    private Integer groupId;

    /**
     * Passenger birth date.
     */
    @Column(name = "BIRTH_DATE")
    private LocalDate birthDate;

    /**
     * Frequent flyer travel card number.
     */
    @Column(name = "FFT")
    private String FFT;

    /**
     * All legs of the flight.
     */
    @Column(name = "SEGMENT")
    private String segment;

    /**
     * Seat number
     */
    @Column(name = "SEAT")
    private String seat;

    /**
     * Seat predicted by the system.
     */
    @Column(name = "PREDICATED_SEAT")
    private boolean predicatedSeat;

    /**
     * Seat section.
     */
    @Column(name = "SEAT_SECTION")
    private String seatSection;

    /**
     * Baggage compartment.
     */
    @Column(name = "BAGGAGE_COMP")
    private String baggageComp;

    /**
     * Weight of checked baggage.
     */
    @Column(name = "BAGGAGE_WEIGHT")
    private Integer baggageWeight;

    /* **************************************************************************************************************
     * CONVENTIONAL METHODS (getters, constructors, hashCode, equals etc.)
     * **************************************************************************************************************/

    /**
     * Get unique flight identifier.
     *
     * @return unique flight identifier
     */
    public String getUfi() {
        return ufi;
    }

    /**
     * Get passenger name.
     *
     * @return passenger name
     */
    public String getName() {
        return name;
    }

    /**
     * Passenger surname.
     *
     * @return passenger surname
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Get passenger sex.
     *
     * @return passenger sex
     */
    public Sex getSex() {
        return sex;
    }

    /**
     * Get passenger code ID.
     *
     * @return passenger code ID
     */
    public String getCodeId() {
        return codeId;
    }

    /**
     * Get passenger group ID.
     *
     * @return passenger group ID
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * Get passenger ID number.
     *
     * @return passenger ID number
     */
    public String getIdNumber() {
        return idNumber;
    }

    /**
     * Get passenger birth date.
     *
     * @return passenger birth date
     */
    public LocalDate getBirthDate() {
        return birthDate;
    }

    /**
     * Get frequent flyer traveler card number.
     *
     * @return frequent flyer traveler card number
     */
    public String getFFT() {
        return FFT;
    }

    /**
     * Get all legs of flight.
     *
     * @return all legs of flight
     */
    public String getSegment() {
        return segment;
    }

    /**
     * Get seat number.
     *
     * @return seat number
     */
    public String getSeat() {
        return seat;
    }

    /**
     * Get predicted seat by the system.
     *
     * @return predicted seat by the system
     */
    public boolean isPredicatedSeat() {
        return predicatedSeat;
    }

    /**
     * Get seat section.
     *
     * @return seat section
     */
    public String getSeatSection() {
        return seatSection;
    }

    /**
     * Get baggage compartment.
     *
     * @return baggage compartment code
     */
    public String getBaggageComp() {
        return baggageComp;
    }

    /**
     * Get baggage weight.
     *
     * @return baggage weight
     */
    public Integer getBaggageWeight() {
        return baggageWeight;
    }

    /**
     * Private constructor for internal using of JPA entity manager.
     */
    @SuppressWarnings("unused")
    private PaxEntity() {
    }

    public PaxEntity(Builder builder) {

        this.id = new PaxIdentity.Builder(builder.ufi, builder.idNumber).build();
        this.ufi = builder.ufi;
        this.flight = builder.flight;
        this.name = builder.name;
        this.surname = builder.surname;
        this.sex = builder.sex;
        this.codeId = builder.codeId;
        this.groupId = builder.groupId;
        this.idNumber = builder.idNumber;
        this.birthDate = builder.birthDate;
        this.FFT = builder.FFT;
        this.segment = builder.segment;
        this.seat = builder.seat;
        this.predicatedSeat = builder.predicatedSeat;
        this.seatSection = builder.seatSection;
        this.baggageComp = builder.baggageComp;
        this.baggageWeight = builder.baggageWeight;
    }


    /* **************************************************************************************************************
     * OBJECT BUILDER
     * **************************************************************************************************************/

    /**
     * Pax entity builder.
     */
    static class Builder {

        private final String ufi;

        private final FlightEntity flight;

        private final String name;

        private final String surname;

        private final Sex sex;

        private String codeId;

        private Integer groupId;

        private final String idNumber;

        private LocalDate birthDate;

        private String FFT;

        private String segment;

        private String seat;

        private boolean predicatedSeat;

        private String seatSection;

        private String baggageComp;

        private Integer baggageWeight;

        Builder(final String ufi,
                final FlightEntity flight,
                final String name,
                final String surname,
                final Sex sex,
                final String idNumber) {

            Assert.isTrue(StringUtils.isNotBlank(ufi),
                    "Unique Flight Number cannot be null or empty!");
            Assert.notNull(flight, "Assigned flight cannot be null!");
            Assert.isTrue(StringUtils.isNotBlank(name),
                    "Passenger name cannot be null or empty!");
            Assert.isTrue(StringUtils.isNotBlank(surname),
                    "Passenger surname cannot be null or empty!");
            Assert.notNull(sex, "Passenger sex cannot be null!");
            Assert.isTrue(StringUtils.isNotBlank(idNumber),
                    "Passenger identification number cannot be null or empty!");

            this.ufi = ufi;
            this.flight = flight;
            this.name = name;
            this.surname = surname;
            this.sex = sex;
            this.idNumber = idNumber;
        }

        Builder withCodeId(final String codeId) {

            this.codeId = codeId;
            return this;
        }

        Builder withGroupId(final Integer groupId) {

            this.groupId = groupId;
            return this;
        }

        Builder withBirthDate(final LocalDate birthDate) {

            this.birthDate = birthDate;
            return this;
        }

        Builder withFFT(final String FFT) {

            this.FFT = FFT;
            return this;
        }

        Builder withSegment(final String segment) {

            this.segment = segment;
            return this;
        }

        Builder withSeat(final String seat) {

            this.seat = seat;
            return this;
        }

        Builder withPredicatedSeat(final boolean predicatedSeat) {

            this.predicatedSeat = predicatedSeat;
            return this;
        }

        Builder withSeatSection(final String seatSection) {

            this.seatSection = seatSection;
            return this;
        }

        Builder withBaggageComp(final String baggageComp) {

            this.baggageComp = baggageComp;
            return this;
        }

        Builder withBaggageWeight(final Integer baggageWeight) {

            this.baggageWeight = baggageWeight;
            return this;
        }

        PaxEntity build() {

            return new PaxEntity(this);
        }
    }

}
