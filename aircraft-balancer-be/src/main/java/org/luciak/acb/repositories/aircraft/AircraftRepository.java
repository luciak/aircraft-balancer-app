package org.luciak.acb.repositories.aircraft;

import org.luciak.acb.domain.aircraft.Aircraft;
import org.luciak.acb.exceptions.AircraftNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Aircraft repository for manipulation of persisted entities.
 *
 * @author Lucia Kleinova
 */
@Repository
public class AircraftRepository {

    /**
     * Generated JPA repository.
     */
    private final AircraftEntityManager aircraftEntityManager;

    @Autowired
    public AircraftRepository(final AircraftEntityManager aircraftEntityManager) {

        this.aircraftEntityManager = aircraftEntityManager;
    }

    /**
     * Find all available aircraft configuration ordered by registration mark.
     *
     * @return list of available aircraft configuration {@link Aircraft}
     */
    public List<Aircraft> findAircraftConfigurations() {

        List<AircraftEntity> result = aircraftEntityManager.findAllByOrderByRegistrationMarkAsc();

        if (result == null) {
            return Collections.emptyList();
        }

        return result.stream()
                .map(AircraftEntityMapper::mapToAircraft)
                .collect(Collectors.toList());
    }

    /**
     * Find particular aircraft configuration by aircraft's registration mark.
     *
     * @param registrationMark aircraft registration mark
     * @return aircraft configuration {@link Aircraft}
     * @throws AircraftNotFoundException in case when aircraft doesn't exists
     */
    public Aircraft getAircraftConfiguration(final String registrationMark) throws AircraftNotFoundException {

        AircraftEntity result = aircraftEntityManager.findByRegistrationMark(registrationMark);

        if (result == null) {
            throw new AircraftNotFoundException(
                    String.format("Configuration of an aircraft with registration mark '%s' has not been found!",
                            registrationMark));
        }

        return AircraftEntityMapper.mapToAircraft(result);
    }

}
