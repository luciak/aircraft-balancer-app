package org.luciak.acb.repositories.flight;

import org.luciak.acb.domain.flight.Pax;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Pax's entity to domain object mapper.
 * <p>
 * <p>
 * This class contains mappers method from flight entity into the domain object {@link Pax}
 *
 * @author Lucia Kleinova
 */
public class PaxEntityMapper {

    /**
     * Convert list of {@link PaxEntity} into list of {@link Pax} domain objects.
     *
     * @param paxlist list of {@link PaxEntity}
     * @return list of {@link Pax}
     */
    public static List<Pax> mapToPaxlist(final List<PaxEntity> paxlist) {

        if (paxlist == null) {
            return Collections.emptyList();
        }

        return paxlist.stream().map(PaxEntityMapper::mapToPax).collect(Collectors.toList());
    }

    /**
     * Convert {@link PaxEntity} into {@link Pax} domain object.
     *
     * @param entity {@link PaxEntity}
     * @return {@link Pax} domain object
     */
    static Pax mapToPax(final PaxEntity entity) {

        return new Pax.Builder(entity.getName(),
                entity.getSurname(),
                entity.getSex(),
                entity.getIdNumber())
                .withBirthDate(entity.getBirthDate())
                .withCodeId(entity.getCodeId())
                .withGroupId(entity.getGroupId())
                .withFFT(entity.getFFT())
                .withSegment(entity.getSegment())
                .withSeat(entity.getSeat())
                .withPredicatedSeat(entity.isPredicatedSeat())
                .withSeatSection(entity.getSeatSection())
                .withBaggageComp(entity.getBaggageComp())
                .withBaggageWeight(entity.getBaggageWeight() == null ? 0 : entity.getBaggageWeight())
                .build();
    }

    /**
     * Convert list of pax domain object {@link Pax} into the list of pax entities {@link PaxEntity}.
     *
     * @param ufi unique flight identifier
     * @param paxlist list of {@link Pax}
     * @return list of {@link PaxEntity}
     */
    public static List<PaxEntity> mapToPaxlistEntities(final String ufi, final List<Pax> paxlist) {

        return paxlist.stream()
                .map(pax -> PaxEntityMapper.mapToPaxEntity(ufi, pax))
                .collect(Collectors.toList());
    }

    /**
     * Convert pax domain object {@link Pax} into the pax entity {@link PaxEntity}.
     *
     * @param ufi unique flight identifier
     * @param pax {@link Pax} domain object
     * @return {@link PaxEntity}
     */
    static PaxEntity mapToPaxEntity(final String ufi, final Pax pax) {

        return new PaxEntity.Builder(
                ufi,
                new FlightEntity(ufi),
                pax.getName(),
                pax.getSurname(),
                pax.getSex(),
                pax.getIdNumber())
                .withBirthDate(pax.getBirthDate())
                .withCodeId(pax.getCodeId())
                .withGroupId(pax.getGroupId())
                .withFFT(pax.getFFT())
                .withSegment(pax.getSegment())
                .withSeat(pax.getSeat())
                .withPredicatedSeat(pax.isPredictedSeat())
                .withSeatSection(pax.getSeatSection())
                .withBaggageComp(pax.getBaggageComp())
                .withBaggageWeight(pax.getBaggageWeight())
                .build();
    }
}
