package org.luciak.acb.repositories.aircraft;

import org.luciak.acb.domain.aircraft.Aircraft;
import org.springframework.util.Assert;

/**
 * Aircraft's configuration entity to domain object mapper.
 * <p>
 * <p>
 * This class contains mappers method from aircraft configuration entity into the domain object {@link Aircraft}
 * @author Lucia Kleinova
 */
public final class AircraftEntityMapper {

    /**
     * Convert {@link AircraftEntity} into {@link Aircraft} domain object.
     *
     * @param aircraftEntity {@link AircraftEntity}
     * @return {@link Aircraft} domain object
     */
    public static Aircraft mapToAircraft(final AircraftEntity aircraftEntity) {

        Assert.notNull(aircraftEntity, "Aircraft configuration entity cannot be null!");

        return new Aircraft.Builder(aircraftEntity.getRegistrationMark())
                .withModelNumber(aircraftEntity.getModelNumber())
                .withVersion(aircraftEntity.getVersion())
                .withCfgCode(aircraftEntity.getCfgCode())
                .withSeatMapSvg(aircraftEntity.getSeatMapSvg())
                .withSeatMap(SeatMapEntityMapper.mapToSeatMapList(aircraftEntity.getSeatMap()))
                .withCompMap(CompMapEntityMapper.mapToCompMapList(aircraftEntity.getCompMap()))
                .build();
    }

    /**
     * Convert {@link Aircraft} domain object into {@link AircraftEntity}
     *
     * @param aircraft {@link Aircraft} domain object
     * @return {@link AircraftEntity}
     */
    public static AircraftEntity mapToAircraftEntity(final Aircraft aircraft) {

        Assert.notNull(aircraft, "Aircraft configuration cannot be null!");

        return new AircraftEntity.Builder(aircraft.getRegistrationMark())
                .withModelNumber(aircraft.getModelNumber())
                .withVersion(aircraft.getVersion())
                .withCfgCode(aircraft.getCfgCode())
                .withSeatMapSvg(aircraft.getSeatMapSvg())
                .build();
    }
}
