package org.luciak.acb.repositories.aircraft;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "COMP_MAP")
public class CompMapEntity {

    /* **************************************************************************************************************
     * CLASS MEMBERS
     * **************************************************************************************************************/

    @EmbeddedId
    private CompMapIdentity id;

    /**
     * Compartment number.
     */
    @Column(name = "NUMBER", insertable = false, updatable = false)
    private String number;

    /**
     * Aircraft registration mark.
     */
    @Column(name = "AIRCRAFT_REGISTRATION_MARK", insertable = false, updatable = false)
    private String aircraftRegistrationMark;

    /**
     * Max capacity of compartment [kg].
     */
    @Column(name = "MAX_CAPACITY")
    private Integer maxCapacity;

    @Column(name = "COMP_ORDER")
    private Integer compOrder;


    /* **************************************************************************************************************
     * CONVENTIONAL METHODS (getters, constructors, hashCode, equals etc.)
     * **************************************************************************************************************/

    /**
     * Get compartment map identity {@link CompMapIdentity}.
     *
     * @return {@link CompMapIdentity}
     */
    public CompMapIdentity getId() {
        return id;
    }

    /**
     * Get compartment number.
     *
     * @return compartment number
     */
    public String getNumber() {
        return number;
    }

    /**
     * Get aircraft registration mark.
     *
     * @return aircraft registration mark
     */
    public String getAircraftRegistrationMark() {
        return aircraftRegistrationMark;
    }

    /**
     * Get max capacity of compartment [kg].
     *
     * @return max capacity of compartment [kg]
     */
    public Integer getMaxCapacity() {
        return maxCapacity;
    }

    /**
     * Get compartment order which defines loading priorities.
     *
     * @return compartment order
     */
    public Integer getCompOrder() {
        return compOrder;
    }

    /**
     * Private constructor for internal using of JPA entity manager.
     */
    @SuppressWarnings("unused")
    private CompMapEntity() {
    }

    CompMapEntity(Builder builder) {

        this.id = new CompMapIdentity.Builder(builder.number, builder.aircraftRegistrationMark).build();
        this.number = builder.number;
        this.aircraftRegistrationMark = builder.aircraftRegistrationMark;
        this.maxCapacity = builder.maxCapacity;
        this.compOrder = builder.compOrder;
    }


    /* **************************************************************************************************************
     * OBJECT BUILDER
     * **************************************************************************************************************/

    /**
     * Compartment entity builder.
     */
    static class Builder {

        private final String number;

        private final String aircraftRegistrationMark;

        private final Integer maxCapacity;

        private final Integer compOrder;

        Builder(String number, String aircraftRegistrationMark, Integer maxCapacity, Integer compOrder) {

            this.number = number;
            this.aircraftRegistrationMark = aircraftRegistrationMark;
            this.maxCapacity = maxCapacity;
            this.compOrder = compOrder;
        }

        CompMapEntity build() {

            return new CompMapEntity(this);
        }
    }
}
