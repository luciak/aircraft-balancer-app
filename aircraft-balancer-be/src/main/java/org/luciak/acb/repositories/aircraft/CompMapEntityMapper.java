package org.luciak.acb.repositories.aircraft;

import org.luciak.acb.domain.aircraft.Comp;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Aircraft's compartment map entity to domain object mapper.
 * <p>
 * <p>
 * This class contains mappers method from aircraft's compartment map entity into the domain object {@link Comp}
 *
 * @author Lucia Kleinova
 */
public class CompMapEntityMapper {

    /**
     * Convert list of {@link CompMapEntity} into list of {@link Comp} domain objects.
     *
     * @param compMapEntities list of {@link SeatMapEntity}
     * @return list of {@link Comp}
     */
    public static List<Comp> mapToCompMapList(final List<CompMapEntity> compMapEntities) {

        if (compMapEntities == null) {
            return Collections.emptyList();
        }

        return compMapEntities.stream()
                .map(CompMapEntityMapper::mapToCompMap)
                .collect(Collectors.toList());
    }
    /**
     * Convert {@link CompMapEntity} into {@link Comp} domain object.
     *
     * @param compMapEntity {@link SeatMapEntity}
     * @return {@link Comp} domain object
     */
    public static Comp mapToCompMap(final CompMapEntity compMapEntity) {

        return new Comp.Builder(compMapEntity.getNumber(),
                compMapEntity.getMaxCapacity(),
                compMapEntity.getCompOrder())
                .build();
    }
}
