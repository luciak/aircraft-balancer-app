package org.luciak.acb.repositories.flight;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Unique Flight Identifier generator.
 *
 * @author Lucia Kleinove
 */
class UfiGenerator {

    /**
     * Generate Unique Flight Identifier (UFI) according to AIDX specification.
     * <p>
     * {@link UfiGenerator#generateUfi(String, LocalDate, String, String, Integer)}
     *
     * @return Unique Flight Identifier (UFI)
     */
    static String generateUfi(final String flightNumber,
                              final LocalDate originDate,
                              final String departureAirport,
                              final String arrivalAirport) {

        return generateUfi(flightNumber, originDate, departureAirport, arrivalAirport, null);
    }

    /**
     * Generate Unique Flight Identifier (UFI) according to AIDX specification.
     * <p>
     * UFI is generated from:
     * <ul>
     * <li>Airline code (IATA or ICAO)</li>
     * <li>Flight number (IATA)</li>
     * <li>Operational suffix (optional)</li>
     * <li>Origin date</li>
     * <li>Departure airport (IATA or ICAO)</li>
     * <li>Arrival airport (IATA or ICAO)</li>
     * <li>Repeat number (optional)</li>
     * </ul>
     * <p>
     * <p>
     * Airline code, Flight number and Operational suffix is a part of pre-generated flight number.
     *
     * @return Unique Flight Identifier (UFI)
     */
    static String generateUfi(final String flightNumber,
                              final LocalDate originDate,
                              final String departureAirport,
                              final String arrivalAirport,
                              final Integer repeatNumber) {

        Assert.isTrue(StringUtils.isNotBlank(flightNumber), "Flight number cannot be null or empty!");
        Assert.isTrue(originDate != null, "Origin date of flight cannot be null!");
        Assert.isTrue(StringUtils.isNotBlank(departureAirport), "Flight number cannot be null or empty!");
        Assert.isTrue(StringUtils.isNotBlank(arrivalAirport), "Flight number cannot be null or empty!");

        StringBuilder builder = new StringBuilder();
        builder.append(flightNumber)
                .append(" ")
                .append(originDate.format(DateTimeFormatter.ISO_LOCAL_DATE))
                .append(" ")
                .append(departureAirport)
                .append("-")
                .append(arrivalAirport);
        if (repeatNumber != null) {
            builder.append(" ").append(repeatNumber);
        }

        return builder.toString();
    }
}
