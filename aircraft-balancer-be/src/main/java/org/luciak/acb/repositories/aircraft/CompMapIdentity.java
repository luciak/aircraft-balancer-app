package org.luciak.acb.repositories.aircraft;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class CompMapIdentity implements Serializable {

    /**
     * Seat number.
     */
    @Column(name = "NUMBER")
    private String number;

    /**
     * Aircraft registration mark.
     */
    @Column(name = "AIRCRAFT_REGISTRATION_MARK")
    private String aircraftRegistrationMark;

    /**
     * Get seat number.
     *
     * @return seat number
     */
    public String getNumber() {
        return number;
    }

    /**
     * Get aircraft regustration mark.
     *
     * @return aircraft registration mark
     */
    public String getAircraftRegistrationMark() {
        return aircraftRegistrationMark;
    }

    /**
     * Private constructor for internal using of JPA entity manager.
     */
    @SuppressWarnings("unused")
    private CompMapIdentity() {
    }

    CompMapIdentity(final Builder builder) {

        this.number = builder.number;
        this.aircraftRegistrationMark = builder.aircraftRegistrationMark;
    }

    static class Builder {

        private final String number;

        private final String aircraftRegistrationMark;

        public Builder(final String number, final String aircraftRegistrationMark) {

            this.number = number;
            this.aircraftRegistrationMark = aircraftRegistrationMark;
        }

        CompMapIdentity build() {

            return new CompMapIdentity(this);
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompMapIdentity that = (CompMapIdentity) o;
        return number.equals(that.number) &&
                aircraftRegistrationMark.equals(that.aircraftRegistrationMark);
    }

    @Override
    public int hashCode() {
        return Objects.hash(number, aircraftRegistrationMark);
    }
}
