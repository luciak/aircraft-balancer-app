package org.luciak.acb.repositories.flight;

import org.apache.commons.lang3.StringUtils;
import org.luciak.acb.domain.flight.Flight;
import org.luciak.acb.domain.flight.FlightInfo;
import org.luciak.acb.domain.flight.FlightStatus;
import org.luciak.acb.exceptions.FlightNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Flight repository for manipulation of persisted entities.
 *
 * @author Lucia Kleinova
 */
@Repository
public class FlightRepository {

    private final FlightEntityManager flightEntityManager;

    @Autowired
    public FlightRepository(final FlightEntityManager flightEntityManager) {

        this.flightEntityManager = flightEntityManager;
    }

    /**
     * Find all flights information.
     *
     * @return list of flights information. {@link FlightInfo}
     */
    public List<FlightInfo> findAllFlights() {

        List<FlightEntity> result = flightEntityManager.findAllByOrderByOriginDateAsc();

        if (result == null) {
            return Collections.emptyList();
        }

        return result.stream()
                .map(FlightEntityMapper::mapToFlightInfo)
                .collect(Collectors.toList());
    }

    /**
     * Find all flights information by status.
     *
     * @param status flight status. {@link FlightStatus}
     * @return list of flights. {@link FlightInfo}
     */
    public List<FlightInfo> findAllFlightsByStatus(final FlightStatus status) {

        Assert.notNull(status, "Flight status cannot be null!");

        List<FlightEntity> result = flightEntityManager.findAllByStatusOrderByOriginDateAsc(status);

        if (result == null) {
            return Collections.emptyList();
        }

        return result.stream()
                .map(FlightEntityMapper::mapToFlightInfo)
                .collect(Collectors.toList());
    }

    /**
     * Get particular flight by UFI.
     *
     * @param ufi Unique Flight Identifier according to AIDX specification
     * @return particular flight. {@link Flight}
     * @throws FlightNotFoundException In case when flight doesn't exists.
     */
    public Flight getFlight(final String ufi) throws FlightNotFoundException {

        Assert.isTrue(StringUtils.isNotBlank(ufi), "UFI cannot be null or empty!");

        FlightEntity result = flightEntityManager.findByUfi(ufi);

        if (result == null) {
            throw new FlightNotFoundException(String.format("Flight with UFI '%s' has not been found!", ufi));
        }

        return FlightEntityMapper.mapToFlight(result);
    }

    /**
     * Create or update matadata about the existing flight.
     *
     * @param flight fight's metadata. {@link Flight}
     */
    public void persistFlight(final Flight flight) {

        Assert.notNull(flight, "Flight cannot be empty!");

        flightEntityManager.saveAndFlush(FlightEntityMapper.mapToFlightEntity(flight));
    }

    /**
     * Delete existing flight.
     * @param ufi Unique Flight Identifier according to AIDX specification
     */
    public void deleteFlight(final String ufi) {

        Assert.isTrue(StringUtils.isNotBlank(ufi), "Unique flight identifier cannot be null or empty!");

        flightEntityManager.deleteByUfi(ufi);
    }

    public boolean exists(String ufi) {

        Assert.isTrue(StringUtils.isNotBlank(ufi), "Unique flight identifier cannot be null or empty!");

        return flightEntityManager.existsByUfi(ufi);
    }
}
