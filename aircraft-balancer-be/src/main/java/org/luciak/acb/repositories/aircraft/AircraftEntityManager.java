package org.luciak.acb.repositories.aircraft;

import org.springframework.data.repository.Repository;

import java.util.List;

/**
 * Spring JPA entity manager for manipulation and persisting aircraft entities.
 */
interface AircraftEntityManager extends Repository<AircraftEntity, String> {

    /**
     * Find all aircraft configurations ordered by registration mark.
     *
     * @return List of {@link AircraftEntity}
     */
    List<AircraftEntity> findAllByOrderByRegistrationMarkAsc();

    /**
     * Find particular aircraft configuration by aircraft registrationMark.
     *
     * @param registrationMark aircraft registration mark
     *
     * @return {@link AircraftEntity}
     */
    AircraftEntity findByRegistrationMark(final String registrationMark);

}
