package org.luciak.acb.repositories.aircraft;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import java.util.List;

/**
 * Aircraft configuration JPA entity.
 *
 * @author Lucia Kleinova
 */
@Entity
@Table(name = "AIRCRAFT")
public final class AircraftEntity {

    /* **************************************************************************************************************
     * CLASS MEMBERS
     * **************************************************************************************************************/

    /**
     * Aircraft registration mark
     */
    @Id
    @Column(name = "REGISTRATION_MARK")
    private String registrationMark;

    /**
     * Aircraft model number
     */
    @Column(name = "MODEL_NUMBER")
    private String modelNumber;

    /**
     * Aircraft version
     */
    @Column(name = "VERSION")
    private String version;

    /**
     * Aircraft configuration
     */
    @Column(name = "CFG_CODE")
    private String cfgCode;

    /**
     * Aircraft seatmap SVG
     */
    @Column(name = "SEAT_MAP_SVG")
    private String seatMapSvg;

    /**
     * Seat map.
     */
    @OneToMany
    @JoinColumn(name = "AIRCRAFT_REGISTRATION_MARK", updatable = false, insertable = false)
    @OrderBy("SEAT_ORDER ASC")
    private List<SeatMapEntity> seatMap;

    /**
     * Compartment map
     */
    @OneToMany
    @JoinColumn(name = "AIRCRAFT_REGISTRATION_MARK", updatable = false, insertable = false)
    @OrderBy("COMP_ORDER ASC")
    private List<CompMapEntity> compMap;


    /* **************************************************************************************************************
     * CONVENTIONAL METHODS (getters, constructors, hashCode, equals etc.)
     * **************************************************************************************************************/

    /**
     * Get aircraft registration mark.
     *
     * @return aircraft registration mark
     */
    public String getRegistrationMark() {
        return registrationMark;
    }

    /**
     * Get aircraft model number.
     *
     * @return aircraft model number
     */
    public String getModelNumber() {
        return modelNumber;
    }

    /**
     * Get aircraft version.
     *
     * @return aircraft version
     */
    public String getVersion() {
        return version;
    }

    /**
     * Get aircraft configuration.
     *
     * @return aircraft configuration
     */
    public String getCfgCode() {
        return cfgCode;
    }

    /**
     * Get aircraft seat map SVG.
     *
     * @return aircraft seat map SVG
     */
    public String getSeatMapSvg() {
        return seatMapSvg;
    }

    /**
     * Get seat map.
     *
     * @return seat map
     */
    public List<SeatMapEntity> getSeatMap() {
        return seatMap;
    }

    /**
     * Get compartment map.
     *
     * @return compartment map.
     */
    public List<CompMapEntity> getCompMap() {
        return compMap;
    }

    /**
     * Private constructor for internal using of JPA entity manager.
     */
    @SuppressWarnings("unused")
    private AircraftEntity() {
    }

    /**
     * Private constructor. Use AircraftEntity.Builder instead.
     *
     * @param builder {@link AircraftEntity.Builder}
     */
    private AircraftEntity(final Builder builder) {

        this.registrationMark = builder.registrationMark;
        this.modelNumber = builder.modelNumber;
        this.version = builder.version;
        this.cfgCode = builder.cfgCode;
        this.seatMapSvg = builder.seatMapSvg;
        this.seatMap = builder.seatMap;
        this.compMap = builder.compMap;
    }


    /* **************************************************************************************************************
     * OBJECT BUILDER
     * **************************************************************************************************************/

    /**
     * Aircraft JPA entity builder.
     */
    static class Builder {

        private final String registrationMark;

        private String modelNumber;

        private String version;

        private String cfgCode;

        private String seatMapSvg;

        private List<SeatMapEntity> seatMap;

        private List<CompMapEntity> compMap;

        Builder(final String registrationMark) {

            Assert.isTrue(StringUtils.isNotBlank(registrationMark),
                    "Registration mark cannot be null or empty!");

            this.registrationMark = registrationMark;
        }

        Builder withModelNumber(final String modelNumber) {

            this.modelNumber = modelNumber;
            return this;
        }

        Builder withVersion(final String version) {

            this.version = version;
            return this;
        }

        Builder withCfgCode(final String cfgCode) {

            this.cfgCode = cfgCode;
            return this;
        }

        Builder withSeatMapSvg(final String seatMapSvg) {

            this.seatMapSvg = seatMapSvg;
            return this;
        }

        Builder withSeatMap(final List<SeatMapEntity> seatMap) {

            this.seatMap = seatMap;
            return this;
        }

        Builder withCompMap(final List<CompMapEntity> compMap) {

            this.compMap = compMap;
            return this;
        }

        AircraftEntity build() {

            return new AircraftEntity(this);
        }
    }
}
