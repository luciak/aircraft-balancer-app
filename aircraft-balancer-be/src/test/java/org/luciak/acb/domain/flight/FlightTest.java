package org.luciak.acb.domain.flight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.luciak.acb.domain.aircraft.Aircraft;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FlightTest {

    private Aircraft aircraft;

    @BeforeEach
    void init() {

        aircraft = new Aircraft.Builder("RM").build();
    }

    @Test
    void builder() {

        // given
        LocalDate originDate = LocalDate.of(2019, 12, 31);

        /*
         * NULL UFI.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new Flight.Builder(null, aircraft, "FN",
                        originDate, "DA", "AA")
                        .build());

        /*
         * Empty UFI.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new Flight.Builder("", aircraft, "FN",
                        originDate, "DA", "AA")
                        .build());

        /*
         * Blank UFI.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new Flight.Builder(" ", aircraft, "FN",
                        originDate, "DA", "AA")
                        .build());

        /*
         * NULL aircraft.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new Flight.Builder("UFI", null, "FN",
                        originDate, "DA", "AA")
                        .build());

        /*
         * NULL flight number.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new Flight.Builder("UFI", aircraft, null,
                        originDate, "DA", "AA")
                        .build());

        /*
         * Empty flight number.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new Flight.Builder("UFI", aircraft, "",
                        originDate, "DA", "AA")
                        .build());

        /*
         * Blank flight number.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new Flight.Builder("UFI", aircraft, " ",
                        originDate, "DA", "AA")
                        .build());

        /*
         * NULL origin date.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new Flight.Builder("UFI", aircraft, "FN",
                        null, "DA", "AA")
                        .build());

        /*
         * NULL departure airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new Flight.Builder("UFI", aircraft, "FN",
                        originDate, null, "AA")
                        .build());

        /*
         * Empty departure airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new Flight.Builder("UFI", aircraft, "FN",
                        originDate, "", "AA")
                        .build());

        /*
         * Blank departure airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new Flight.Builder("UFI", aircraft, "FN",
                        originDate, " ", "AA")
                        .build());

        /*
         * NULL arrival airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new Flight.Builder("UFI", aircraft, "FN",
                        originDate, "DA", null)
                        .build());

        /*
         * Empty arrival airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new Flight.Builder("UFI", aircraft, "FN",
                        originDate, "DA", "")
                        .build());

        /*
         * Blank arrival airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new Flight.Builder("UFI", aircraft, "FN",
                        originDate, "DA", " ")
                        .build());

        /*
         * Mandatory values.
         */
        // when
        Flight flight = new Flight.Builder("UFI", aircraft, "FN",
                originDate, "DA", "AA")
                .build();

        // then
        assertNotNull(flight);
        assertEquals("UFI", flight.getUfi());
        assertNotNull(flight.getAircraft());
        assertEquals("RM", flight.getAircraft().getRegistrationMark());
        assertEquals("FN", flight.getFlightNumber());
        assertEquals(originDate, flight.getOriginDate());
        assertEquals("DA", flight.getDepartureAirport());
        assertEquals("AA", flight.getArrivalAirport());
        assertNull(flight.getStatus());
        assertNull(flight.getRepeatNumber());

        /*
         * All values.
         */
        // when
        flight = new Flight.Builder("UFI", aircraft, "FN",
                originDate, "DA", "AA")
                .withStatus(FlightStatus.BALANCED)
                .withRepeatNumber(1)
                .build();
        // then
        assertNotNull(flight);
        assertEquals(FlightStatus.BALANCED, flight.getStatus());
        assertEquals(1, flight.getRepeatNumber().intValue());
    }

    @Test
    void importPaxlist() {

        // TODO
    }

    @Test
    void deletePaxlist() {

        // TODO
    }

    @Test
    void confirmPaxlist() {

        // TODO
    }
}