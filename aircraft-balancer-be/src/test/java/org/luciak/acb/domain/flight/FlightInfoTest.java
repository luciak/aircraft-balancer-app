package org.luciak.acb.domain.flight;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FlightInfoTest {

    @Test
    void builder() {

        // given
        LocalDate originDate = LocalDate.of(2019, 12, 31);

        /*
         * NULL UFI.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightInfo.Builder(null, "RM1", "FN",
                        originDate, "DA", "AA")
                        .build());

        /*
         * Empty UFI.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightInfo.Builder("", "RM1", "FN",
                        originDate, "DA", "AA")
                        .build());

        /*
         * Blank UFI.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightInfo.Builder(" ", "RM1", "FN",
                        originDate, "DA", "AA")
                        .build());

        /*
         * NULL aircraft registration mark.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightInfo.Builder("UFI", null, "FN",
                        originDate, "DA", "AA")
                        .build());

        /*
         * Empty aircraft registration mark.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightInfo.Builder("UFI", "", "FN",
                        originDate, "DA", "AA")
                        .build());

        /*
         * Blank aircraft registration mark.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightInfo.Builder("UFI", " ", "FN",
                        originDate, "DA", "AA")
                        .build());

        /*
         * NULL flight number.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightInfo.Builder("UFI", "RM", null,
                        originDate, "DA", "AA")
                        .build());

        /*
         * Empty flight number.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightInfo.Builder("UFI", "RM", "",
                        originDate, "DA", "AA")
                        .build());

        /*
         * Blank flight number.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightInfo.Builder("UFI", "RM", " ",
                        originDate, "DA", "AA")
                        .build());

        /*
         * NULL origin date.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightInfo.Builder("UFI", "RM", "FN",
                        null, "DA", "AA")
                        .build());

        /*
         * NULL departure airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightInfo.Builder("UFI", "RM", "FN",
                        originDate, null, "AA")
                        .build());

        /*
         * Empty departure airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightInfo.Builder("UFI", "RM", "FN",
                        originDate, "", "AA")
                        .build());

        /*
         * Blank departure airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightInfo.Builder("UFI", "RM", "FN",
                        originDate, " ", "AA")
                        .build());

        /*
         * NULL arrival airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightInfo.Builder("UFI", "RM", "FN",
                        originDate, "DA", null)
                        .build());

        /*
         * Empty arrival airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightInfo.Builder("UFI", "RM", "FN",
                        originDate, "DA", "")
                        .build());

        /*
         * Blank arrival airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightInfo.Builder("UFI", "RM", "FN",
                        originDate, "DA", " ")
                        .build());

        /*
         * Mandatory values.
         */
        // when
        FlightInfo flight = new FlightInfo.Builder("UFI", "RM", "FN",
                originDate, "DA", "AA")
                .build();

        // then
        assertNotNull(flight);
        assertEquals("UFI", flight.getUfi());
        assertEquals("RM", flight.getAircraftRegistrationMark());
        assertEquals("FN", flight.getFlightNumber());
        assertEquals(originDate, flight.getOriginDate());
        assertEquals("DA", flight.getDepartureAirport());
        assertEquals("AA", flight.getArrivalAirport());
        assertNull(flight.getStatus());
        assertNull(flight.getRepeatNumber());

        /*
         * All values.
         */
        // when
        flight = new FlightInfo.Builder("UFI", "RM", "FN",
                originDate, "DA", "AA")
                .withRepeatNumber(1)
                .withStatus(FlightStatus.PREPARED)
                .build();
        // then
        assertNotNull(flight);
        assertEquals(1, flight.getRepeatNumber().intValue());
        assertEquals(FlightStatus.PREPARED, flight.getStatus());
    }
}