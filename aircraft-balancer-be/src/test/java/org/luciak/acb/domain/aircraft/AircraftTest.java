package org.luciak.acb.domain.aircraft;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AircraftTest {

    @Test
    void builder() {

        /*
         * NULL mandatory field.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> new Aircraft.Builder(null)
                .build());

        /*
         * Empty mandatory field.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> new Aircraft.Builder("")
                .build());

        /*
         * Blank mandatory field.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> new Aircraft.Builder(" ")
                .build());

        /*
         * Mandatory values.
         */
        // when
        Aircraft aircraft = new Aircraft.Builder("regMark1")
                .build();

        // then
        assertNotNull(aircraft);
        assertEquals("regMark1", aircraft.getRegistrationMark());
        assertNull(aircraft.getModelNumber());
        assertNull(aircraft.getVersion());
        assertNull(aircraft.getCfgCode());

        /*
         * All values.
         */
        // when
        aircraft = new Aircraft.Builder("regMark1")
                .withModelNumber("modelNum1")
                .withVersion("version1")
                .withCfgCode("cfgCode1")
                .build();

        // then
        assertNotNull(aircraft);
        assertEquals("regMark1", aircraft.getRegistrationMark());
        assertEquals("modelNum1", aircraft.getModelNumber());
        assertEquals("version1", aircraft.getVersion());
        assertEquals("cfgCode1", aircraft.getCfgCode());
    }
}