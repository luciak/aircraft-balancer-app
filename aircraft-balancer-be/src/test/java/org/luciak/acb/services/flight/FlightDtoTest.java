package org.luciak.acb.services.flight;

import org.junit.jupiter.api.Test;
import org.luciak.acb.domain.aircraft.Aircraft;
import org.luciak.acb.domain.flight.FlightStatus;
import org.luciak.acb.services.aircraft.AircraftDto;
import org.luciak.acb.services.aircraft.AircraftDtoMapper;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Test for flight DTO.
 *
 * @author Lucia Kleinova
 */
class FlightDtoTest {

    @Test
    void builder() {

        // given
        LocalDate originDate = LocalDate.of(2019, 4, 5);
        AircraftDto aircraft = AircraftDtoMapper.mapGeneralInfoToAircraftDto(
                new Aircraft.Builder("RM1").build());

        /*
         * NULL aircraft.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightDto.Builder(
                        null,
                        "FN1",
                        originDate,
                        "DA1",
                        "AA1")
                        .build());

        /*
         * NULL flight number.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightDto.Builder(
                        aircraft,
                        null,
                        originDate,
                        "DA1",
                        "AA1")
                        .build());

        /*
         * Empty flight number.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightDto.Builder(
                        aircraft,
                        "",
                        originDate,
                        "DA1",
                        "AA1")
                        .build());

        /*
         * Blank flight number.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightDto.Builder(
                        aircraft,
                        " ",
                        originDate,
                        "DA1",
                        "AA1")
                        .build());

        /*
         * NULL origin date.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightDto.Builder(
                        aircraft,
                        "FN1",
                        null,
                        "DA1",
                        "AA1")
                        .build());

        /*
         * NULL destination airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightDto.Builder(
                        aircraft,
                        "FN1",
                        originDate,
                        null,
                        "AA1")
                        .build());

        /*
         * Empty destination airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightDto.Builder(
                        aircraft,
                        "FN1",
                        originDate,
                        "",
                        "AA1")
                        .build());

        /*
         * NULL destination airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightDto.Builder(
                        aircraft,
                        "FN1",
                        originDate,
                        " ",
                        "AA1")
                        .build());

        /*
         * NULL arrival airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightDto.Builder(
                        aircraft,
                        "FN1",
                        originDate,
                        "DA1",
                        null)
                        .build());

        /*
         * NULL arrival airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightDto.Builder(
                        aircraft,
                        "FN1",
                        originDate,
                        "DA1",
                        "")
                        .build());

        /*
         * NULL arrival airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightDto.Builder(
                        aircraft,
                        "FN1",
                        originDate,
                        "DA1",
                        " ")
                        .build());

        /*
         * Mandatory values.
         */
        // then
        FlightDto flightDto = new FlightDto.Builder(
                aircraft,
                "FN1",
                originDate,
                "DA1",
                "AA1")
                .build();
        assertNotNull(flightDto);
        assertNotNull(flightDto.getAircraft());
        assertEquals("RM1", flightDto.getAircraft().getRegistrationMark());
        assertEquals("FN1", flightDto.getFlightNumber());
        assertEquals(originDate, flightDto.getOriginDate());
        assertEquals("DA1", flightDto.getDepartureAirport());
        assertEquals("AA1", flightDto.getArrivalAirport());
        assertNull(flightDto.getUfi());
        assertNull(flightDto.getRepeatNumber());
        assertNull(flightDto.getStatus());

        /*
         * All values.
         */
        // then
        flightDto = new FlightDto.Builder(
                aircraft,
                "FN1",
                originDate,
                "DA1",
                "AA1")
                .withUfi("UFI")
                .withRepeatNumber(3)
                .withStatus(FlightStatus.PREPARED)
                .build();
        assertNotNull(flightDto);
        assertEquals("UFI", flightDto.getUfi());
        assertEquals(FlightStatus.PREPARED, flightDto.getStatus());
        assertEquals(3, flightDto.getRepeatNumber().intValue());
    }
}