package org.luciak.acb.services.aircraft;

import org.junit.jupiter.api.Test;
import org.luciak.acb.domain.aircraft.Aircraft;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AircraftDtoMapperTest {

    @Test
    void mapGeneralInfoToAircraftDto() {

        /*
         * NULL mandatory value.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> AircraftDtoMapper.mapGeneralInfoToAircraftDto(null));

        /*
         * Mandatory values.
         */
        // given
        Aircraft aircraft = new Aircraft.Builder("regMark1")
                .build();
        // when
        AircraftDto aircraftDto = AircraftDtoMapper.mapGeneralInfoToAircraftDto(aircraft);
        // then
        assertNotNull(aircraftDto);
        assertEquals("regMark1", aircraftDto.getRegistrationMark());
        assertNull(aircraftDto.getModelNumber());
        assertNull(aircraftDto.getVersion());
        assertNull(aircraftDto.getCfgCode());

        /*
         * All values.
         */
        // given
        aircraft = new Aircraft.Builder("regMark1")
                .withModelNumber("modelNum1")
                .withVersion("version1")
                .withCfgCode("cfgCode1")
                .build();
        // when
        aircraftDto = AircraftDtoMapper.mapGeneralInfoToAircraftDto(aircraft);
        // then
        assertNotNull(aircraftDto);
        assertEquals("regMark1", aircraftDto.getRegistrationMark());
        assertEquals("modelNum1", aircraftDto.getModelNumber());
        assertEquals("version1", aircraftDto.getVersion());
        assertEquals("cfgCode1", aircraftDto.getCfgCode());
    }

    @Test
    void mapDetailedInfoToAircraftDto() {

        /*
         * NULL mandatory value.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> AircraftDtoMapper.mapDetailedInfoToAircraftDto(null));

        /*
         * Mandatory values.
         */
        // given
        Aircraft aircraft = new Aircraft.Builder("regMark1")
                .build();

        // when
        AircraftDto aircraftDto = AircraftDtoMapper.mapDetailedInfoToAircraftDto(aircraft);

        // then
        assertNotNull(aircraftDto);
        assertEquals("regMark1", aircraftDto.getRegistrationMark());
        assertNull(aircraftDto.getModelNumber());
        assertNull(aircraftDto.getVersion());
        assertNull(aircraftDto.getCfgCode());

        /*
         * All values.
         */
        // TODO
    }
}