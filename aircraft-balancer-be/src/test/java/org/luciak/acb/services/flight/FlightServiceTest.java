package org.luciak.acb.services.flight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.luciak.acb.domain.aircraft.Aircraft;
import org.luciak.acb.domain.flight.Flight;
import org.luciak.acb.domain.flight.FlightInfo;
import org.luciak.acb.domain.flight.FlightStatus;
import org.luciak.acb.exceptions.AircraftNotFoundException;
import org.luciak.acb.exceptions.FlightAlreadyExistsException;
import org.luciak.acb.exceptions.FlightNotFoundException;
import org.luciak.acb.exceptions.IncorrectStateOfFlightException;
import org.luciak.acb.repositories.flight.FlightFactory;
import org.luciak.acb.repositories.flight.FlightRepository;
import org.luciak.acb.repositories.flight.PaxFactory;
import org.luciak.acb.services.aircraft.AircraftDto;
import org.luciak.acb.services.aircraft.AircraftDtoMapper;
import org.luciak.acb.services.baggage.BaggagePredicationService;
import org.luciak.acb.services.baggage.SimpleBaggagePredicationService;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class FlightServiceTest {

    private FlightService flightService;

    @Mock
    private FlightRepository flightRepository;

    @Mock
    private FlightFactory flightFactory;

    @Mock
    private PaxFactory paxFactory;

    @BeforeEach
    void init() {

        flightService = new FlightService(flightRepository, flightFactory, paxFactory,
                new SimpleBaggagePredicationService());
    }

    @Test
    void findAllFlights() {

        /*
         * Empty result from repo.
         */
        // given
        Mockito.when(flightRepository.findAllFlights()).thenReturn(Collections.emptyList());
        // when
        List<FlightDto> flightDtos = flightService.findAllFlights();

        // then
        assertNotNull(flightDtos);
        assertTrue(flightDtos.isEmpty());

        /*
         * Multiple results from repo.
         */
        // given
        List<FlightInfo> flights = new ArrayList<>();
        flights.add(new FlightInfo.Builder("ufi1",
                "RegMark1",
                "FN1",
                LocalDate.of(2019, 1, 1),
                "DA1",
                "AA1")
                .build());
        flights.add(new FlightInfo.Builder("ufi2",
                "RegMark2",
                "FN2",
                LocalDate.of(2019, 1, 2),
                "DA2",
                "AA2")
                .build());
        Mockito.when(flightRepository.findAllFlights()).thenReturn(flights);
        // when`
        flightDtos = flightService.findAllFlights();
        // then
        assertNotNull(flightDtos);
        assertEquals(2, flightDtos.size());
        assertEquals("ufi1", flightDtos.get(0).getUfi());
        assertEquals("ufi2", flightDtos.get(1).getUfi());
    }

    @Test
    void findFlightsByStatus() {

        /*
         * NULL flight state.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> flightService.findFlightsByStatus(null));

        /*
         * Empty result from repo.
         */
        // given
        Mockito.when(flightRepository.findAllFlightsByStatus(Mockito.any(FlightStatus.class)))
                .thenReturn(Collections.emptyList());
        // when
        List<FlightDto> flightDtos = flightService.findFlightsByStatus(FlightStatus.PREPARED);
        // then
        assertNotNull(flightDtos);
        assertTrue(flightDtos.isEmpty());

        /*
         * Multiple results from repo.
         */
        // given
        List<FlightInfo> flights = new ArrayList<>();
        flights.add(new FlightInfo.Builder("ufi1",
                "RegMark1",
                "FN1",
                LocalDate.of(2019, 1, 1),
                "DA1",
                "AA1")
                .build());
        flights.add(new FlightInfo.Builder("ufi2",
                "RegMark2",
                "FN2",
                LocalDate.of(2019, 1, 2),
                "DA2",
                "AA2")
                .build());
        Mockito.when(flightRepository.findAllFlightsByStatus(Mockito.any(FlightStatus.class))).thenReturn(flights);
        // when`
        flightDtos = flightService.findFlightsByStatus(FlightStatus.CREATED);
        // then
        assertNotNull(flightDtos);
        assertEquals(2, flightDtos.size());
        assertEquals("ufi1", flightDtos.get(0).getUfi());
        assertEquals("ufi2", flightDtos.get(1).getUfi());
    }

    @Test
    void getFlight() throws FlightNotFoundException {

        /*
         * NULL UFI.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> flightService.getFlight(null));

        /*
         * Empty UFI.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> flightService.getFlight(""));

        /*
         * Blank UFI.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> flightService.getFlight(" "));

        /*
         * NULL result from repo.
         */
        // given
        Mockito.when(flightRepository.getFlight(Mockito.anyString())).thenReturn(null);
        // then
        assertThrows(IllegalArgumentException.class, () -> flightService.getFlight("ufi"));

        /*
         * Correct result from repo.
         */
        // given
        Aircraft aircraft = new Aircraft.Builder("RM1").build();
        LocalDate originDate = LocalDate.of(2019, 5, 6);
        Flight flight = new Flight.Builder("ufi1",
                aircraft,
                "FN1",
                originDate,
                "DA1",
                "AA1")
                .build();
        Mockito.when(flightRepository.getFlight(Mockito.anyString())).thenReturn(flight);
        // when
        FlightDto flightDto = flightService.getFlight("ufi1");
        // then
        assertNotNull(flightDto);
        assertEquals("ufi1", flightDto.getUfi());
        assertNotNull(flightDto.getAircraft());
        assertEquals("RM1", flightDto.getAircraft().getRegistrationMark());
        assertEquals("FN1", flightDto.getFlightNumber());
        assertEquals(originDate, flightDto.getOriginDate());
        assertEquals("DA1", flightDto.getDepartureAirport());
        assertEquals("AA1", flightDto.getArrivalAirport());
    }


    @Test
    void createFlight() throws AircraftNotFoundException, FlightAlreadyExistsException {

        /*
         * Missing mandatory fields.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> flightService.createFlight(null));

        // given
        LocalDate originDate = LocalDate.of(2018, 5, 25);
        Aircraft aircraft = new Aircraft.Builder("RM").build();
        AircraftDto aircraftDto = AircraftDtoMapper.mapGeneralInfoToAircraftDto(aircraft);
        FlightDto flightDto = new FlightDto.Builder(aircraftDto,
                "FN",
                originDate,
                "DA",
                "AA")
                .withRepeatNumber(1)
                .build();
        Flight flight = new Flight.Builder("UFI",
                aircraft,
                "FN",
                originDate,
                "DA",
                "AA")
                .withRepeatNumber(1)
                .build();

        /*
         * Flight exists.
         */
        // given
        Mockito.when(flightFactory.createFlight(
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.any(LocalDate.class),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyInt()))
                .thenReturn(flight);
        Mockito.when(flightRepository.exists(Mockito.eq("UFI"))).thenReturn(true);
        // then
        assertThrows(FlightAlreadyExistsException.class, () -> flightService.createFlight(flightDto));

        /*
         * Aircraft configuration doesn't exists
         */
        // given
        Mockito.when(flightFactory.createFlight(
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.any(LocalDate.class),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyInt()))
                .thenThrow(new AircraftNotFoundException("Aircraft not found!"));
        // then
        assertThrows(AircraftNotFoundException.class, () -> flightService.createFlight(flightDto));

        /*
         * Correct metadata.
         */
        // given
        Mockito.when(flightFactory.createFlight(
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.any(LocalDate.class),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyInt()))
                .thenReturn(flight);
        Mockito.when(flightRepository.exists(Mockito.eq("UFI"))).thenReturn(false);
        // when
        FlightDto result = flightService.createFlight(flightDto);
        // then
        Mockito.verify(flightRepository, Mockito.times(1))
                .persistFlight(Mockito.eq(flight));
        assertNotNull(result);
        assertEquals("UFI", result.getUfi());
        assertNotNull(result.getAircraft());
        assertEquals("RM", result.getAircraft().getRegistrationMark());
        assertEquals("FN", result.getFlightNumber());
        assertEquals(originDate, result.getOriginDate());
        assertEquals("DA", result.getDepartureAirport());
        assertEquals("AA", result.getArrivalAirport());
    }

    @Test
    void updateFligh() throws AircraftNotFoundException, FlightAlreadyExistsException,
            FlightNotFoundException, IncorrectStateOfFlightException {

        // given
        LocalDate originDate = LocalDate.of(2017, 1, 1);
        Flight flight = new Flight.Builder(
                "UFI",
                new Aircraft.Builder("RM").build(),
                "FN",
                originDate,
                "DA",
                "AA")
                .withStatus(FlightStatus.CREATED)
                .withRepeatNumber(1)
                .build();
        FlightDto flightDto = FlightDtoMapper.mapFlightToFlightDto(flight);

        /*
         * NULL UFI
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                flightService.updateFlight(null, flightDto));

        /*
         * Enpty UFI
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                flightService.updateFlight("", flightDto));

        /*
         * Blank UFI
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                flightService.updateFlight(" ", flightDto));

        /*
         * NULL flightDto
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                flightService.updateFlight("UFI", null));

        /*
         * Original flight doesn't exists.
         */
        // given
        Mockito.when(flightRepository.getFlight(Mockito.eq("NotExistingUFI")))
                .thenThrow(new FlightNotFoundException("Flight doesn't exists!"));
        // then
        assertThrows(FlightNotFoundException.class, () ->
                flightService.updateFlight("NotExistingUFI", flightDto));

        /*
         * Target flight already exists. UFI has been changed.
         */
        // given
        Mockito.when(flightRepository.getFlight(Mockito.eq("OriginalUFI")))
                .thenReturn(flight);
        Mockito.when(flightFactory.createFlight(
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.any(LocalDate.class),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyInt()))
                .thenReturn(flight);
        Mockito.when(flightRepository.exists(Mockito.eq("UFI"))).thenReturn(true);

        // then
        assertThrows(FlightAlreadyExistsException.class, () ->
                flightService.updateFlight("OriginalUFI", flightDto));

        /*
         * Aircraft configuration doesn't exists.
         */
        // given
        Mockito.when(flightRepository.getFlight(Mockito.eq("UFI")))
                .thenReturn(flight);
        Mockito.when(flightFactory.createFlight(
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.any(LocalDate.class),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyInt()))
                .thenThrow(new AircraftNotFoundException("Aircraft doesn't exist!!"));

        // then
        assertThrows(AircraftNotFoundException.class, () ->
                flightService.updateFlight("UFI", flightDto));

        /*
         * Flight already contains other information (flight is not in CREATED state).
         */
        // given
        Flight preparedFlight = new Flight.Builder(
                "UFI",
                new Aircraft.Builder("RM").build(),
                "FN",
                originDate,
                "DA",
                "AA")
                .withStatus(FlightStatus.PREPARED)
                .withRepeatNumber(1)
                .build();
        Mockito.when(flightRepository.getFlight(Mockito.eq("UFI")))
                .thenReturn(preparedFlight);
        // then
        assertThrows(IncorrectStateOfFlightException.class, () ->
                flightService.updateFlight("UFI", flightDto));

        /*
         * Correct metadata. UFI has not been changed.
         */
        // given
        Mockito.when(flightRepository.getFlight(Mockito.eq("UFI")))
                .thenReturn(flight);
        Mockito.when(flightFactory.createFlight(
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.any(LocalDate.class),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyInt()))
                .thenReturn(flight);
        // when
        FlightDto result = flightService.updateFlight("UFI", flightDto);
        // then
        Mockito.verify(flightRepository, Mockito.times(1))
                .deleteFlight(Mockito.eq("UFI"));
        Mockito.verify(flightRepository, Mockito.times(1))
                .persistFlight(Mockito.eq(flight));
        assertNotNull(result);
        assertEquals("UFI", result.getUfi());
        assertNotNull(result.getAircraft());
        assertEquals("RM", result.getAircraft().getRegistrationMark());
        assertEquals("FN", result.getFlightNumber());
        assertEquals(originDate, result.getOriginDate());
        assertEquals("DA", result.getDepartureAirport());
        assertEquals("AA", result.getArrivalAirport());

        /*
         * Correct metadata. UFI has been changed.
         */
        // given
        Mockito.when(flightRepository.getFlight(Mockito.eq("OriginalUFI")))
                .thenReturn(flight);
        Mockito.when(flightFactory.createFlight(
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.any(LocalDate.class),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyInt()))
                .thenReturn(flight);
        Mockito.when(flightRepository.exists(Mockito.eq("UFI"))).thenReturn(false);
        // when
        result = flightService.updateFlight("OriginalUFI", flightDto);
        // then
        Mockito.verify(flightRepository, Mockito.times(1))
                .deleteFlight(Mockito.eq("OriginalUFI"));
        Mockito.verify(flightRepository, Mockito.times(2))
                .persistFlight(Mockito.eq(flight));
        assertNotNull(result);
        assertEquals("UFI", result.getUfi());
        assertNotNull(result.getAircraft());
        assertEquals("RM", result.getAircraft().getRegistrationMark());
        assertEquals("FN", result.getFlightNumber());
        assertEquals(originDate, result.getOriginDate());
        assertEquals("DA", result.getDepartureAirport());
        assertEquals("AA", result.getArrivalAirport());

    }

    @Test
    void deleteFlight() throws FlightNotFoundException {

        /*
         * NULL UFI.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> flightService.deleteFlight(null));

        /*
         * Empty UFI.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> flightService.deleteFlight(""));

        /*
         * Blank UFI.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> flightService.deleteFlight(" "));

        /*
         * Target flight not found.
         */
        Mockito.when(flightRepository.exists(Mockito.eq("UFI"))).thenReturn(false);
        assertThrows(FlightNotFoundException.class, () -> flightService.deleteFlight("UFI"));

        /*
         * Correct.
         */
        Mockito.when(flightRepository.exists(Mockito.eq("UFI"))).thenReturn(true);
        flightService.deleteFlight("UFI");
        Mockito.verify(flightRepository, Mockito.times(1))
                .deleteFlight(Mockito.eq("UFI"));
    }

    @Test
    void uploadPaxlist() {

        // TODO
    }

    @Test
    void deletePaxlist() {

        // TODO
    }

    @Test
    void getPaxlist() {

        // TODO
    }

    @Test
    void confirmPaxlist() {

        // TODO
    }
}