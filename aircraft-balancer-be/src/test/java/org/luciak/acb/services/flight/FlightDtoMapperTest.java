package org.luciak.acb.services.flight;

import org.junit.jupiter.api.Test;
import org.luciak.acb.domain.aircraft.Aircraft;
import org.luciak.acb.domain.flight.Flight;
import org.luciak.acb.domain.flight.FlightInfo;
import org.luciak.acb.domain.flight.FlightStatus;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FlightDtoMapperTest {

    @Test
    void mapFlightInfoToFlightDto() {

        /*
         * NULL argument.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> FlightDtoMapper.mapFlightInfoToFlightDto(null));

        /*
         * Mandatory values.
         */
        // given
        LocalDate originDate = LocalDate.of(2018, 2, 3);
        FlightInfo flight = new FlightInfo.Builder("Ufi1",
                "RegMark1",
                "Fn1",
                originDate,
                "DA1",
                "AA1")
                .build();
        // when
        FlightDto flightDto = FlightDtoMapper.mapFlightInfoToFlightDto(flight);
        // then
        assertNotNull(flightDto);
        assertEquals("Ufi1", flightDto.getUfi());
        assertNotNull(flightDto.getAircraft());
        assertEquals("RegMark1", flightDto.getAircraft().getRegistrationMark());
        assertEquals(originDate, flightDto.getOriginDate());
        assertEquals("DA1", flightDto.getDepartureAirport());
        assertEquals("AA1", flightDto.getArrivalAirport());
        assertNull(flightDto.getStatus());
        assertNull(flightDto.getRepeatNumber());
        /*
         * All values.
         */
        // given
        flight = new FlightInfo.Builder("Ufi1",
                "RegMark1",
                "FN1",
                originDate,
                "DA1",
                "AA1")
                .withRepeatNumber(2)
                .withStatus(FlightStatus.CLOSED)
                .build();
        // when
        flightDto = FlightDtoMapper.mapFlightInfoToFlightDto(flight);
        assertNotNull(flightDto);
        assertEquals(2, flightDto.getRepeatNumber().intValue());
        assertEquals(FlightStatus.CLOSED, flightDto.getStatus());
    }

    @Test
    void mapFlightToFlightDto() {

        /*
         * NULL argument.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> FlightDtoMapper.mapFlightToFlightDto(null));

        /*
         * Mandatory values.
         */
        // given
        LocalDate originDate = LocalDate.of(2019, 10, 30);
        Aircraft aircraft = new Aircraft.Builder("RM1").build();
        Flight flight = new Flight.Builder("ufi1",
                aircraft,
                "FN1",
                originDate,
                "DA1",
                "AA1")
                .build();
        // when
        FlightDto flightDto = FlightDtoMapper.mapFlightToFlightDto(flight);
        // then
        assertNotNull(flightDto);
        assertEquals("ufi1", flightDto.getUfi());
        assertNotNull(flightDto.getAircraft());
        assertEquals("RM1", flightDto.getAircraft().getRegistrationMark());
        assertEquals(originDate, flightDto.getOriginDate());
        assertEquals("DA1", flightDto.getDepartureAirport());
        assertEquals("AA1", flightDto.getArrivalAirport());
        assertNull(flightDto.getStatus());
        assertNull(flightDto.getRepeatNumber());

        /*
         * All values.
         */
        flight = new Flight.Builder("ufi1",
                aircraft,
                "FN1",
                originDate,
                "DA1",
                "AA1")
                .withStatus(FlightStatus.PREPARED)
                .withRepeatNumber(1)
                .build();
        // when
        flightDto = FlightDtoMapper.mapFlightToFlightDto(flight);
        assertNotNull(flightDto);
        assertEquals(1, flightDto.getRepeatNumber().intValue());
        assertEquals(FlightStatus.PREPARED, flightDto.getStatus());
    }
}