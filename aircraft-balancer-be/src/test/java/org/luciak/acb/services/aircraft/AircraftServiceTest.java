package org.luciak.acb.services.aircraft;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.luciak.acb.domain.aircraft.Aircraft;
import org.luciak.acb.exceptions.AircraftNotFoundException;
import org.luciak.acb.repositories.aircraft.AircraftRepository;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * Test of Aircraft service.
 *
 * @author Lucia Kleinova
 */
@ExtendWith(MockitoExtension.class)
class AircraftServiceTest {

    private AircraftService aircraftService;

    @Mock
    private AircraftRepository aircraftRepository;

    @BeforeEach
    void init() {

        this.aircraftService = new AircraftService(aircraftRepository);
    }

    @Test
    void findAircraftConfigurations() {

        /*
         * Empty result from repo.
         */
        // given
        List<Aircraft> configurations = new ArrayList<>();
        Mockito.when(aircraftRepository.findAircraftConfigurations()).thenReturn(configurations);
        // when
        List<AircraftDto> result = aircraftService.findAircraftConfigurations();
        // then
        assertNotNull(result);
        assertTrue(result.isEmpty());

        /*
         * Multiple result from repo.
         */
        // given
        configurations = new ArrayList<>();
        configurations.add(new Aircraft.Builder("regMark1").build());
        configurations.add(new Aircraft.Builder("regMark2").build());
        Mockito.when(aircraftRepository.findAircraftConfigurations()).thenReturn(configurations);
        // when
        result = aircraftService.findAircraftConfigurations();
        // then
        assertNotNull(result);
        assertEquals(result.size(), 2);
        assertEquals("regMark1", result.get(0).getRegistrationMark());
        assertEquals("regMark2", result.get(1).getRegistrationMark());
    }

    @Test
    void getAircraftConfiguration() throws AircraftNotFoundException {

        /*
         * NULL registration mark.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                aircraftService.getAircraftConfiguration(null));

        /*
         * NULL response from repo.
         */
        // given
        Mockito.when(aircraftRepository.getAircraftConfiguration(Mockito.anyString())).thenReturn(null);
        // then
        assertThrows(IllegalArgumentException.class, () ->
                aircraftService.getAircraftConfiguration("regNum"));

        /*
         * Correct result from repo.
         */
        // given
        Mockito.when(aircraftRepository.getAircraftConfiguration(Mockito.anyString()))
                .thenReturn(new Aircraft.Builder("regNum").build());

        // when
        AircraftDto result = aircraftService.getAircraftConfiguration("regNum");

        // then
        assertNotNull(result);
        assertEquals("regNum", result.getRegistrationMark());
    }

    @Test
    void getSeatMapSvg() {

        // TODO
    }
}