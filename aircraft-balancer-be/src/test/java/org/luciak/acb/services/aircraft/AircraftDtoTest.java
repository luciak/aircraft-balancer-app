package org.luciak.acb.services.aircraft;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Test for aircraft DTO.
 *
 * @author Lucia Kleinova
 */
class AircraftDtoTest {

    @Test
    void builder() {

        /*
         * NULL registration mark.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> new AircraftDto.Builder(null)
                .build());

        /*
         * Mandatory fields.
         */
        // when
        AircraftDto aircraftDto = new AircraftDto.Builder("regNum")
                .build();
        // then
        assertNotNull(aircraftDto);
        assertEquals("regNum", aircraftDto.getRegistrationMark());
        assertNull(aircraftDto.getModelNumber());
        assertNull(aircraftDto.getVersion());
        assertNull(aircraftDto.getCfgCode());

        /*
         * All values
         */
        // when
        aircraftDto = new AircraftDto.Builder("regNum")
                .withModelNumber("modelNum")
                .withVersion("ver")
                .withCfgCode("cfgCode")
                .build();

        // then
        assertNotNull(aircraftDto);
        assertEquals("regNum", aircraftDto.getRegistrationMark());
        assertEquals("modelNum", aircraftDto.getModelNumber());
        assertEquals("ver", aircraftDto.getVersion());
        assertEquals("cfgCode", aircraftDto.getCfgCode());
    }
}