package org.luciak.acb.repositories.aircraft;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

/**
 * Test of aircraft entity.
 *
 * @author Lucia Kleinova
 */
class AircraftEntityTest {

    @Test
    void builder() {

        /*
         * NULL registration mark.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> new AircraftEntity.Builder(null)
                .build());

        /*
         * Empty registration mark.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> new AircraftEntity.Builder("")
                .build());

        /*
         * Blank registration mark.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> new AircraftEntity.Builder(" ")
                .build());

        /*
         * Mandatory values.
         */
        // when
        AircraftEntity aircraftEntity = new AircraftEntity.Builder("regNum")
                .build();
        // then
        assertNotNull(aircraftEntity);
        assertEquals("regNum", aircraftEntity.getRegistrationMark());
        assertNull(aircraftEntity.getModelNumber());
        assertNull(aircraftEntity.getVersion());
        assertNull(aircraftEntity.getCfgCode());

        /*
         * All values.
         */
        // when
        aircraftEntity = new AircraftEntity.Builder("regNum")
                .withModelNumber("modelNum")
                .withVersion("ver")
                .withCfgCode("conf")
                .build();
        // then
        assertNotNull(aircraftEntity);
        assertEquals("regNum", aircraftEntity.getRegistrationMark());
        assertEquals("modelNum", aircraftEntity.getModelNumber());
        assertEquals("ver", aircraftEntity.getVersion());
        assertEquals("conf", aircraftEntity.getCfgCode());
    }
}