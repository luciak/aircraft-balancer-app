package org.luciak.acb.repositories.aircraft;

import org.junit.jupiter.api.Test;
import org.luciak.acb.domain.aircraft.Aircraft;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AircraftEntityMapperTest {

    @Test
    void mapToAircraft() {

        /*
         * NULL argument.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> AircraftEntityMapper.mapToAircraft(null));

        /*
         * Mandatory values.
         */
        // given
        AircraftEntity aircraftEntity = new AircraftEntity.Builder("regMark1")
                .build();
        // when
        Aircraft aircraft = AircraftEntityMapper.mapToAircraft(aircraftEntity);
        // then
        assertNotNull(aircraft);
        assertEquals("regMark1", aircraft.getRegistrationMark());
        assertNull(aircraft.getModelNumber());
        assertNull(aircraft.getVersion());

        /*
         * All values.
         */
        // given
        aircraftEntity = new AircraftEntity.Builder("regMark1")
                .withModelNumber("modelNum1")
                .withVersion("version1")
                .withCfgCode("cfgCode1")
                .build();
        // when
        aircraft = AircraftEntityMapper.mapToAircraft(aircraftEntity);
        // then
        assertNotNull(aircraft);
        assertEquals("regMark1", aircraft.getRegistrationMark());
        assertEquals("modelNum1", aircraft.getModelNumber());
        assertEquals("version1", aircraft.getVersion());
        assertEquals("cfgCode1", aircraft.getCfgCode());
    }

    @Test
    void mapToAircraftEntity() {

        /*
         * NULL argument.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> AircraftEntityMapper.mapToAircraft(null));

        /*
         * Mandatory values
         */
        // given
        Aircraft aircraft = new Aircraft.Builder("RM").build();
        // when
        AircraftEntity aircraftEntity = AircraftEntityMapper.mapToAircraftEntity(aircraft);
        // then
        assertNotNull(aircraftEntity);
        assertEquals("RM", aircraftEntity.getRegistrationMark());
        assertNull(aircraftEntity.getModelNumber());
        assertNull(aircraftEntity.getVersion());
        assertNull(aircraftEntity.getCfgCode());

        /*
         * All values
         */
        // given
        aircraft = new Aircraft.Builder("RM")
                .withModelNumber("MN")
                .withVersion("Ver")
                .withCfgCode("Cfg")
                .build();
        // when
        aircraftEntity = AircraftEntityMapper.mapToAircraftEntity(aircraft);
        // then
        assertNotNull(aircraftEntity);
        assertEquals("RM", aircraftEntity.getRegistrationMark());
        assertEquals("MN", aircraftEntity.getModelNumber());
        assertEquals("Ver", aircraftEntity.getVersion());
        assertEquals("Cfg", aircraftEntity.getCfgCode());
    }
}