package org.luciak.acb.repositories.flight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.luciak.acb.domain.aircraft.Aircraft;
import org.luciak.acb.domain.flight.Flight;
import org.luciak.acb.domain.flight.FlightInfo;
import org.luciak.acb.domain.flight.FlightStatus;
import org.luciak.acb.exceptions.FlightNotFoundException;
import org.luciak.acb.repositories.aircraft.AircraftEntity;
import org.luciak.acb.repositories.aircraft.AircraftEntityMapper;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class FlightRepositoryTest {

    private FlightRepository flightRepository;

    @Mock
    private FlightEntityManager flightEntityManager;

    @BeforeEach
    void setUp() {

        flightRepository = new FlightRepository(flightEntityManager);
    }

    @Test
    void findAllFlights() {

        /*
         * NULL result from DB.
         */
        // given
        Mockito.when(flightEntityManager.findAllByOrderByOriginDateAsc()).thenReturn(null);
        // when
        List<FlightInfo> flights = flightRepository.findAllFlights();
        // then
        assertNotNull(flights);
        assertTrue(flights.isEmpty());

        /*
         * Empty result from DB.
         */
        // given
        Mockito.when(flightEntityManager.findAllByOrderByOriginDateAsc()).thenReturn(Collections.emptyList());
        // when
        flights = flightRepository.findAllFlights();
        // then
        assertNotNull(flights);
        assertTrue(flights.isEmpty());

        /*
         * Multiple results from DB.
         */
        // given
        LocalDate originDate = LocalDate.of(2018, 5, 4);
        List<FlightEntity> flightEntities = new ArrayList<>();
        AircraftEntity aircraftEntity= AircraftEntityMapper.mapToAircraftEntity(
                new Aircraft.Builder("RM1").build());
        flightEntities.add(new FlightEntity.Builder(
                "FN1 2018-05-04 DA1-AA1",
                aircraftEntity,
                "FN1",
                originDate,
                "DA1",
                "AA1")
                .build());
        flightEntities.add(new FlightEntity.Builder(
                "FN2 2018-05-04 DA2-AA2",
                aircraftEntity,
                "FN2",
                originDate,
                "DA2",
                "AA2")
                .build());
        Mockito.when(flightEntityManager.findAllByOrderByOriginDateAsc()).thenReturn(flightEntities);
        // when
        flights = flightRepository.findAllFlights();
        // then
        assertNotNull(flights);
        assertEquals(2, flights.size());
        assertEquals("FN1 2018-05-04 DA1-AA1", flights.get(0).getUfi());
        assertEquals("FN2 2018-05-04 DA2-AA2", flights.get(1).getUfi());
    }

    @Test
    void findFlightsByStatus() {

        /*
         * NULL status argument.
         */
        assertThrows(IllegalArgumentException.class, () -> flightRepository.findAllFlightsByStatus(null));

        /*
         * NULL result from DB.
         */
        // given
        Mockito.when(flightEntityManager.findAllByStatusOrderByOriginDateAsc(Mockito.any(FlightStatus.class)))
                .thenReturn(null);
        // when
        List<FlightInfo> flights = flightRepository.findAllFlightsByStatus(FlightStatus.CREATED);
        // then
        assertNotNull(flights);
        assertTrue(flights.isEmpty());

        /*
         * Empty result from DB.
         */
        // given
        Mockito.when(flightEntityManager.findAllByStatusOrderByOriginDateAsc(Mockito.any(FlightStatus.class)))
                .thenReturn(Collections.emptyList());
        // when
        flights = flightRepository.findAllFlightsByStatus(FlightStatus.CREATED);
        // then
        assertNotNull(flights);
        assertTrue(flights.isEmpty());

        /*
         * Multiple results from DB.
         */
        // given
        LocalDate originDate = LocalDate.of(2018, 5, 4);
        AircraftEntity aircraftEntity= AircraftEntityMapper.mapToAircraftEntity(
                new Aircraft.Builder("RM1").build());
        List<FlightEntity> flightEntities = new ArrayList<>();
        flightEntities.add(new FlightEntity.Builder(
                "FN1 2018-05-04 DA1-AA1",
                aircraftEntity,
                "FN1",
                originDate,
                "DA1",
                "AA1")
                .build());
        flightEntities.add(new FlightEntity.Builder(
                "FN2 2018-05-04 DA2-AA2",
                aircraftEntity,
                "FN2",
                originDate,
                "DA2",
                "AA2")
                .build());
        Mockito.when(flightEntityManager.findAllByStatusOrderByOriginDateAsc(Mockito.any(FlightStatus.class)))
                .thenReturn(flightEntities);
        // when
        flights = flightRepository.findAllFlightsByStatus(FlightStatus.CREATED);
        // then
        assertNotNull(flights);
        assertEquals(2, flights.size());
        assertEquals("FN1 2018-05-04 DA1-AA1", flights.get(0).getUfi());
        assertEquals("FN2 2018-05-04 DA2-AA2", flights.get(1).getUfi());
    }

    @Test
    void getFlight() throws FlightNotFoundException {

        /*
         * NULL UFI argument.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> flightRepository.getFlight(null));

        /*
         * Empty UFI argument.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> flightRepository.getFlight(""));

        /*
         * Blank UFI argument.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> flightRepository.getFlight(" "));

        /*
         * NULL result from DB.
         */
        // given
        Mockito.when(flightEntityManager.findByUfi(Mockito.anyString())).thenReturn(null);
        // then
        assertThrows(FlightNotFoundException.class, () -> flightRepository.getFlight("ufi"));

        /*
         * Correct result from DB.
         */
        // given
        LocalDate originDate = LocalDate.of(2019, 7, 8);
        AircraftEntity aircraftEntity= AircraftEntityMapper.mapToAircraftEntity(
                new Aircraft.Builder("RM1").build());
        FlightEntity entity = new FlightEntity.Builder(
                "FN1 2019-07-08 DA1-AA1",
                aircraftEntity,
                "FN1",
                originDate,
                "DA1",
                "AA1")
                .build();
        Mockito.when(flightEntityManager.findByUfi(Mockito.anyString())).thenReturn(entity);
        // when
        Flight flight = flightRepository.getFlight("ufi");
        // then
        assertNotNull(flight);
        assertEquals("FN1 2019-07-08 DA1-AA1", flight.getUfi());
        assertNotNull(flight.getAircraft());
        assertEquals("RM1", flight.getAircraft().getRegistrationMark());
        assertEquals("FN1", flight.getFlightNumber());
        assertEquals(originDate, flight.getOriginDate());
        assertEquals("DA1", flight.getDepartureAirport());
        assertEquals("AA1", flight.getArrivalAirport());
    }

    @Test
    void persistFlight() {

        /*
         * NULL flight argument
         */
        assertThrows(IllegalArgumentException.class, () -> flightRepository.persistFlight(null));

        // given
        LocalDate originDate = LocalDate.of(2019, 7, 8);

        /*
         * Correct flight
         */
        // given
        Flight flight = new Flight.Builder("UFI",
                new Aircraft.Builder("RM").build(),
                "FN",
                originDate,
                "DA",
                "AA")
                .withStatus(FlightStatus.CREATED)
                .build();
        // when
        flightRepository.persistFlight(flight);
        // then
        Mockito.verify(flightEntityManager, Mockito.times(1))
                .saveAndFlush(Mockito.any(FlightEntity.class));
    }

    @Test
    void deleteFlight() {

        /*
         * NULL UFI.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> flightRepository.deleteFlight(null));

        /*
         * Empty UFI.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> flightRepository.deleteFlight(""));

        /*
         * Blank UFI.
         */
        assertThrows(IllegalArgumentException.class, () -> flightRepository.deleteFlight(" "));

        /*
         * Correct UFI.
         */
        // when
        flightRepository.deleteFlight("UFI");
        // then
        Mockito.verify(flightEntityManager, Mockito.times(1))
                .deleteByUfi(Mockito.anyString());
    }

    @Test
    void exists() {

        /*
         * NULL UFI.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> flightRepository.exists(null));

        /*
         * Empty UFI.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> flightRepository.exists(""));

        /*
         * Blank UFI.
         */
        assertThrows(IllegalArgumentException.class, () -> flightRepository.exists(" "));

        /*
         * Correct UFI.
         */
        // when
        flightRepository.exists("UFI");
        // then
        Mockito.verify(flightEntityManager, Mockito.times(1))
                .existsByUfi(Mockito.anyString());
    }
}