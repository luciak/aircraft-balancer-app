package org.luciak.acb.repositories.flight;

import org.junit.jupiter.api.Test;
import org.luciak.acb.domain.aircraft.Aircraft;
import org.luciak.acb.domain.flight.Flight;
import org.luciak.acb.domain.flight.FlightInfo;
import org.luciak.acb.domain.flight.FlightStatus;
import org.luciak.acb.repositories.aircraft.AircraftEntity;
import org.luciak.acb.repositories.aircraft.AircraftEntityMapper;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FlightEntityMapperTest {

    @Test
    void mapToFlightInfo() {

        // given
        LocalDate originDate = LocalDate.of(2019, 3, 4);
        Aircraft aircraft = new Aircraft.Builder("RM1").build();
        AircraftEntity aircraftEntity = AircraftEntityMapper.mapToAircraftEntity(aircraft);

        /*
         * NULL arguments
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> FlightEntityMapper.mapToFlightInfo(null));

        /*
         * Mandatory values
         */
        // given
        FlightEntity entity = new FlightEntity.Builder(
                "FN1 2019-03-04 DA1-AA1",
                aircraftEntity,
                "FN1",
                originDate,
                "DA1",
                "AA1")
                .build();
        // when
        FlightInfo flight = FlightEntityMapper.mapToFlightInfo(entity);
        // then
        assertNotNull(flight);
        assertEquals("FN1 2019-03-04 DA1-AA1", flight.getUfi());
        assertEquals("RM1", flight.getAircraftRegistrationMark());
        assertEquals("FN1", flight.getFlightNumber());
        assertEquals(originDate, flight.getOriginDate());
        assertEquals("DA1", flight.getDepartureAirport());
        assertEquals("AA1", flight.getArrivalAirport());
        assertNull(flight.getStatus());
        assertNull(flight.getRepeatNumber());

        /*
         * All values
         */
        // given
        entity = new FlightEntity.Builder(
                "FN2 2019-03-04 DA2-AA2 5",
                aircraftEntity,
                "FN2",
                originDate,
                "DA2",
                "AA2")
                .withStatus(FlightStatus.PREPARED)
                .withRepeatNumber(5)
                .build();
        // when
        flight = FlightEntityMapper.mapToFlightInfo(entity);
        // then
        assertNotNull(flight);
        assertEquals("FN2 2019-03-04 DA2-AA2 5", flight.getUfi());
        assertEquals(FlightStatus.PREPARED, flight.getStatus());
        assertEquals(5, flight.getRepeatNumber().intValue());
    }

    @Test
    void mapToFlight() {

        /*
         * NULL arguments
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> FlightEntityMapper.mapToFlight(null));

        /*
         * Mandatory values
         */
        // given
        LocalDate originDate = LocalDate.of(2018, 7, 20);
        AircraftEntity aircraftEntity = AircraftEntityMapper.mapToAircraftEntity(
                new Aircraft.Builder("RM").build());
        FlightEntity flightEntity = new FlightEntity.Builder(
                "FN 2018-07-20 DA-AA",
                aircraftEntity,
                "FN",
                originDate,
                "DA",
                "AA")
                .build();
        // when
        Flight flight = FlightEntityMapper.mapToFlight(flightEntity);
        // then
        assertNotNull(flight);
        assertNotNull(flight.getAircraft());
        assertEquals("FN 2018-07-20 DA-AA", flight.getUfi());
        assertEquals("RM", flight.getAircraft().getRegistrationMark());
        assertEquals("FN", flight.getFlightNumber());
        assertEquals("DA", flight.getDepartureAirport());
        assertEquals("AA", flight.getArrivalAirport());
        assertNull(flight.getStatus());
        assertNull(flight.getRepeatNumber());

        /*
         * All values
         */
        // given
        flightEntity = new FlightEntity.Builder(
                "FN 2018-07-20 DA-AA",
                aircraftEntity,
                "FN",
                originDate,
                "DA",
                "AA")
                .withStatus(FlightStatus.PREPARED)
                .withRepeatNumber(1)
                .build();
        // when
        flight = FlightEntityMapper.mapToFlight(flightEntity);
        // then
        assertNotNull(flight);
        assertEquals(FlightStatus.PREPARED, flight.getStatus());
        assertEquals(1, flight.getRepeatNumber().intValue());
    }

    @Test
    void mapToFlightEntity() {

        /*
         * NULL arguments.
         */
        // then
        assertThrows(IllegalArgumentException.class, () -> FlightEntityMapper.mapToFlightEntity(null));

        // given
        LocalDate originDate = LocalDate.of(2018, 1, 30);

        /*
         * Mandatory values.
         */
        // given
        Aircraft aircraft = new Aircraft.Builder("RM").build();
        Flight flight = new Flight.Builder("UFI", aircraft, "FN", originDate,
                "DA", "AA").build();
        // when
        FlightEntity flightEntity = FlightEntityMapper.mapToFlightEntity(flight);
        // then
        assertNotNull(flightEntity);
        assertEquals("UFI", flightEntity.getUfi());
        assertNotNull(aircraft);
        assertEquals("RM", flightEntity.getAircraft().getRegistrationMark());
        assertEquals("RM", flightEntity.getAircraftRegistrationMark());
        assertEquals("FN", flightEntity.getFlightNumber());
        assertEquals(originDate, flightEntity.getOriginDate());
        assertEquals("DA", flightEntity.getDepartureAirport());
        assertEquals("AA", flightEntity.getArrivalAirport());
        assertNull(flightEntity.getStatus());
        assertNull(flightEntity.getRepeatNumber());

        /*
         * All values.
         */
        flight = new Flight.Builder("UFI", aircraft, "FN", originDate,
                "DA", "AA")
                .withStatus(FlightStatus.PREPARED)
                .withRepeatNumber(1)
                .build();
        // when
        flightEntity = FlightEntityMapper.mapToFlightEntity(flight);
        // then
        assertNotNull(flightEntity);
        assertEquals(FlightStatus.PREPARED, flightEntity.getStatus());
        assertEquals(1, flightEntity.getRepeatNumber().intValue());
    }
}