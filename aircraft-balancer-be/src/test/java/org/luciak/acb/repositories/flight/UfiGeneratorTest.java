package org.luciak.acb.repositories.flight;


import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class UfiGeneratorTest {

    @Test
    void generateUfi() {

        // given
        final LocalDate originDate = LocalDate.of(2019, 12, 31);

        /*
         * NULL flight number.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                UfiGenerator.generateUfi(null, originDate, "DA", "AA"));

        /*
         * Empty flight number.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                UfiGenerator.generateUfi("", originDate, "DA", "AA"));

        /*
         * Blank flight number.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                UfiGenerator.generateUfi(" ", originDate, "DA", "AA"));

        /*
         * NULL origin date.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                UfiGenerator.generateUfi("FN", null, "DA", "AA"));

        /*
         * NULL departure airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                UfiGenerator.generateUfi("FN", originDate, null, "AA"));

        /*
         * Empty departure airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                UfiGenerator.generateUfi("FN", originDate, "", "AA"));

        /*
         * Blank departure airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                UfiGenerator.generateUfi("FN", originDate, " ", "AA"));

        /*
         * NULL arrival airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                UfiGenerator.generateUfi("FN", originDate, "DA", null));

        /*
         * Empty arrival airport
         */
        assertThrows(IllegalArgumentException.class, () ->
                UfiGenerator.generateUfi("FN", originDate, "DA", ""));

        /*
         * Blank arrival airport
         */
        assertThrows(IllegalArgumentException.class, () ->
                UfiGenerator.generateUfi("FN", originDate, "DA", " "));

        /*
         * Mandatory fields.
         */
        // when
        String ufi = UfiGenerator.generateUfi("FN", originDate, "DA", "AA");
        // then
        assertEquals(ufi, "FN 2019-12-31 DA-AA" );

        /*
         * All fields.
         */
        // when
        ufi = UfiGenerator.generateUfi("FN", originDate,
                "DA", "AA", 1);
        // then
        assertEquals(ufi, "FN 2019-12-31 DA-AA 1" );
    }
}