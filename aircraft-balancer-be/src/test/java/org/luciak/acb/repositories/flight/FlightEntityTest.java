package org.luciak.acb.repositories.flight;

import org.junit.jupiter.api.Test;
import org.luciak.acb.domain.aircraft.Aircraft;
import org.luciak.acb.domain.flight.FlightStatus;
import org.luciak.acb.repositories.aircraft.AircraftEntity;
import org.luciak.acb.repositories.aircraft.AircraftEntityMapper;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FlightEntityTest {

    @Test
    void builder() {

        // given
        LocalDate originDate = LocalDate.of(2019, 12, 31);
        Aircraft aircraft = new Aircraft.Builder("ARM").build();
        AircraftEntity aircraftEntity = AircraftEntityMapper.mapToAircraftEntity(aircraft);

        /*
         * NULL UFI.
         */
        assertThrows(IllegalArgumentException.class, () ->
                new FlightEntity.Builder(null, aircraftEntity, "FN",
                        originDate, "DA", "AA")
                        .build());

        /*
         * Empty UFI.
         */
        assertThrows(IllegalArgumentException.class, () ->
                new FlightEntity.Builder("", aircraftEntity, "FN",
                        originDate, "DA", "AA")
                        .build());

        /*
         * Blan UFI.
         */
        assertThrows(IllegalArgumentException.class, () ->
                new FlightEntity.Builder(" ", aircraftEntity, "FN",
                        originDate, "DA", "AA")
                        .build());

        /*
         * NULL aircraft.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightEntity.Builder("UFI", null, "FN",
                        originDate, "DA", "AA")
                        .build());

        /*
         * NULL flight number.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightEntity.Builder("UFI", aircraftEntity, null,
                        originDate, "DA", "AA")
                        .build());

        /*
         * NULL origin date.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightEntity.Builder("UFI", aircraftEntity, "FN",
                        null, "DA", "AA")
                        .build());

        /*
         * NULL departure airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightEntity.Builder("UFI", aircraftEntity, "FN",
                        originDate, null, "AA")
                        .build());

        /*
         * NULL arrival airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                new FlightEntity.Builder("UFI", aircraftEntity, "FN",
                        originDate, "DA", null)
                        .build());

        /*
         * Mandatory values.
         */
        // when
        FlightEntity entity = new FlightEntity.Builder("UFI",
                aircraftEntity, "FN",
                originDate, "DA", "AA")
                .build();
        // then
        assertNotNull(entity);
        assertEquals("UFI", entity.getUfi());
        assertNotNull(aircraft);
        assertEquals("ARM", entity.getAircraft().getRegistrationMark());
        assertEquals("ARM", entity.getAircraftRegistrationMark());
        assertEquals("FN", entity.getFlightNumber());
        assertEquals(originDate, entity.getOriginDate());
        assertEquals("DA", entity.getDepartureAirport());
        assertEquals("AA", entity.getArrivalAirport());
        assertNull(entity.getStatus());
        assertNull(entity.getRepeatNumber());

        /*
         * All values.
         */
        // when
        entity = new FlightEntity.Builder("UFI", aircraftEntity, "FN",
                originDate, "DA", "AA")
                .withRepeatNumber(1)
                .withStatus(FlightStatus.PROCESSING)
                .build();
        // then
        assertNotNull(entity);
        assertEquals(FlightStatus.PROCESSING, entity.getStatus());
        assertEquals(1, entity.getRepeatNumber().intValue());
    }
}