package org.luciak.acb.repositories.flight;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.luciak.acb.domain.aircraft.Aircraft;
import org.luciak.acb.domain.flight.Flight;
import org.luciak.acb.domain.flight.FlightStatus;
import org.luciak.acb.exceptions.AircraftNotFoundException;
import org.luciak.acb.repositories.aircraft.AircraftRepository;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class FlightFactoryTest {

    private FlightFactory flightFactory;

    @Mock
    private AircraftRepository aircraftRepository;

    @BeforeEach
    void init() {

        flightFactory = new FlightFactory(aircraftRepository);
    }

    @Test
    void createFlight() throws AircraftNotFoundException {

        // given
        LocalDate originDate = LocalDate.of(2018, 12, 31);

        /*
         * NULL aircraft registration number.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                flightFactory.createFlight(null,
                        "FN",
                        originDate,
                        "DA",
                        "AA",
                        1));

        /*
         * Empty aircraft registration number.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                flightFactory.createFlight("",
                        "FN",
                        originDate,
                        "DA",
                        "AA",
                        1));

        /*
         * Blank aircraft registration number.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                flightFactory.createFlight(" ",
                        "FN",
                        originDate,
                        "DA",
                        "AA",
                        1));

        /*
         * NULL flight number.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                flightFactory.createFlight("RM",
                        null,
                        originDate,
                        "DA",
                        "AA",
                        1));

        /*
         * Empty flight number.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                flightFactory.createFlight("RM",
                        "",
                        originDate,
                        "DA",
                        "AA",
                        1));

        /*
         * Blank flight number.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                flightFactory.createFlight("RM",
                        " ",
                        originDate,
                        "DA",
                        "AA",
                        1));

        /*
         * NULL origin date.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                flightFactory.createFlight("RM",
                        "FN",
                        null,
                        "DA",
                        "AA",
                        1));

        /*
         * NULL departure airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                flightFactory.createFlight("RM",
                        "FN",
                        originDate,
                        null,
                        "AA",
                        1));

        /*
         * Empty departure airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                flightFactory.createFlight("RM",
                        "FN",
                        originDate,
                        "",
                        "AA",
                        1));

        /*
         * Blank departure airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                flightFactory.createFlight("RM",
                        "FN",
                        originDate,
                        " ",
                        "AA",
                        1));

        /*
         * NULL arrival airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                flightFactory.createFlight("RM",
                        "FN",
                        originDate,
                        "DA",
                        null,
                        1));

        /*
         * Empty arrival airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                flightFactory.createFlight("RM",
                        "FN",
                        originDate,
                        "DA",
                        "",
                        1));

        /*
         * Blank arrival airport.
         */
        // then
        assertThrows(IllegalArgumentException.class, () ->
                flightFactory.createFlight("RM",
                        "FN",
                        originDate,
                        "DA",
                        " ",
                        1));

        // given
        Aircraft aircraft = new Aircraft.Builder("RM").build();
        Mockito.when(aircraftRepository.getAircraftConfiguration(Mockito.eq("NonExistRM")))
                .thenThrow(new AircraftNotFoundException("Aircraft not found!"));
        Mockito.when(aircraftRepository.getAircraftConfiguration(Mockito.eq("RM")))
                .thenReturn(aircraft);

        /*
         * Aircraft configuration doesn't exists.
         */
        // then
        assertThrows(AircraftNotFoundException.class, () ->
                flightFactory.createFlight("NonExistRM",
                        "FN",
                        originDate,
                        "DA",
                        "AA",
                        1));

        /*
         * Mandatory fields.
         */
        // when
        Flight flight = flightFactory.createFlight("RM",
                "FN",
                originDate,
                "DA",
                "AA",
                null);
        // when
        assertNotNull(flight);
        assertEquals("FN 2018-12-31 DA-AA", flight.getUfi());
        assertNotNull(flight.getAircraft());
        assertEquals("RM", flight.getAircraft().getRegistrationMark());
        assertEquals("FN", flight.getFlightNumber());
        assertEquals(originDate, flight.getOriginDate());
        assertEquals("DA", flight.getDepartureAirport());
        assertEquals("AA", flight.getArrivalAirport());
        assertEquals(FlightStatus.CREATED, flight.getStatus());
        assertNull(flight.getRepeatNumber());

        /*
         * All fields.
         */
        // when
        flight = flightFactory.createFlight("RM",
                "FN",
                originDate,
                "DA",
                "AA",
                1);
        assertNotNull(flight);
        assertEquals("FN 2018-12-31 DA-AA 1", flight.getUfi());
        assertEquals(1, flight.getRepeatNumber().intValue());
    }
}