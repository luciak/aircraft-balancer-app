package org.luciak.acb.repositories.aircraft;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.luciak.acb.domain.aircraft.Aircraft;
import org.luciak.acb.exceptions.AircraftNotFoundException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
class AircraftRepositoryTest {

    private AircraftRepository aircraftRepository;
    @Mock
    private AircraftEntityManager aircraftEntityManager;

    @BeforeEach
    void init() {

        this.aircraftRepository = new AircraftRepository(aircraftEntityManager);
    }

    @Test
    void findAircraftConfigurations() {

        /*
         * NULL result from DB.
         */
        // given
        Mockito.when(aircraftEntityManager.findAllByOrderByRegistrationMarkAsc()).thenReturn(null);
        // when
        List<Aircraft> result = aircraftRepository.findAircraftConfigurations();
        // then
        assertNotNull(result);
        assertTrue(result.isEmpty());

        /*
         * Empty result from DB.
         */
        // given
        Mockito.when(aircraftEntityManager.findAllByOrderByRegistrationMarkAsc()).thenReturn(new ArrayList<>());
        // when
        result = aircraftRepository.findAircraftConfigurations();
        // then
        assertNotNull(result);
        assertTrue(result.isEmpty());

        /*
         * Multiple results from DB.
         */
        // given
        List<AircraftEntity> aircraftEntities = new ArrayList<>();
        aircraftEntities.add(new AircraftEntity.Builder("regMark1").build());
        aircraftEntities.add(new AircraftEntity.Builder("regMark2").build());
        Mockito.when(aircraftEntityManager.findAllByOrderByRegistrationMarkAsc()).thenReturn(aircraftEntities);

        // when
        result = aircraftRepository.findAircraftConfigurations();

        // then
        assertNotNull(result);
        assertEquals(result.size(), 2);
        assertEquals("regMark1", result.get(0).getRegistrationMark());
        assertEquals("regMark2", result.get(1).getRegistrationMark());
    }

    @Test
    void getAircraftConfiguration() throws AircraftNotFoundException {

        /*
         * NULL result from DB.
         */
        // given
        Mockito.when(aircraftEntityManager.findByRegistrationMark(Mockito.anyString())).thenReturn(null);
        // then
        assertThrows(AircraftNotFoundException.class, () ->
                aircraftRepository.getAircraftConfiguration("regMark"));

        /*
         * Correct result from DB.
         */
        // given
        Mockito.when(aircraftEntityManager.findByRegistrationMark(Mockito.anyString()))
                .thenReturn(new AircraftEntity.Builder("regMark").build());
        // when
        Aircraft aircraft = aircraftRepository.getAircraftConfiguration("regMark");
        // then
        assertNotNull(aircraft);
        assertEquals("regMark", aircraft.getRegistrationMark());
    }
}