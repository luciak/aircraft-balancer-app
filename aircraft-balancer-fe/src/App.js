import React, {PureComponent} from 'react';
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';
import styled from 'styled-components';
import SideNav, {NavItem, NavIcon, NavText, Toggle, Nav} from '@trendmicro/react-sidenav';
import {library} from '@fortawesome/fontawesome-svg-core'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {
    faChair,
    faCheck,
    faEdit,
    faPlaneDeparture,
    faPlus,
    faQuestionCircle,
    faSave,
    faTrash
} from '@fortawesome/free-solid-svg-icons'

import About from './components/About';
import Flights from './components/Flights';
import FlightDetail from './components/FlightDetail';

import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import './App.css';

library.add(faPlaneDeparture, faQuestionCircle, faPlus, faEdit, faTrash, faSave, faCheck, faChair);

const Main = styled.main`
    position: relative;
    overflow: hidden;
    transition: all .15s;
    padding: 0 0px;
    margin-left: ${props => (props.expanded ? 240 : 64)}px;
`;

const NavHeader = styled.div`
    display: ${props => (props.expanded ? 'block' : 'none')};
    white-space: nowrap;
    background-color: #db3d44;
    color: #fff;
    > * {
        color: inherit;
        background-color: inherit;
    }
`;

const NavTitle = styled.div`
    font-size: 1.3em;
    line-height: 20px;
    padding: 22px 0;
`;

const Separator = styled.div`
    clear: both;
    position: relative;
    margin: .8rem 0;
    background-color: #ddd;
    height: 1px;
`;

class App extends PureComponent {

    state = {
        selected: 'flights',
        expanded: false
    };

    updateNavSelection = (selected) => {
        this.setState({selected: selected})
    }

    render() {
        const {expanded, selected} = this.state;

        return (
            <Router>
                <Route render={({location, history}) => (
                    <React.Fragment>
                        <SideNav
                            className="ACBSideNav"
                            onSelect={(selected) => {
                                const to = '/' + selected;
                                if (location.pathname !== to) {
                                    history.push(to);
                                }
                                this.setState({selected: selected});
                            }}
                            onToggle={(expanded) => {
                                this.setState({expanded: expanded});
                            }}>
                            <Toggle className="ACBSideNav"/>
                            <NavHeader className="ACBSideNav" expanded={expanded}>
                                <NavTitle className="ACBSideNav">Aircraft Balancer</NavTitle>
                            </NavHeader>
                            <Nav selected={selected}>
                                <NavItem eventKey="flights">
                                    <NavIcon>
                                        <FontAwesomeIcon icon="plane-departure"
                                                         style={{fontSize: '1.75em', verticalAlign: 'middle'}}/>
                                    </NavIcon>
                                    <NavText style={{paddingRight: 32}}>
                                        Flighs
                                    </NavText>
                                </NavItem>
                                <Separator/>
                                <NavItem eventKey="about">
                                    <NavIcon>
                                        <FontAwesomeIcon icon="question-circle"
                                                         style={{fontSize: '1.75em', verticalAlign: 'middle'}}/>
                                    </NavIcon>
                                    <NavText style={{paddingRight: 32}}>
                                        About
                                    </NavText>
                                </NavItem>
                            </Nav>
                        </SideNav>

                        <Main expanded={expanded}>
                            <Switch>
                                <Route path="/" exact render={() => (<Redirect to="/flights"/>)}/>
                                <Route path="/flights" exact
                                       render={() => {
                                           return (
                                               <React.Fragment>
                                                   <NavSelector navSelection={'flights'}
                                                                navSelectionHandler={this.updateNavSelection}/>
                                                   <Flights/>
                                               </React.Fragment>
                                           )
                                       }}/>
                                <Route path="/flights/create"
                                       render={({match}) => {
                                           return (
                                               <React.Fragment>
                                                   <NavSelector navSelection={'flights'}
                                                                navSelectionHandler={this.updateNavSelection}/>
                                                   <FlightDetail ufi={null}/>
                                               </React.Fragment>
                                           )
                                       }}/>
                                <Route path="/flights/:ufi"
                                       render={({match}) => {
                                           return (
                                               <React.Fragment>
                                                   <NavSelector navSelection={'flights'}
                                                                navSelectionHandler={this.updateNavSelection}/>
                                                   <FlightDetail ufi={match.params.ufi}/>
                                               </React.Fragment>
                                           )
                                       }}/>
                                <Route path="/about"
                                       render={() => {
                                           return (
                                               <React.Fragment>
                                                   <NavSelector navSelection={'about'}
                                                                navSelectionHandler={this.updateNavSelection}/>
                                                   <About/>
                                               </React.Fragment>
                                           )
                                       }}/>
                            </Switch>
                        </Main>
                    </React.Fragment>
                )}
                />
            </Router>
        );
    }
}

class NavSelector extends PureComponent {

    componentDidMount() {
        this.props.navSelectionHandler(this.props.navSelection);
    }

    render() {
        return null;
    }
}

export default App;
