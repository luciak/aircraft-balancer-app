export const getErrorMsgFromResponse = (error) => {

    var msg = '';
    if (error && error.response) {
        msg = error.response.data.message
    }
    return msg;
}