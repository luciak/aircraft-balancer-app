import React, {PureComponent} from 'react';
import {Button, Col, Form} from "react-bootstrap";
import Select from 'react-select';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import axios from "axios";
import 'react-select/dist/react-select.min.css'

import {getErrorMsgFromResponse} from '../utils/api-utils'
import {withRouter} from "react-router-dom";

class FlightMetadata extends PureComponent {

    state = {
        availableAircraft: []
    }

    componentDidMount() {
        axios.get("/api/aircraft")
            .then(result => (
                this.setState({
                    availableAircraft: result.data
                })
            ))
            .catch(error => (
                window.alert("Error while finding available aircraft:\n\n" + getErrorMsgFromResponse(error))
            ))
    }

    handleSubmit = () => {
        const {flight, afterSave} = this.props;
        if (flight.ufi) {
            // update
            axios.put(`/api/flight/${flight.ufi}`, flight)
                .then(result => (
                    afterSave(result.data)
                ))
                .catch(error => (
                    window.alert("Error while updating flight:\n\n" + getErrorMsgFromResponse(error))
                ))
        } else {
            // create
            axios.post("/api/flight", flight)
                .then(result => {
                    this.props.history.push(`/flights/${result.data.ufi}`)
                    afterSave(result.data)
                })
                .catch(error => (
                    window.alert("Error while creating flight:\n\n" + getErrorMsgFromResponse(error))
                ))
        }
    }

    getControlDisabled = (flight) => {
        if (!flight || !flight.status || flight.status === 'CREATED') {
            return false
        }
        return true
    }

    render() {
        const {flight, onChange} = this.props

        return (
            <Form style={{height: "500px"}}>
                <Form.Row>
                    <Form.Group as={Col} controlId="formAircraftRegistrationMark">
                        <Form.Label>Aircraft Registration Mark</Form.Label>
                        <Select
                            disabled={this.getControlDisabled(flight)}
                            name="aircraft"
                            valueKey="registrationMark"
                            labelKey="registrationMark"
                            value={flight.aircraft && flight.aircraft.registrationMark}
                            options={this.state.availableAircraft}
                            optionRenderer={(option) => (
                                <div>
                                    <div>
                                        {option.registrationMark}
                                    </div>
                                    <div key="modelNumber">
                                        <small>
                                            {option.modelNumber} / {option.version}
                                        </small>
                                    </div>
                                </div>
                            )}
                            onChange={(target) => {
                                onChange('aircraft', target && {...target})
                            }}/>
                    </Form.Group>
                    <Form.Group as={Col} controlId="formAircraftModelNumber">
                        <Form.Label>Model Number</Form.Label>
                        <Form.Control
                            readOnly
                            type="text"
                            name="aircraftModelNumber"
                            value={(flight.aircraft && flight.aircraft.modelNumber) || ''}
                            placeholder="Model Number"/>
                    </Form.Group>
                    <Form.Group as={Col} controlId="formAircraftVersion">
                        <Form.Label>Version</Form.Label>
                        <Form.Control
                            readOnly
                            type="text"
                            name="aircraftVersion"
                            value={(flight.aircraft && flight.aircraft.version) || ''}
                            placeholder="Version"/>
                    </Form.Group>
                </Form.Row>

                <Form.Row>
                    <Form.Group as={Col} controlId="formFlightNumber">
                        <Form.Label>Flight Number</Form.Label>
                        <Form.Control
                            readOnly={this.getControlDisabled(flight)}
                            type="text"
                            maxLength="10"
                            name="flightNumber"
                            value={flight.flightNumber || ''}
                            onChange={(e) => (onChange("flightNumber", e.target.value))}
                            placeholder="Flight Number"/>
                    </Form.Group>
                </Form.Row>

                <Form.Row>
                    <Form.Group as={Col} controlId="formDepartureAirport">
                        <Form.Label>Departure Airport</Form.Label>
                        <Form.Control readOnly={this.getControlDisabled(flight)}
                                      type="text"
                                      maxLength="4"
                                      name="departureAirport"
                                      value={flight.departureAirport || ''}
                                      onChange={(e) => (onChange("departureAirport", e.target.value))}
                                      placeholder="Departure Airport"/>
                    </Form.Group>
                    <Form.Group as={Col} controlId="formArrivalAirport">
                        <Form.Label>Arrival Airport</Form.Label>
                        <Form.Control readOnly={this.getControlDisabled(flight)}
                                      type="text"
                                      maxLength="4"
                                      name="arrivalAirport"
                                      value={flight.arrivalAirport || ''}
                                      onChange={(e) => (onChange("arrivalAirport", e.target.value))}
                                      placeholder="Arrival Airport"/>
                    </Form.Group>
                    <Form.Group as={Col} controlId="formOriginDate">
                        <Form.Label>Origin Date [YYYY-MM-DD]</Form.Label>
                        <Form.Control readOnly={this.getControlDisabled(flight)}
                                      type="text"
                                      maxLength="10"
                                      name="originDate"
                                      value={flight.originDate || ''}
                                      onChange={(e) => (onChange("originDate", e.target.value))}
                                      placeholder="UTC Departure Date (YYYY-MM-DD)"/>
                    </Form.Group>
                    <Form.Group as={Col} controlId="formRepeatCode">
                        <Form.Label>Repeat Code</Form.Label>
                        <Form.Control readOnly={this.getControlDisabled(flight)}
                                      type="number"
                                      name="repeatCode"
                                      value={flight.repeatCode || ''}
                                      onChange={(e) => (onChange("repeatCode", e.target.value))}
                                      placeholder="Repeat Code"/>
                    </Form.Group>
                </Form.Row>
                <Button disabled={this.getControlDisabled(flight)}
                        variant="primary" onClick={this.handleSubmit}>
                    <FontAwesomeIcon style={{marginRight: "8px"}} icon="save"/>Save Flight
                </Button>
            </Form>
        )
    }
}

export default withRouter(FlightMetadata);