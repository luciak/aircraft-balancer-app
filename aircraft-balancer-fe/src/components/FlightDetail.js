import React, {PureComponent} from 'react';
import Breadcrumb from "react-bootstrap/Breadcrumb";
import {Link} from 'react-router-dom';
import axios from 'axios';
import set from 'lodash/fp/set';

import {Tabs, Tab} from 'react-bootstrap';

import FlightMetadata from './FlightMetadata';
import PassengerList from "./PassengerList";
import Seating from "./Seating";

import {getErrorMsgFromResponse} from "../utils/api-utils"

class FlightDetail extends PureComponent {

    state = {
        flight: {},
        tabKey: 'metadata'
    }

    componentDidMount() {
        if (this.props.ufi) {
            axios.get(`/api/flight/${this.props.ufi}`)
                .then(result => {
                    this.setState({flight: result.data});
                    this.calculateActiveKeyFromFlightStatus();
                })
                .catch(error => (
                    window.alert("Error while finding current flight:\n\n" + getErrorMsgFromResponse(error))
                ))

        }
    }

    calculateActiveKeyFromFlightStatus = () => {
        switch (this.state.flight.status) {
            case 'CREATED':
            case 'PREPARED':
                this.setState({tabKey: 'passengers'})
                break
            case 'PROCESSING':
            case 'BALANCED':
            case 'CLOSED':
                this.setState({tabKey: 'seating'})
                break
            default:
                this.setState({tabKey: 'metadata'})
        }
    }

    calculateTabEnabled = (tabId) => {
        switch (tabId) {
            case 'paxlist':
                if (this.state.flight && this.state.flight.ufi) {
                    switch (this.state.flight.status) {
                        case 'CREATED':
                        case 'PREPARED':
                        case 'PROCESSING':
                        case 'BALANCED':
                        case 'CLOSED':
                            return true
                        default:
                            return false
                    }
                }
                break;
            case 'seating':
                if (this.state.flight && this.state.flight.ufi) {
                    switch (this.state.flight.status) {
                        case 'PROCESSING':
                        case 'BALANCED':
                        case 'CLOSED':
                            return true
                        default:
                            return false
                    }
                }
                break
            default:
                return true;
        }
    }

    handleChange = (key, value) => (
        this.setState((state) => ({...state, flight: set(key, value, state.flight)}))
    )

    reloadFlight = (data) => {
        this.setState({flight: data})
        this.calculateActiveKeyFromFlightStatus()
    }

    render() {
        const ufi = this.state.flight && this.state.flight.ufi
        return (
            <div>
                <Breadcrumb>
                    <Breadcrumb.Item active><Link to={"/flights"}>Flights</Link></Breadcrumb.Item>
                    <Breadcrumb.Item active>{ufi ? ufi : "New Flight"}</Breadcrumb.Item>
                </Breadcrumb>

                <div className="content">
                    <Tabs id="edit-flight-tab"
                          activeKey={this.state.tabKey}
                          onSelect={key => this.setState({tabKey: key})}>
                        <Tab eventKey="metadata" title="Flight metadata">
                            <FlightMetadata flight={this.state.flight}
                                            onChange={this.handleChange}
                                            afterSave={this.reloadFlight}/>
                        </Tab>
                        <Tab eventKey="passengers" title="Passenger list"
                             disabled={!this.calculateTabEnabled("paxlist")}>
                            {this.calculateTabEnabled("paxlist") &&
                            <PassengerList flight={this.state.flight}
                                           onChange={this.handleChange}
                                           afterSave={this.reloadFlight}/>
                            }
                        </Tab>
                        <Tab eventKey="seating" title="Seating" disabled={!this.calculateTabEnabled("seating")}>
                            {this.calculateTabEnabled("seating") &&
                            <Seating flight={this.state.flight}
                                     afterSave={this.reloadFlight}/>
                            }
                        </Tab>
                    </Tabs>
                </div>
            </div>
        );
    }
}

export default FlightDetail;