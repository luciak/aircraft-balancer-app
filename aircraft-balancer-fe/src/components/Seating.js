import React, {PureComponent} from 'react';
import {Button, Col, Form} from "react-bootstrap";
import Select from "react-select";
import axios from "axios";
import {SvgLoader, SvgProxy} from 'react-svgmt';
import {getErrorMsgFromResponse} from "../utils/api-utils";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import set from "lodash/fp/set";

class Seating extends PureComponent {

    state = {
        paxlist: [],
        pax: null,
        seatMapSvg: null,
        seatMap: null,
        sectionWeightMap: null,
        compWeightMap: null,
        assignedSeats: 0,
        paxLeft: 0,
        selectedPaxSeat: null
    }

    getFlightSetup = () => {

        axios.get(`/api/flight/${this.props.flight.ufi}/setup`)
            .then(result => {

                const seatmap = new Map()

                const sectionmap = this.state.sectionWeightMap ? this.state.sectionWeightMap : new Map()
                // clear previous section weight map because it is mapped to SVG component
                sectionmap && sectionmap.forEach((value, key) => (
                    sectionmap.set(key, 0)
                ))

                const compmap = this.state.compWeightMap ? this.state.compWeightMap : new Map()
                // clear previous compartment weight map because it is mapped to SVG component
                compmap && compmap.forEach((value, key) => (
                    compmap.set(key, 0)
                ))

                // seatMap
                if (result.data && result.data.seatMap) {
                    Object.keys(result.data.seatMap).forEach(k => seatmap.set(k, result.data.seatMap[k]))
                }
                // sectionMap
                if (result.data && result.data.sectionWeightMap) {
                    Object.keys(result.data.sectionWeightMap).forEach(k => sectionmap.set(k, result.data.sectionWeightMap[k]))
                }
                // compMap
                if (result.data && result.data.compWeightMap) {
                    Object.keys(result.data.compWeightMap).forEach(k => compmap.set(k, result.data.compWeightMap[k]))

                }

                // assigned seats
                const assignedseats = result.data.assignedSeats ? result.data.assignedSeats : 0
                // pax left
                const paxleft = result.data.paxLeft ? result.data.paxLeft : 0

                this.setState({
                    seatMap: seatmap,
                    sectionWeightMap: sectionmap,
                    compWeightMap: compmap,
                    assignedSeats: assignedseats,
                    paxLeft: paxleft
                })
            })
            .catch(error => (
                window.alert("Error while loading seat map:\n\n" + getErrorMsgFromResponse(error))
            ))
    }

    handleSelectPax = (target) => {
        this.setState({pax: target, selectedPaxSeat: target && target.seat})
    }

    filterPax = (option, inputValue) => {
        if (inputValue) {
            if ((option.name && option.name.toLowerCase().indexOf(inputValue.toLowerCase()) !== -1)
                || (option.surname && option.surname.toLowerCase().indexOf(inputValue.toLowerCase()) !== -1)
                || (option.idNumber && option.idNumber.toLowerCase().indexOf(inputValue.toLowerCase()) !== -1)) {
                return true
            }
            return false
        }
        return true
    }

    handlePax = (key, value) => {
        this.setState((state) => ({...state, pax: set(key, value, state.pax)}))
    }

    handleSelectSeat = (id) => {

        const {pax} = this.state;

        // because the component ID cannot start with number, seat id has format id-SEATNUM
        // prefix 'id-' must be removed before handle seat
        if (pax && id && id.length > 4) {

            const seat = this.state.seatMap.get(id.substr(3))
            if (seat && seat.number) {

                // there is not possible to assign seat:
                // 1. target seat has assigned for another pax
                //    OR
                // 2. pax has no restriction or seat has no restrictions
                if ((seat.pax && !seat.pax.predicatedSeat && pax.idNumber !== seat.pax.idNumber)
                    || (seat.restriction && (pax.codeId || pax.sex === 'CHD'))) {

                    window.alert("This seat cannot be assigned to this passenger!")
                } else {

                    this.setState({selectedPaxSeat: seat.number})
                }
            }
        }
    }

    handleAssignPax = () => {
        if (this.state.pax) {

            const paxToAssign = this.state.pax

            // valideate input
            if (paxToAssign.baggageWeight > 99) {
                window.alert("Baggage weight cannot be higher then 99kg!")
                return
            }

            // update seat number
            paxToAssign.seat = this.state.selectedPaxSeat;

            // post to server
            axios.put(`/api/flight/${this.props.flight.ufi}/pax/confirm`, paxToAssign)
                .then(result => {
                    const updatedPax = result.data
                    // update pax
                    this.setState({pax: updatedPax})

                    // update paxlist
                    const updatedPaxlist = this.state.paxlist.slice()
                    updatedPaxlist.forEach((item, i) => {
                        if (item.idNumber === updatedPax.idNumber) {
                            updatedPaxlist[i] = updatedPax
                        }
                    })
                    this.setState({paxlist: updatedPaxlist})

                    // update flight setup
                    this.getFlightSetup()

                })
                .catch(error => (
                    window.alert("Error while confirming passenger's seat:\n\n" + getErrorMsgFromResponse(error))
                ))
        }
    }

    handleCloseFlight = () => {

        const {afterSave} = this.props

        if (window.confirm("This action will close the flight. All unassigned passengers will be removed from pax list." +
            " Do you want to proceed?")) {

            axios.put(`/api/flight/${this.props.flight.ufi}/close`)
                .then(result => {
                    // update flight status
                    afterSave(result.data)
                    // update flight setup
                    this.getFlightSetup()
                })
                .catch(error => (
                    window.alert("Error while closing flight:\n\n" + getErrorMsgFromResponse(error))
                ))
        }
    }


    componentDidMount() {

        // get paxlist
        axios.get(`/api/flight/${this.props.flight.ufi}/paxlist`)
            .then(result => (
                this.setState({paxlist: result.data})
            ))
            .catch(error => (
                window.alert("Error while loading passenger list:\n\n" + getErrorMsgFromResponse(error))
            ))

        // get seatmap SVG
        axios.get(`/api/aircraft/${this.props.flight.aircraft.registrationMark}/seatmapsvg`)
            .then(result => (
                this.setState({seatMapSvg: result.data})
            ))
            .catch(error => (
                window.alert("Error while loading seat map svg file:\n\n" + getErrorMsgFromResponse(error))
            ))

        // get flight setup
        this.getFlightSetup()
    }

    calculateSeatClass = (seat, pax) => {

        // restricted seats (display only if pax has some restriction)
        if (pax && (pax.codeId || pax.sex === 'CHD') && seat.restriction) {
            return "restricted-seat"
        }

        // seat which is actually assigned or predicated for the pax
        if (this.state.selectedPaxSeat === seat.number) {
            return "assigned-seat"
        }

        // seat which is actually assigned or predicated for the pax
        if (pax && pax.seat === seat.number) {
            return "prev-assigned-seat"
        }

        // seat which is finally occupied by the pax
        if (seat.pax && !seat.pax.predicatedSeat) {
            return "occupied-seat"
        }
        // seat which was predicted for the pax, but this seat wasn't finally assigned for him
        else if (seat.pax) {
            return "predicated-seat"
        }

        return "free-seat"
    }

    getControlDisabled = (flight) => {
        if (!flight || !flight.status || flight.status === 'PROCESSING') {
            return false
        }
        return true
    }

    render() {
        const {paxlist, pax, seatMapSvg, seatMap, sectionWeightMap, compWeightMap, selectedPaxSeat} = this.state

        const svgProxies = [];

        seatMap && seatMap.forEach(seat => {
            svgProxies.push(<SvgProxy key={seat.number}
                                      selector={`#id-${seat.number}`}
                                      class={this.calculateSeatClass(seat, pax)}
                                      onClick={(e) => (this.handleSelectSeat(e.target.id))}/>)
        })

        sectionWeightMap && sectionWeightMap.forEach((weight, key) => {
            svgProxies.push(<SvgProxy key={key} selector={`#${key}`}>{`${weight} kg`}</SvgProxy>)
        })

        compWeightMap && compWeightMap.forEach((weight, key) => {
            svgProxies.push(<SvgProxy key={key} selector={`#${key}`}>{`${weight} kg`}</SvgProxy>)
        })

        return (
            <div>
                <div className="seating-info-row">
                    <div className="seating-info">
                        <span>Seats assigned:</span>
                        <span style={{
                            fontWeight: "bold",
                            marginLeft: "10px",
                            marginRight: "10px"
                        }}>{this.state.assignedSeats}</span>
                        <span style={{marginRight: "10px"}}>|</span>
                        <span>Pax left:</span>
                        <span style={{
                            fontWeight: "bold",
                            marginLeft: "10px",
                            marginRight: "10px"
                        }}>{this.state.paxLeft}</span>
                    </div>
                    <div className="flight-close">
                        <Button disabled={this.getControlDisabled(this.props.flight)} onClick={this.handleCloseFlight}>
                            <FontAwesomeIcon style={{marginRight: "8px"}} icon="plane-departure"/>Close flight
                        </Button>
                    </div>
                </div>
                <Form style={{height: "600px"}}>
                    <Form.Row>
                        <Form.Group as={Col} controlId="formPax">
                            <Form.Label>Select a passenger</Form.Label>
                            <Select
                                disabled={this.getControlDisabled(this.props.flight)}
                                name="formPax"
                                placeholder="Search passengers by name, surname or ID number..."
                                valueKey="idNumber"
                                filterOption={this.filterPax}
                                value={pax}
                                options={paxlist}
                                onChange={this.handleSelectPax}
                                valueRenderer={(value) => (
                                    <div className={(value && !value.predicatedSeat) ? "assigned-pax" : ""}>
                                        <span
                                            style={{fontWeight: "bold"}}>{value.name} {value.surname}</span> (ID: {value.idNumber})
                                    </div>
                                )}
                                optionRenderer={(option) => (
                                    <div className={!option.predicatedSeat ? "assigned-pax" : ""}>
                                        <div>
                                            <span>Passenger: </span>
                                            <span style={{fontWeight: "bold"}}>{option.name} {option.surname}</span>
                                            <span> (ID: {option.idNumber})</span>
                                        </div>
                                        <div>
                                            <span>Sex: </span>
                                            <span style={{fontWeight: "bold"}}> {option.sex}</span>
                                            <span> | Birthdate:</span>
                                            <span style={{fontWeight: "bold"}}> {option.birthDate}</span>
                                            {option.codeId && <span>
                                                <span> | Code:</span>
                                                <span style={{fontWeight: "bold"}}> {option.codeId}</span>
                                            </span>}
                                        </div>
                                    </div>
                                )}
                            />
                        </Form.Group>
                        <Form.Group as={Col} controlId="formPaxBaggageWeight">
                            <Form.Label>Checked baggage weight [kg]</Form.Label>
                            <Form.Control
                                readOnly={this.getControlDisabled(this.props.flight)}
                                type="number"
                                name="paxBaggage"
                                min="0"
                                max="99"
                                value={(pax && pax.baggageWeight) || ''}
                                onChange={(e) => (this.handlePax("baggageWeight", e.target.value))}/>
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                        <Form.Group as={Col} controlId="formPaxSeat">
                            <Form.Label>Current seat</Form.Label>
                            <Form.Control
                                readOnly
                                type="text"
                                name="paxSeat"
                                value={selectedPaxSeat || ''}/>
                        </Form.Group>
                        <Form.Group as={Col}>
                            <Form.Label>Sex</Form.Label>
                            <Form.Control
                                readOnly
                                type="text"
                                name="paxSex"
                                value={(pax && pax.sex) || ''}/>
                        </Form.Group>
                        <Form.Group as={Col}>
                            <Form.Label>Birth date</Form.Label>
                            <Form.Control
                                readOnly
                                type="text"
                                name="paxBirthDate"
                                value={(pax && pax.birthDate) || ''}/>
                        </Form.Group>
                        <Form.Group as={Col}>
                            <Form.Label>Segment</Form.Label>
                            <Form.Control
                                readOnly
                                type="text"
                                name="paxSegment"
                                value={(pax && pax.segment) || ''}/>
                        </Form.Group>
                        <Form.Group as={Col}>
                            <Form.Label>Code ID</Form.Label>
                            <Form.Control
                                readOnly={this.getControlDisabled(this.props.flight)}
                                type="text"
                                maxLength="10"
                                name="paxCodeId"
                                value={(pax && pax.codeId) || ''}
                                onChange={(e) => (this.handlePax("codeId", e.target.value))}/>
                        </Form.Group>
                        <Form.Group as={Col}>
                            <Form.Label>FFT</Form.Label>
                            <Form.Control
                                readOnly={this.getControlDisabled(this.props.flight)}
                                type="text"
                                maxLength="30"
                                name="paxFFT"
                                value={(pax && pax.fft) || ''}
                                onChange={(e) => (this.handlePax("fft", e.target.value))}/>
                        </Form.Group>
                    </Form.Row>
                    <Button disabled={this.getControlDisabled(this.props.flight)} variant="primary" onClick={this.handleAssignPax}>
                        <FontAwesomeIcon style={{marginRight: "8px"}} icon="chair"/>Assign seat
                    </Button>
                    <div style={{textAlign: "center"}}>
                        <SvgLoader svgXML={seatMapSvg}>
                            {svgProxies}
                        </SvgLoader>
                    </div>
                </Form>
            </div>)
    }
}

export default Seating;