import React, {PureComponent} from 'react';
import {Button, Table} from 'react-bootstrap';

import {FilePond} from 'react-filepond';
import 'filepond/dist/filepond.min.css';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import axios from "axios";
import {getErrorMsgFromResponse} from "../utils/api-utils";

class PassengerList extends PureComponent {

    handleSubmit = () => {

        const {flight, afterSave} = this.props;

        // confirm paxlist
        axios.put(`/api/flight/${flight.ufi}/paxlist/confirm`)
            .then(result => (
                afterSave(result.data)
            ))
            .catch(error => (
                window.alert("Error while confirming paxlist for the flight:\n\n" + getErrorMsgFromResponse(error))
            ))
    }

    render() {
        const {flight, afterSave} = this.props;

        return (
            <div>
                <FilePond
                    disabled={!flight || (flight.status !== 'CREATED' && flight.status !== 'PREPARED')}
                    name="paxlist"
                    server={
                        {
                            url: `/api/flight/${this.props.flight.ufi}/paxlist`,
                            process: {
                                onload: (res) => {
                                    const json = JSON.parse(res)
                                    afterSave(json)
                                    return res
                                }
                            },
                            revert: {
                                onload: (res) => {
                                    const json = JSON.parse(res)
                                    afterSave(json)
                                    return res
                                }
                            }
                        }
                    }
                />
                {flight.paxSummary &&
                    <div>
                        <Table bordered hover>
                            <thead>
                            <tr>
                                <th className={"td-header"}></th>
                                <th>Count</th>
                                <th>Weight / Unit [kg]</th>
                                <th>Weight / Sum [kg]</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td className={"td-header"}>Male:</td>
                                <td>{flight.paxSummary['M']}</td>
                                <td>84</td>
                                <td>{flight.paxSummary['M'] * 84}</td>
                            </tr>
                            <tr>
                                <td className={"td-header"}>Female:</td>
                                <td>{flight.paxSummary['F']}</td>
                                <td>84</td>
                                <td>{flight.paxSummary['F'] * 84}</td>
                            </tr>
                            <tr>
                                <td className={"td-header"}>Child:</td>
                                <td>{flight.paxSummary['CHD']}</td>
                                <td>35</td>
                                <td>{flight.paxSummary['CHD'] * 35}</td>
                            </tr>
                            </tbody>
                        </Table>
                    </div>
                }
                <Button disabled={!flight || flight.status !== 'PREPARED'}
                    variant="primary" onClick={this.handleSubmit}>
                    <FontAwesomeIcon style={{marginRight: "8px"}} icon="check"/>Confirm paxlist
                </Button>
            </div>)
    }
}

export default PassengerList;