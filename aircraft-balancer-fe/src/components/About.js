import React, {PureComponent} from 'react';
import Breadcrumb from "react-bootstrap/Breadcrumb";
import axios from 'axios'
import {Table} from "react-bootstrap";

class About extends PureComponent {

    state = {
        info: {},
        health: {}
    }

    componentDidMount() {

        axios.get("/api/info")
            .then(result => (
                this.setState({info: result.data})
            ))
            .catch(error => (
                window.alert("Error while getting information about application:\n\n"
                    + error)
            ))
        axios.get("/api/health")
            .then(result => (
                this.setState({health: result.data})
            ))
            .catch(error => (
                window.alert("Error while getting information about application:\n\n"
                    + error)
            ))
    }

    render() {
        const {info, health} = this.state
        const beName = info.build && info.build.name
        const beArtifact = info.build && info.build.artifact
        const beVersion = info.build && info.build.version
        const beTime = info.build && info.build.time

        return (
            <div>
                <Breadcrumb>
                    <Breadcrumb.Item active>About Aircraft Balancer</Breadcrumb.Item>
                </Breadcrumb>

                <div className="content">
                    <Table bordered>
                        <thead>
                        <tr>
                            <th colSpan="2">Backend services</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Name:</td><td>{beName}</td>
                        </tr>
                        <tr>
                            <td>Artifact:</td><td>{beArtifact}</td>
                        </tr>
                        <tr>
                            <td>Build version:</td><td>{beVersion}</td>
                        </tr>
                        <tr>
                            <td>Build time:</td><td>{beTime}</td>
                        </tr>
                        <tr>
                            <td>Status:</td><td>{health.status}</td>
                        </tr>
                        </tbody>
                    </Table>
                </div>
            </div>
        );
    }
}

export default About;