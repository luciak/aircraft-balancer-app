import React, {PureComponent} from 'react';
import axios from 'axios';
import Breadcrumb from 'react-bootstrap/Breadcrumb'
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {Link} from 'react-router-dom';

import ButtonToolbar from "react-bootstrap/ButtonToolbar";
import Button from "react-bootstrap/Button";

import ReactTable from 'react-table';
import 'react-table/react-table.css'

import {getErrorMsgFromResponse} from "../utils/api-utils";

class Flights extends PureComponent {

    state = {
        flights: []
    }

    componentDidMount() {
        this.findFlights()
    }

    findFlights = () => (
        axios.get('/api/flight')
            .then(result => {
                this.setState({flights: result.data});
            })
            .catch(error => (
                window.alert("Error while finding flights:\n\n" + getErrorMsgFromResponse(error))
            ))
    )

    deleteFlight = (ufi) => {
        if (window.confirm("This will delete the flight. Do you want to proceed?")) {
            axios.delete(`/api/flight/${ufi}`)
                .then(result => {
                    this.findFlights()
                })
                .catch(error => (
                    window.alert("Error while deleting the flight:\n\n" + getErrorMsgFromResponse(error))
                ))
        }
    }

    render() {

        const columns = [{
            Header: 'UFI',
            accessor: 'ufi'
        }, {
            Header: 'Aircraft',
            accessor: 'aircraft.registrationMark'
        }, {
            Header: 'Flight Number',
            accessor: 'flightNumber'
        }, {
            Header: 'Origin Date',
            accessor: 'originDate'
        }, {
            Header: 'Departure Airport',
            accessor: 'departureAirport'
        }, {
            Header: 'Arrival Airport',
            accessor: 'arrivalAirport'
        }, {
            Header: 'Repeat Code',
            accessor: 'repeatCode',
            maxWidth: 115
        }, {
            Header: 'Status',
            accessor: 'status',
        }, {
            Header: '',
            width: 90,
            accessor: 'ufi',
            Cell: row =>
                <div style={{textAlign: "center"}}>
                    <Link to={`/flights/${row.value}`}>
                        <Button size="sm" style={{marginRight: "2px"}}><FontAwesomeIcon icon="edit"/></Button>
                    </Link>
                    <Button size="sm" variant="danger" style={{marginLeft: "2px"}}
                            onClick={() => (this.deleteFlight(row.value))}>
                        <FontAwesomeIcon icon="trash"/></Button>
                </div>
        }]

        return (
            <div>
                <Breadcrumb>
                    <Breadcrumb.Item active>Flights</Breadcrumb.Item>
                </Breadcrumb>

                <div className="content">
                    <ButtonToolbar>
                        <Link to={'/flights/create'}>
                            <Button><FontAwesomeIcon style={{marginRight: "8px"}} icon="plus"/>Create
                                Flight</Button>
                        </Link>
                    </ButtonToolbar>

                    <ReactTable
                        defaultPageSize={10}
                        data={this.state.flights}
                        columns={columns}
                        style={{height: "100%"}}
                    />
                </div>
            </div>
        );
    }
}

export default Flights;